Old Light Readme
================

The Old Light project provides a toolkit for processing astronomical
images captured with DSLR cameras.

The primary aim is to be able to stack images, with bias, dark and flat
frame adjustments applied. Light frames can be aligned by detecting
stars and determining the transformation between star catalogs for each
image. The tools are written to take full advantage of multiprocessor
systems, so by default will spread processing across all available CPU
cores.

NB start alignment is only partially implemented and not working reliably
yet, so at this time oldlight is only useful for creating startrail
images. Watch this space.

Although using DSLR raw files as the primary data source, processing will
involve converting these to plain PGM/PPM file formats. This enables the
stacking tools to have efficient random access to regions within the images.
This enables stacking of enourmous image sequences to be done incrementally
line-by-line, without consuming a significant amount of RAM, albeit at the
cost of considerable disk space usage for the intermediate files.

The tool is initially targetting the Linux operating system, and requires
GLib2,  LibRaw and the Meson build system.

It is made available under the terms of the GNU LGPLv2+.

## Example processing sequences

### Bias frames

Bias frames are used record the inherant noise in camera sensors. They
are captured by taking as short an exposure as possible, with the lens
cover on. Approximately 50 frames are desirable.

They are processed by extracting the raw file content to a PGM file
with no debayering. All the frames are then combined using the median
stacking method to create a master bias file.

```
$ oldlight-raw-extract -v \
     Milky-Way/Bias/*nef

$ oldlight-stack \
     --method median \
     -o Milky-Way/Bias.pgm \
     Milky-Way/Bias/*pgm
```

### Dark frames

Dark frames are used to record the noise in camera sensors that builds up
during exposure of light frames. They are captured using the same ISO,
shutter speed, and physical environment (especially temperature) as the
light frames, but with the lens cap on. A minimum of 50 frame are
desirable, upto as many as there are light frames.

They are processed by extracting the raw file content to a PGM file
with no debaying, while subtracting the master bias frame. All the
frames are then combined using the median stacking method to create a
master dark file.

```
$ oldlight-raw-extract -v \
     --bias-file Milky-Way/Bias.pgm \
     Milky-Way/Dark/*nef

$ oldlight-stack \
     --method median \
     -o Milky-Way/Dark.pgm \
     Milky-Way/Dark/*pgm
```

### Dark flat frames

Dark flat frames are used to record the noise in camera sensors that builds
up during exposure of flat frames. They are captured using the same ISO,
shutter speed, and physical environment (especially temperature) as the
flat frames, but with the lens cap on. Approximately 50 frames are
desirable.

They are processed by extracting the raw file content to a PGM file
with no debaying, while subtracting the master bias frame. All the
frames are then combined using the median stacking method to create a
master dark flat file.

```
$ oldlight-raw-extract -v \
     --bias-file Milky-Way/Bias.pgm \
     Milky-Way/DarkFlat/*nef

$ oldlight-stack \
     --method median \
     -o Milky-Way/DarkFlat.pgm \
     Milky-Way/DarkFlat/*pgm
```

### Flat frames

Flat frames are used to record visual artifacts in the light frame introduced
by the lens and sensor. This primarily allows any lens vignetting and dust
spots to be eliminated. They are captured using the same focal distance as
the light frames, but with the lens pointing at a uniformly white source.
This can be achieved by placing a translucent plastic opal sheet in front
of the lens and capturing pictures pointing at a bright cloudy sky or a
laptop screen displaying a pure white background. The exposure time must
be chosen so there is no clipping of highlights or shadows. Approximately
50 frames are desirable.

They are processed by extracting the raw file content to a PGM file
with no debaying, while subtracting the master bias frame and the
master dark flat frame. All the frames are then combined using the
median stacking method to create a master flat file.

```
$ oldlight-raw-extract -v \
     --bias-file Milky-Way/Bias.pgm \
     --dark-file Milky-Way/DarkFlat.pgm \
     Milky-Way/Flat/*nef

$ oldlight-stack \
     --method median \
     -o Milky-Way/Flat.pgm \
     Milky-Way/Flat/*pgm
```

### Light frames

Light frames record the primary subject in the night sky. As many light
frames as possible/practical are desirable. When stacking frames with
star alignment, a greater number of light frames will result in a greater
signal to noise ratio. When stacking frames without star alignemnt, a
greater number of light frames will result in longer star trails.

#### No star alignment (aka star trails)

They are processed by extracting the raw file content to a full colour
PPM file with debaying, while subtracting the master bias frame and the
master dark flat frame and dividing by the master flat frame. All the
frames are then combined using the maximum stacking method to create a
master star trail image.

```
$ oldlight-raw-extract -v \
     --bias-file Milky-Way/Bias.pgm \
     --dark-file Milky-Way/Dark.pgm \
     --flat-file Milky-Way/Flat.pgm \
     --rgb --bayer-method ahd \
     Milky-Way/Light/*nef

$ oldlight-stack \
     --method maximum \
     -o Milky-Way/LightTrail.pgm \
     Milky-Way/Light/*ppm
```
