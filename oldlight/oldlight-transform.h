/*
 * oldlight-transform.h: transformation between two locations
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_TRANSFORM_H__
#define OL_TRANSFORM_H__

#include <glib-object.h>

#include "oldlight-location.h"
#include "oldlight-matrix.h"

G_BEGIN_DECLS

#define OL_TYPE_TRANSFORM            (ol_transform_get_type ())

typedef struct _OLTransform OLTransform;

struct _OLTransform
{
    gdouble rotation;
    gdouble xshift;
    gdouble yshift;
    OLMatrix *affine;
};

GType ol_transform_get_type(void);

OLTransform *ol_transform_new(gdouble rotation,
                              gdouble xshift,
                              gdouble yshift);

OLTransform *ol_transform_copy(OLTransform *transform);

void ol_transform_free(OLTransform *transform);

OLLocation *ol_transform_apply(OLTransform *transform,
                               OLLocation *location);

G_END_DECLS

#endif /* OL_TRANSFORM_H__ */
