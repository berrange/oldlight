/*
 * oldlight-image-writer-pnm.c: Pnm image writer
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-writer-pnm.h"

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

struct _OLImageWriterPnm
{
    OLImageWriter parent;
    int fd;
    off_t payload;
};


G_DEFINE_TYPE(OLImageWriterPnm, ol_image_writer_pnm, OL_TYPE_IMAGE_WRITER);


static gboolean
ol_image_writer_pnm_open(OLImageIO *io,
                         GError **err);
static gboolean
ol_image_writer_pnm_close(OLImageIO *io,
                          GError **err);
static gboolean
ol_image_writer_pnm_save_payload(OLImageWriter *writer,
                                 OLImage *image,
                                 guint x,
                                 guint y,
                                 guint width,
                                 guint height,
                                 GError **err);


#define OL_IMAGE_WRITER_PNM_ERROR ol_image_writer_pnm_error_quark()

static GQuark
ol_image_writer_pnm_error_quark(void)
{
    return g_quark_from_static_string("ol-image-writer-pnm");
}


static void ol_image_writer_pnm_finalize(GObject *object)
{
    ol_image_io_close(OL_IMAGE_IO(object), NULL);

    G_OBJECT_CLASS(ol_image_writer_pnm_parent_class)->finalize(object);
}


static void ol_image_writer_pnm_class_init(OLImageWriterPnmClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    OLImageIOClass *io_class = OL_IMAGE_IO_CLASS(klass);
    OLImageWriterClass *writer_class = OL_IMAGE_WRITER_CLASS(klass);

    object_class->finalize = ol_image_writer_pnm_finalize;

    io_class->open = ol_image_writer_pnm_open;
    io_class->close = ol_image_writer_pnm_close;
    writer_class->save_payload = ol_image_writer_pnm_save_payload;
}


static void ol_image_writer_pnm_init(OLImageWriterPnm *writer G_GNUC_UNUSED)
{
}


OLImageWriterPnm *ol_image_writer_pnm_new(const gchar *filename,
                                          OLMatrixFormat format,
                                          OLImageChannels channels,
                                          guint width,
                                          guint height)
{
    return OL_IMAGE_WRITER_PNM(g_object_new(OL_TYPE_IMAGE_WRITER_PNM,
                                            "filename", filename,
                                            "format", format,
                                            "channels", channels,
                                            "width", width,
                                            "height", height,
                                            NULL));
}


static gboolean ol_image_writer_set_error(const gchar *filename,
                                          const gchar *fmt,
                                          int ret,
                                          GError **err)
{
    const gchar *msg;
    if (ret == 0)
        return FALSE;

    msg = strerror(ret);
    g_set_error(err, OL_IMAGE_WRITER_PNM_ERROR, 0,
                fmt, filename, msg);
    return TRUE;
}


static gboolean
ol_image_writer_pnm_open(OLImageIO *io,
                         GError **err)
{
    OLImageWriterPnm *pnm = OL_IMAGE_WRITER_PNM(io);
    const gchar *filename = ol_image_io_get_filename(io);
    int ret;
    char header[1024];
    guint64 max;

    pnm->fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (pnm->fd < 0) {
        ol_image_writer_set_error(filename, "Unable to open file %s: %s", errno, err);
        return FALSE;
    }

    switch (ol_image_io_get_format(io)) {
    case OL_MATRIX_FORMAT_UINT8:
        max = 255;
        break;
    case OL_MATRIX_FORMAT_UINT16:
        max = 65535;
        break;
    case OL_MATRIX_FORMAT_UINT32:
        max = 4294967295;
        break;
    case OL_MATRIX_FORMAT_UINT64:
        max = 18446744073709551615ULL;
        break;
    default:
        g_set_error(err, OL_IMAGE_WRITER_PNM_ERROR, 0,
                    "Unsupported matrix format %d", ol_image_io_get_format(io));
        return FALSE;
    }

    ret = snprintf(header, sizeof(header) - 1, "P%c\n%d %d\n%" G_GUINT64_FORMAT "\n",
                   ol_image_io_get_channels(io) == OL_IMAGE_CHANNELS_GRAY ? '5' : '6',
                   ol_image_io_get_width(io),
                   ol_image_io_get_height(io),
                   max);
    if (ret < 0 || (ret >= (sizeof(header) - 1))) {
        ol_image_writer_set_error(filename, "Cannot write file header in %s: %s", EINVAL, err);
        return FALSE;
    }

    if (write(pnm->fd, header, ret) != ret) {
        ol_image_writer_set_error(filename, "Cannot write file header in %s: %s", EINVAL, err);
        return FALSE;
    }

    pnm->payload = ret;

    return TRUE;
}


static gboolean
ol_image_writer_pnm_close(OLImageIO *io,
                          GError **err G_GNUC_UNUSED)
{
    OLImageWriterPnm *pnm = OL_IMAGE_WRITER_PNM(io);
    if (pnm->fd != -1)
        close(pnm->fd);
    pnm->fd = -1;
    return TRUE;
}


static gboolean
ol_image_writer_pnm_save_payload(OLImageWriter *writer,
                                 OLImage *image,
                                 guint x,
                                 guint y,
                                 guint width,
                                 guint height,
                                 GError **err)
{
    OLImageWriterPnm *pnm = OL_IMAGE_WRITER_PNM(writer);
    const gchar *filename = ol_image_io_get_filename(OL_IMAGE_IO(writer));
    guint iwidth, iheight;
    off_t start;
    int n;
    gsize i;
    gboolean ret = FALSE;
    guint8 *buf;
    gsize buflen;

    iwidth = ol_image_io_get_width(OL_IMAGE_IO(writer));
    iheight = ol_image_io_get_height(OL_IMAGE_IO(writer));

    ol_image_data_host_to_be(image);

    if (((x + width) > iwidth) ||
        ((y + height) > iheight)) {
        ol_image_writer_set_error(filename, "Data region out of range saving %s: %s", EINVAL, err);
        goto cleanup;
    }

    if (x == 0 && width == iwidth) {
        start = (y * (image->bpp * width * height));
        buf = image->matrix->data.u8;
        buflen = image->bpp * width * height;
        while (buflen) {
            n = pwrite(pnm->fd, buf, buflen, pnm->payload + start);
            if (n > 0) {
                buflen -= n;
                start += n;
                buf += n;
            } else {
                g_set_error(err, OL_IMAGE_WRITER_PNM_ERROR, 0,
                            "Unable to write image data in '%s': %s",
                            filename,
                            strerror(errno));
                goto cleanup;
            }
        }
    } else {
        for (i = 0; i < height; i++) {
            start = ((y + i) * (image->bpp * width * height));
            buf = image->matrix->data.u8 + (i * image->bpp * width);
            buflen = image->bpp * width;
            while (buflen) {
                n = pwrite(pnm->fd, buf, buflen, pnm->payload + start);
                if (n > 0) {
                    buflen -= n;
                    start += n;
                    buf += n;
                } else {
                    g_set_error(err, OL_IMAGE_WRITER_PNM_ERROR, 0,
                                "Unable to write image data in '%s': %s",
                                filename,
                                strerror(errno));
                    goto cleanup;
                }
            }
        }
    }

    ret = TRUE;
 cleanup:
    ol_image_data_be_to_host(image);
    return ret;
}
