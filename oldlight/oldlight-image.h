/*
 * oldlight-image.h: image processing
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IMAGE_H__
#define OL_IMAGE_H__

#include "oldlight/oldlight-matrix.h"

G_BEGIN_DECLS

#define OL_TYPE_IMAGE            (ol_image_get_type ())

typedef enum {
    OL_IMAGE_CHANNELS_GRAY = 1,
    OL_IMAGE_CHANNELS_RGB = 3,
} OLImageChannels;

typedef struct _OLImage OLImage;

struct _OLImage
{
    OLMatrix *matrix;
    gsize width;
    gsize height;
    gsize bpp; /* bytes per pixel */
    OLImageChannels channels;
};


GType ol_image_get_type(void);

OLImage *ol_image_new(OLMatrixFormat format,
                      OLImageChannels channels,
                      gsize width, gsize height);

OLImage *ol_image_copy(OLImage *image);

void ol_image_free(OLImage *image);

OLImage *ol_image_load(const char *filename, GError **err);

gboolean ol_image_save(OLImage *image, const char *filename, GError **err);

OLImage *ol_image_convert(OLImage *image,
                          OLMatrixFormat format,
                          OLImageChannels channels);

int ol_image_width(OLImage *image);
int ol_image_height(OLImage *image);

GByteArray *ol_image_data(OLImage *image);

void ol_image_draw_rect(OLImage *image,
                        gssize x1, gssize y1, gssize x2, gssize y2,
                        guint8 *pixel);
void ol_image_draw_line(OLImage *image,
                        gssize x1, gssize y1, gssize x2, gssize y2,
                        guint8 *pixel);

void ol_image_data_host_to_be(OLImage *image);
void ol_image_data_be_to_host(OLImage *image);

void ol_image_subtract(OLImage *image,
                       OLImage *other);

void ol_image_divide_mean(OLImage *image,
                          OLImage *other,
                          gdouble mean);

G_END_DECLS

#endif /* OL_IMAGE_H__ */
