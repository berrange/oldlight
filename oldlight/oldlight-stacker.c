/*
 * oldlight-image-stacker.c: image stacker
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-stacker.h"
#include "oldlight/oldlight-matrix.h"
#include "oldlight/oldlight-image-writer-pnm.h"

#include <unistd.h>

struct _OLStacker
{
    GObject parent;
    guint threads;
    OLStackerMethod method;
    OLImageList *input;
    OLImageWriter *output;
    GMutex errlock;
    GError *err;
};


G_DEFINE_TYPE(OLStacker, ol_stacker, G_TYPE_OBJECT);


enum {
    PROP_0,
    PROP_THREADS,
    PROP_METHOD,
};

#define OL_STACKER_ERROR ol_stacker_error_quark()

static GQuark
ol_stacker_error_quark(void)
{
    return g_quark_from_static_string("ol-stacker");
}


static void ol_stacker_get_property(GObject *object,
                                    guint prop_id,
                                    GValue *value,
                                    GParamSpec *pspec)
{
    OLStacker *stacker = OL_STACKER(object);

    switch (prop_id) {
    case PROP_THREADS:
        g_value_set_uint(value, stacker->threads);
        break;

    case PROP_METHOD:
        g_value_set_enum(value, stacker->method);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_stacker_set_property(GObject *object,
                                    guint prop_id,
                                    const GValue *value,
                                    GParamSpec *pspec)
{
    OLStacker *stacker = OL_STACKER(object);

    switch (prop_id) {
    case PROP_THREADS:
        stacker->threads = g_value_get_uint(value);
        break;

    case PROP_METHOD:
        stacker->method = g_value_get_enum(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_stacker_finalize(GObject *object)
{
    OLStacker *stacker = OL_STACKER(object);

    g_mutex_clear(&stacker->errlock);

    G_OBJECT_CLASS(ol_stacker_parent_class)->finalize(object);
}


static void ol_stacker_class_init(OLStackerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    int nthreads = sysconf(_SC_NPROCESSORS_ONLN);
    if (nthreads <= 0)
        nthreads = 1;

    object_class->finalize = ol_stacker_finalize;
    object_class->get_property = ol_stacker_get_property;
    object_class->set_property = ol_stacker_set_property;

    g_object_class_install_property(object_class,
                                    PROP_THREADS,
                                    g_param_spec_uint("threads",
						      "Threads",
						      "Number of processing threads",
						      0, 65536, nthreads,
						      G_PARAM_READWRITE |
						      G_PARAM_CONSTRUCT_ONLY |
						      G_PARAM_STATIC_NAME |
						      G_PARAM_STATIC_NICK |
						      G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_METHOD,
                                    g_param_spec_enum("method",
						      "Method",
						      "Stacking method",
                                                      OL_TYPE_STACKER_METHOD,
                                                      OL_STACKER_METHOD_MEAN,
						      G_PARAM_READWRITE |
						      G_PARAM_CONSTRUCT_ONLY |
						      G_PARAM_STATIC_NAME |
						      G_PARAM_STATIC_NICK |
						      G_PARAM_STATIC_BLURB));
}


static void ol_stacker_init(OLStacker *stacker)
{
    g_mutex_init(&stacker->errlock);
}


OLStacker *ol_stacker_new(OLStackerMethod method)
{
    return OL_STACKER(g_object_new(OL_TYPE_STACKER,
                                   "method", method,
                                   NULL));
}


static void
ol_stacker_worker(gpointer job, gpointer opaque)
{
    OLStacker *stacker = opaque;
    int line = GPOINTER_TO_INT(job) - 1;
    GError *err = NULL;
    OLImage **inputs = NULL;
    gsize i;
    gsize offset;
    OLMatrix *slice = NULL;
    OLImage *output = NULL;

    g_mutex_lock(&stacker->errlock);
    if (stacker->err) {
        g_mutex_unlock(&stacker->errlock);
        return;
    }
    g_mutex_unlock(&stacker->errlock);

    inputs = ol_image_list_load_lines(stacker->input, line, 1, &err);
    if (!inputs)
        goto cleanup;

    output = ol_image_new(ol_image_list_get_format(stacker->input),
                          ol_image_list_get_channels(stacker->input),
                          ol_image_list_get_width(stacker->input),
                          1);

    slice = ol_matrix_new(ol_image_list_get_format(stacker->input),
                          ol_image_list_get_count(stacker->input), 1);

    for (offset = 0; offset < (output->width * output->height * output->channels); offset++) {
        for (i = 0; i < slice->len; i++) {
            switch (slice->format) {
            case OL_MATRIX_FORMAT_UINT8:
                slice->data.u8[i] = *(inputs[i]->matrix->data.u8 + offset);
                break;
            case OL_MATRIX_FORMAT_UINT16:
                slice->data.u16[i] = *(inputs[i]->matrix->data.u16 + offset);
                break;
            case OL_MATRIX_FORMAT_UINT32:
                slice->data.u32[i] = *(inputs[i]->matrix->data.u32 + offset);
                break;
            case OL_MATRIX_FORMAT_UINT64:
                slice->data.u64[i] = *(inputs[i]->matrix->data.u64 + offset);
                break;
            default:
                g_assert_not_reached();
            }
        }

        switch (stacker->method) {
        case OL_STACKER_METHOD_MEAN:
            switch (slice->format) {
            case OL_MATRIX_FORMAT_UINT8:
                *(output->matrix->data.u8 + offset) = ol_matrix_mean(slice);
                break;
            case OL_MATRIX_FORMAT_UINT16:
                *(output->matrix->data.u16 + offset) = ol_matrix_mean(slice);
                break;
            case OL_MATRIX_FORMAT_UINT32:
                *(output->matrix->data.u32 + offset) = ol_matrix_mean(slice);
                break;
            case OL_MATRIX_FORMAT_UINT64:
                *(output->matrix->data.u64 + offset) = ol_matrix_mean(slice);
                break;
            default:
                g_assert_not_reached();
            }
            break;
        case OL_STACKER_METHOD_MEDIAN:
            switch (slice->format) {
            case OL_MATRIX_FORMAT_UINT8:
                *(output->matrix->data.u8 + offset) = ol_matrix_median(slice);
                break;
            case OL_MATRIX_FORMAT_UINT16:
                *(output->matrix->data.u16 + offset) = ol_matrix_median(slice);
                break;
            case OL_MATRIX_FORMAT_UINT32:
                *(output->matrix->data.u32 + offset) = ol_matrix_median(slice);
                break;
            case OL_MATRIX_FORMAT_UINT64:
                *(output->matrix->data.u64 + offset) = ol_matrix_median(slice);
                break;
            default:
                g_assert_not_reached();
            }
            break;
        case OL_STACKER_METHOD_MAXIMUM:
            switch (slice->format) {
            case OL_MATRIX_FORMAT_UINT8:
                *(output->matrix->data.u8 + offset) = ol_matrix_maximum(slice);
                break;
            case OL_MATRIX_FORMAT_UINT16:
                *(output->matrix->data.u16 + offset) = ol_matrix_maximum(slice);
                break;
            case OL_MATRIX_FORMAT_UINT32:
                *(output->matrix->data.u32 + offset) = ol_matrix_maximum(slice);
                break;
            case OL_MATRIX_FORMAT_UINT64:
                *(output->matrix->data.u64 + offset) = ol_matrix_maximum(slice);
                break;
            default:
                g_assert_not_reached();
            }
            break;
        default:
            g_assert_not_reached();
        }
    }

    if (!ol_image_writer_save_payload(stacker->output,
                                      output,
                                      0, line,
                                      ol_image_list_get_width(stacker->input), 1,
                                      &err))
        goto cleanup;

 cleanup:
    if (inputs) {
        for (i = 0; i < ol_image_list_get_count(stacker->input); i++) {
            ol_image_free(inputs[i]);
        }
        g_free(inputs);
    }
    ol_image_free(output);
    ol_matrix_free(slice);
    if (err) {
        g_mutex_lock(&stacker->errlock);
        if (!stacker->err)
            stacker->err = err;
        else
            g_error_free(err);
        g_mutex_unlock(&stacker->errlock);
    }
}


gboolean ol_stacker_run(OLStacker *stacker,
                        OLImageList *input_files,
                        const char *output_file,
                        GError **err)
{
    gboolean ret = FALSE;
    gsize i;
    GError *lerr = NULL;
    GThreadPool *pool = g_thread_pool_new(ol_stacker_worker,
                                          stacker,
                                          stacker->threads,
                                          TRUE,
                                          &lerr);
    if (lerr) {
        g_propagate_error(err, lerr);
        goto cleanup;
    }

    if (!ol_image_list_open(input_files, err))
        goto cleanup;

    stacker->output = OL_IMAGE_WRITER(ol_image_writer_pnm_new(
                                          output_file,
                                          ol_image_list_get_format(input_files),
                                          ol_image_list_get_channels(input_files),
                                          ol_image_list_get_width(input_files),
                                          ol_image_list_get_height(input_files)));
    stacker->input = input_files;

    if (!ol_image_io_open(OL_IMAGE_IO(stacker->output), err))
        goto cleanup;

    for (i = 0; i < ol_image_list_get_height(input_files); i++) {
        if (!g_thread_pool_push(pool, GINT_TO_POINTER(i + 1), err))
            goto cleanup;
    }

    g_thread_pool_free(pool, FALSE, TRUE);
    pool = NULL;

    if (!ol_image_io_close(OL_IMAGE_IO(stacker->output), err))
        goto cleanup;

    if (stacker->err) {
        g_propagate_error(err, stacker->err);
        stacker->err = NULL;
    }

    ret = TRUE;

 cleanup:
    if (!ret) {
        if (pool)
            g_thread_pool_free(pool, TRUE, TRUE);
        unlink(output_file);
    }
    if (stacker->output)
        g_object_unref(stacker->output);
    stacker->output = NULL;
    stacker->input = NULL;
    return ret;
}


gboolean ol_stacker_method_from_string(const char *method,
                                       int *value,
                                       GError **err)
{
    GEnumClass *enum_class;
    GEnumValue *enum_value;

    *value = -1;

    enum_class = g_type_class_ref(OL_TYPE_STACKER_METHOD);
    enum_value = g_enum_get_value_by_nick(enum_class, method);
    g_type_class_unref(enum_class);

    if (enum_value == NULL) {
        g_set_error(err, OL_STACKER_ERROR, 0,
                    "Unknown stacking method '%s'", method);
        return FALSE;
    }

    *value = enum_value->value;
    return TRUE;
}
