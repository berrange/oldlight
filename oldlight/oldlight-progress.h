/*
 *  oldlight-progress.h: monitor operation progress
 *
 *  Copyright (C) 2018 Daniel P. Berrange
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __OL_PROGRESS_H__
#define __OL_PROGRESS_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define OL_TYPE_PROGRESS                (ol_progress_get_type ())

G_DECLARE_INTERFACE(OLProgress, ol_progress, OL, PROGRESS, GObject);

struct _OLProgressInterface {
    GTypeInterface parent;

    void (*start) (OLProgress *prog, gfloat target, const gchar *msg);
    void (*update) (OLProgress *prog, gfloat current);
    void (*stop) (OLProgress *prog);
};

void ol_progress_start(OLProgress *prog, gfloat target, const gchar *msg);
void ol_progress_update(OLProgress *prog, gfloat current);
void ol_progress_stop(OLProgress *prog);

G_END_DECLS

#endif /* __OL_PROGRESS_H__ */
