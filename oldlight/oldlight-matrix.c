/*
 * oldlight-matrix.c: matrix processing
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-matrix.h"

#include "libraw/libraw.h"

#include <stdio.h>
#include <endian.h>
#include <math.h>

typedef struct {
    void (*copy_region)(OLMatrix *src, OLMatrix *dst,
                        gsize x, gsize y, gsize width, gsize height);
    void (*copy_with_border)(OLMatrix *src, OLMatrix *dst,
                             gsize left, gsize top);
    void (*clear_border)(OLMatrix *matrix,
                         gsize left, gsize right,
                         gsize top, gsize bottom);
    void (*abs)(OLMatrix *matrix);
    gdouble (*sum)(OLMatrix *matrix);
    gdouble (*sum_abs)(OLMatrix *matrix);
    gdouble (*sum_masked)(OLMatrix *matrix, OLMatrix *mask);
    void (*sum_xaxis)(OLMatrix *matrix, OLMatrix *output);
    void (*sum_yaxis)(OLMatrix *matrix, OLMatrix *output);
    gdouble (*median)(OLMatrix *matrix);
    gdouble (*maximum)(OLMatrix *matrix);
    void (*maximum_filter)(OLMatrix *input, OLMatrix *mask, OLMatrix *output);
    void (*convolve)(OLMatrix *input, OLMatrix *kern, OLMatrix *output);
    void (*product)(OLMatrix *matrix, OLMatrix *other, OLMatrix *output);
    void (*multiply)(OLMatrix *matrix, OLMatrix *other, OLMatrix *output);
} ol_matrix_ops;

#define OL_MATRIX_TYPE gint8
#define OL_MATRIX_FIELD i8
#define OL_ABS(a) abs(a)
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE gint16
#define OL_MATRIX_FIELD i16
#define OL_ABS(a) abs(a)
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE gint32
#define OL_MATRIX_FIELD i32
#define OL_ABS(a) abs(a)
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE gint64
#define OL_MATRIX_FIELD i64
#define OL_ABS(a) llabs(a)
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE guint8
#define OL_MATRIX_FIELD u8
#define OL_ABS(a) a
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE guint16
#define OL_MATRIX_FIELD u16
#define OL_ABS(a) a
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE guint32
#define OL_MATRIX_FIELD u32
#define OL_ABS(a) a
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE guint64
#define OL_MATRIX_FIELD u64
#define OL_ABS(a) a
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE gfloat
#define OL_MATRIX_FIELD f32
#define OL_ABS(a) fabs(a)
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

#define OL_MATRIX_TYPE gdouble
#define OL_MATRIX_FIELD f64
#define OL_ABS(a) fabs(a)
#include "oldlight/oldlight-matrix-ops.h"
#undef OL_ABS
#undef OL_MATRIX_FIELD
#undef OL_MATRIX_TYPE

static const ol_matrix_ops *ops[] = {
    &ol_matrix_ops_gint8,
    &ol_matrix_ops_gint16,
    &ol_matrix_ops_gint32,
    &ol_matrix_ops_gint64,
    &ol_matrix_ops_guint8,
    &ol_matrix_ops_guint16,
    &ol_matrix_ops_guint32,
    &ol_matrix_ops_guint64,
    &ol_matrix_ops_gfloat,
    &ol_matrix_ops_gdouble,
};

GType ol_matrix_get_type(void)
{
    static GType matrix_type = 0;

    if (G_UNLIKELY(matrix_type == 0)) {
        matrix_type = g_boxed_type_register_static
            ("OLMatrix",
             (GBoxedCopyFunc)ol_matrix_copy,
             (GBoxedFreeFunc)ol_matrix_free);
    }

    return matrix_type;
}


OLMatrix *ol_matrix_new(OLMatrixFormat format,
			gsize width,
			gsize height)
{
    OLMatrix *matrix = g_new0(OLMatrix, 1);

    matrix->format = format;
    matrix->width = width;
    matrix->height = height;
    matrix->len = width * height;

    switch (format) {
    case OL_MATRIX_FORMAT_INT8:
        matrix->data.i8 = g_new0(gint8, matrix->len);
        break;
    case OL_MATRIX_FORMAT_INT16:
        matrix->data.i16 = g_new0(gint16, matrix->len);
        break;
    case OL_MATRIX_FORMAT_INT32:
        matrix->data.i32 = g_new0(gint32, matrix->len);
        break;
    case OL_MATRIX_FORMAT_INT64:
        matrix->data.i64 = g_new0(gint64, matrix->len);
        break;
    case OL_MATRIX_FORMAT_UINT8:
        matrix->data.u8 = g_new0(guint8, matrix->len);
        break;
    case OL_MATRIX_FORMAT_UINT16:
        matrix->data.u16 = g_new0(guint16, matrix->len);
        break;
    case OL_MATRIX_FORMAT_UINT32:
        matrix->data.u32 = g_new0(guint32, matrix->len);
        break;
    case OL_MATRIX_FORMAT_UINT64:
        matrix->data.u64 = g_new0(guint64, matrix->len);
        break;
    case OL_MATRIX_FORMAT_FLOAT32:
        matrix->data.f32 = g_new0(gfloat, matrix->len);
        break;
    case OL_MATRIX_FORMAT_FLOAT64:
        matrix->data.f64 = g_new0(gdouble, matrix->len);
        break;
    default:
        g_assert_not_reached();
    }

    return matrix;
}

OLMatrix *ol_matrix_copy(OLMatrix *matrix)
{
    OLMatrix *newmatrix = ol_matrix_new(matrix->format,
                                        matrix->width,
                                        matrix->height);

    switch (matrix->format) {
    case OL_MATRIX_FORMAT_INT8:
        memcpy(newmatrix->data.i8, matrix->data.i8, matrix->len * sizeof(gint8));
        break;
    case OL_MATRIX_FORMAT_INT16:
        memcpy(newmatrix->data.i16, matrix->data.i16, matrix->len * sizeof(gint16));
        break;
    case OL_MATRIX_FORMAT_INT32:
        memcpy(newmatrix->data.i32, matrix->data.i32, matrix->len * sizeof(gint32));
        break;
    case OL_MATRIX_FORMAT_INT64:
        memcpy(newmatrix->data.i64, matrix->data.i64, matrix->len * sizeof(gint64));
        break;
    case OL_MATRIX_FORMAT_UINT8:
        memcpy(newmatrix->data.u8, matrix->data.u8, matrix->len * sizeof(guint8));
        break;
    case OL_MATRIX_FORMAT_UINT16:
        memcpy(newmatrix->data.u16, matrix->data.u16, matrix->len * sizeof(guint16));
        break;
    case OL_MATRIX_FORMAT_UINT32:
        memcpy(newmatrix->data.u32, matrix->data.u32, matrix->len * sizeof(guint32));
        break;
    case OL_MATRIX_FORMAT_UINT64:
        memcpy(newmatrix->data.u64, matrix->data.u64, matrix->len * sizeof(guint64));
        break;
    case OL_MATRIX_FORMAT_FLOAT32:
        memcpy(newmatrix->data.f32, matrix->data.f32, matrix->len * sizeof(gfloat));
        break;
    case OL_MATRIX_FORMAT_FLOAT64:
        memcpy(newmatrix->data.f64, matrix->data.f64, matrix->len * sizeof(gdouble));
        break;
    default:
        g_assert_not_reached();
    }

    return newmatrix;
}

OLMatrix *ol_matrix_copy_region(OLMatrix *matrix,
                                gsize x,
                                gsize y,
                                gsize width,
                                gsize height)
{
    OLMatrix *newmatrix = ol_matrix_new(matrix->format,
                                        width,
                                        height);
    ops[matrix->format]->copy_region(matrix, newmatrix,
                                     x, y, width, height);
    return newmatrix;
}

OLMatrix *ol_matrix_copy_with_border(OLMatrix *matrix,
                                     gsize left,
                                     gsize right,
                                     gsize top,
                                     gsize bottom)
{
    OLMatrix *newmatrix = ol_matrix_new(matrix->format,
                                        matrix->width + left + right,
                                        matrix->height + top + bottom);
    ops[matrix->format]->copy_with_border(matrix, newmatrix,
                                          left, top);
    return newmatrix;
}

void ol_matrix_free(OLMatrix *matrix)
{
    if (!matrix)
        return;
    g_free(matrix->data.u8);
    g_free(matrix);
}

void ol_matrix_clear_border(OLMatrix *matrix,
                            gsize left,
                            gsize right,
                            gsize top,
                            gsize bottom)
{
    ops[matrix->format]->clear_border(matrix, left, right, top, bottom);
}

void ol_matrix_abs(OLMatrix *matrix)
{
    ops[matrix->format]->abs(matrix);
}

gdouble ol_matrix_sum(OLMatrix *matrix)
{
    return ops[matrix->format]->sum(matrix);
}

gdouble ol_matrix_sum_abs(OLMatrix *matrix)
{
    return ops[matrix->format]->sum_abs(matrix);
}

gdouble ol_matrix_sum_masked(OLMatrix *matrix, OLMatrix *mask)
{
    return ops[matrix->format]->sum_masked(matrix, mask);
}

OLMatrix *ol_matrix_sum_xaxis(OLMatrix *matrix)
{
    OLMatrix *output = ol_matrix_new(matrix->format, matrix->width, 1);
    ops[matrix->format]->sum_xaxis(matrix, output);
    return output;
}

OLMatrix *ol_matrix_sum_yaxis(OLMatrix *matrix)
{
    OLMatrix *output = ol_matrix_new(matrix->format, matrix->height, 1);
    ops[matrix->format]->sum_yaxis(matrix, output);
    return output;
}

gdouble ol_matrix_mean(OLMatrix *matrix)
{
    gdouble sum = ol_matrix_sum(matrix);
    return sum / matrix->len;
}

gdouble ol_matrix_median(OLMatrix *matrix)
{
    return ops[matrix->format]->median(matrix);
}


gdouble ol_matrix_median_abs_deviation(OLMatrix *matrix)
{
    OLMatrix *tmp = ol_matrix_copy(matrix);
    gdouble ret;
    ops[matrix->format]->abs(tmp);
    ret = ops[matrix->format]->median(tmp);
    ol_matrix_free(tmp);
    return ret;
}

gdouble ol_matrix_median_abs_deviation_std(OLMatrix *matrix)
{
    return ol_matrix_median_abs_deviation(matrix) *  1.482602218505602;
}


gdouble ol_matrix_maximum(OLMatrix *matrix)
{
    return ops[matrix->format]->maximum(matrix);
}


OLMatrix *ol_matrix_new_maximum_filter(OLMatrix *matrix,
                                       OLMatrix *mask)
{
    OLMatrix *dst = ol_matrix_new(matrix->format,
                                  matrix->width,
                                  matrix->height);
    ol_matrix_maximum_filter(matrix, mask, dst);
    return dst;
}


OLMatrix *ol_matrix_new_convolve(OLMatrix *matrix,
                                 OLMatrix *kern)
{
    OLMatrix *dst = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64,
                                  matrix->width,
                                  matrix->height);
    ol_matrix_convolve(matrix, kern, dst);
    return dst;
}

OLMatrix *ol_matrix_new_product(OLMatrix *matrix,
                                OLMatrix *other)
{
    OLMatrix *dst = ol_matrix_new(matrix->format,
                                  other->width,
                                  matrix->height);
    ol_matrix_product(matrix, other, dst);
    return dst;
}

OLMatrix *ol_matrix_new_multiply(OLMatrix *matrix,
                                 OLMatrix *other)
{
    OLMatrix *dst = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64,
                                  matrix->width,
                                  matrix->height);
    ol_matrix_multiply(matrix, other, dst);
    return dst;
}


void ol_matrix_maximum_filter(OLMatrix *matrix,
                              OLMatrix *mask,
                              OLMatrix *output)
{
    g_assert(matrix->format == output->format);
    g_assert(matrix->width == output->width);
    g_assert(matrix->height == output->height);
    ops[matrix->format]->maximum_filter(matrix, mask, output);
}


void ol_matrix_convolve(OLMatrix *matrix,
                        OLMatrix *kern,
                        OLMatrix *output)
{
    g_assert(output->format == OL_MATRIX_FORMAT_FLOAT64);
    g_assert(kern->format == OL_MATRIX_FORMAT_FLOAT64);
    g_assert(matrix->width == output->width);
    g_assert(matrix->height == output->height);
    ops[matrix->format]->convolve(matrix, kern, output);
}


void ol_matrix_product(OLMatrix *matrix,
                       OLMatrix *other,
                       OLMatrix *output)
{
    g_assert(matrix->format == other->format);
    g_assert(matrix->format == output->format);
    g_assert(matrix->width == other->height);
    g_assert(other->width == output->width);
    g_assert(matrix->height == output->height);
    ops[matrix->format]->product(matrix, other, output);
}


void ol_matrix_multiply(OLMatrix *matrix,
                        OLMatrix *other,
                        OLMatrix *output)
{
    g_assert(matrix->format == other->format);
    g_assert(matrix->width == other->width);
    g_assert(matrix->height == other->height);
    g_assert(matrix->format == output->format);
    g_assert(matrix->width == output->width);
    g_assert(matrix->height == output->height);
    ops[matrix->format]->multiply(matrix, other, output);
}
