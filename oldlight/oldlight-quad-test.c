/*
 * oldlight-quad-test.c: asterism of four stars
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight-quad.h"

#include <math.h>


static void test_normalize(void)
{
    OLLocation stars[][4] = {
        /* Ordering permutations */
        {
            { .x = 10, .y = 40 },
            { .x = 20, .y = 20 },
            { .x = 35, .y = 45 },
            { .x = 60, .y = 65 },
        },
        {
            { .x = 10, .y = 40 },
            { .x = 20, .y = 20 },
            { .x = 60, .y = 65 },
            { .x = 35, .y = 45 },
        },
        {
            { .x = 10, .y = 40 },
            { .x = 35, .y = 45 },
            { .x = 20, .y = 20 },
            { .x = 60, .y = 65 },
        },
        {
            { .x = 10, .y = 40 },
            { .x = 35, .y = 45 },
            { .x = 60, .y = 65 },
            { .x = 20, .y = 20 },
        },
        {
            { .x = 10, .y = 40 },
            { .x = 60, .y = 65 },
            { .x = 20, .y = 20 },
            { .x = 35, .y = 45 },
        },
        {
            { .x = 10, .y = 40 },
            { .x = 60, .y = 65 },
            { .x = 35, .y = 45 },
            { .x = 20, .y = 20 },
        },

        {
            { .x = 20, .y = 20 },
            { .x = 10, .y = 40 },
            { .x = 35, .y = 45 },
            { .x = 60, .y = 65 },
        },
        {
            { .x = 20, .y = 20 },
            { .x = 10, .y = 40 },
            { .x = 60, .y = 65 },
            { .x = 35, .y = 45 },
        },
        {
            { .x = 20, .y = 20 },
            { .x = 35, .y = 45 },
            { .x = 10, .y = 40 },
            { .x = 60, .y = 65 },
        },
        {
            { .x = 20, .y = 20 },
            { .x = 35, .y = 45 },
            { .x = 60, .y = 65 },
            { .x = 10, .y = 40 },
        },
        {
            { .x = 20, .y = 20 },
            { .x = 60, .y = 65 },
            { .x = 10, .y = 40 },
            { .x = 35, .y = 45 },
        },
        {
            { .x = 20, .y = 20 },
            { .x = 60, .y = 65 },
            { .x = 35, .y = 45 },
            { .x = 10, .y = 40 },
        },


        {
            { .x = 35, .y = 45 },
            { .x = 10, .y = 40 },
            { .x = 20, .y = 20 },
            { .x = 60, .y = 65 },
        },
        {
            { .x = 35, .y = 45 },
            { .x = 10, .y = 40 },
            { .x = 60, .y = 65 },
            { .x = 20, .y = 20 },
        },
        {
            { .x = 35, .y = 45 },
            { .x = 20, .y = 20 },
            { .x = 10, .y = 40 },
            { .x = 60, .y = 65 },
        },
        {
            { .x = 35, .y = 45 },
            { .x = 20, .y = 20 },
            { .x = 60, .y = 65 },
            { .x = 10, .y = 40 },
        },
        {
            { .x = 35, .y = 45 },
            { .x = 60, .y = 65 },
            { .x = 10, .y = 40 },
            { .x = 20, .y = 20 },
        },
        {
            { .x = 35, .y = 45 },
            { .x = 60, .y = 65 },
            { .x = 20, .y = 20 },
            { .x = 10, .y = 40 },
        },

        {
            { .x = 60, .y = 65 },
            { .x = 10, .y = 40 },
            { .x = 20, .y = 20 },
            { .x = 35, .y = 45 },
        },
        {
            { .x = 60, .y = 65 },
            { .x = 10, .y = 40 },
            { .x = 35, .y = 45 },
            { .x = 20, .y = 20 },
        },
        {
            { .x = 60, .y = 65 },
            { .x = 20, .y = 20 },
            { .x = 10, .y = 40 },
            { .x = 35, .y = 45 },
        },
        {
            { .x = 60, .y = 65 },
            { .x = 20, .y = 20 },
            { .x = 35, .y = 45 },
            { .x = 10, .y = 40 },
        },
        {
            { .x = 60, .y = 65 },
            { .x = 35, .y = 45 },
            { .x = 10, .y = 40 },
            { .x = 20, .y = 20 },
        },
        {
            { .x = 60, .y = 65 },
            { .x = 35, .y = 45 },
            { .x = 20, .y = 20 },
            { .x = 10, .y = 40 },
        },

        /* As first, but rotated 90 degrees */
        {
            { .x = 40, .y = -10 },
            { .x = 20, .y = -20 },
            { .x = 45, .y = -35 },
            { .x = 65, .y = -60 },
        },
        /* As first, but rotated 180 degrees */
        {
            { .x = -10, .y = -40 },
            { .x = -20, .y = -20 },
            { .x = -35, .y = -45 },
            { .x = -60, .y = -65 },
        },
        /* As first, but rotated 270 degrees */
        {
            { .x = -40, .y = 10 },
            { .x = -20, .y = 20 },
            { .x = -45, .y = 35 },
            { .x = -65, .y = 60 },
        },
        /* As first, but rotated 270 degrees, offset 100, -35 */
        {
            { .x = 60, .y = -25 },
            { .x = 80, .y = -15 },
            { .x = 55, .y = 0 },
            { .x = 35, .y = 25 },
        },
    };
    gsize i;

    OLQuad *a = ol_quad_new(&stars[0][0], &stars[0][1], &stars[0][2], &stars[0][3]);
    for (i = 1; i < G_N_ELEMENTS(stars); i++) {
        OLQuad *b = ol_quad_new(&stars[i][0], &stars[i][1], &stars[i][2], &stars[i][3]);

        g_assert_cmpfloat(fabs(a->hash[0].x - b->hash[0].x), <, 0.0001);
        g_assert_cmpfloat(fabs(a->hash[0].y - b->hash[0].y), <, 0.0001);
        g_assert_cmpfloat(fabs(a->hash[1].x - b->hash[1].x), <, 0.0001);
        g_assert_cmpfloat(fabs(a->hash[1].y - b->hash[1].y), <, 0.0001);

        ol_quad_free(b);
    }
    ol_quad_free(a);
}


int main(int argc, char **argv)
{
    g_test_init(&argc, &argv, NULL);

    g_test_add_func("/transform/normalize", test_normalize);

    return g_test_run();
}
