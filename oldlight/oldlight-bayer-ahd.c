/*
 * oldlight-bayer-ahd.h: bayer processing ahd algorithm
 *
 * This file is derived from bayer.c in libdc1394,
 * written by Damien Douxchamps and Frederic Devernay
 * The original AHD Bayer decoding is from Dave Coffin's DCRAW.
 *
 * The oldlight changes are:
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 *
 * Adaptive Homogeneity-Directed interpolation is based on
 * the work of Keigo Hirakawa, Thomas Parks, and Paul Lee.
 */

#include "oldlight/oldlight-bayer-ahd.h"

#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


struct _OLBayerAHD
{
    OLBayer parent;
};

G_DEFINE_TYPE(OLBayerAHD, ol_bayer_ahd, OL_TYPE_BAYER);


static void
ol_bayer_ahd_decode_uint8(const guint8 *restrict bayer,
                          guint8 *restrict dst,
                          gsize sx,
                          gsize sy,
                          OLBayerFilter filter);
static void
ol_bayer_ahd_decode_uint16(const guint16 *restrict bayer,
                           guint16 *restrict dst,
                           gsize sx,
                           gsize sy,
                           OLBayerFilter filter,
                           int bits);


static void ol_bayer_ahd_class_init(OLBayerAHDClass *klass)
{
    OLBayerClass *bayer_class = OL_BAYER_CLASS(klass);

    bayer_class->decode8 = ol_bayer_ahd_decode_uint8;
    bayer_class->decode16 = ol_bayer_ahd_decode_uint16;
}


static void ol_bayer_ahd_init(OLBayerAHD *bayer G_GNUC_UNUSED)
{
}


/* AHD interpolation ported from dcraw to libdc1394 by Samuel Audet */
static gsize ahd_inited = 0;

#define CLIPOUT(x)        LIM(x,0,255)
#define CLIPOUT16(x,bits) LIM(x,0,((1<<bits)-1))
#define FORUC3 for (uc=0; uc < 3; uc++)
#define FORC3 for (c=0; c < 3; c++)

#define SQR(x) ((x)*(x))
#define LIM(x,min,max) MAX(min,MIN(x,max))
#define ULIM(x,y,z) ((y) < (z) ? LIM(x,y,z) : LIM(x,z,y))
/*
  In order to inline this calculation, I make the risky
  assumption that all filter patterns can be described
  by a repeating pattern of eight rows and two columns

  Return values are either 0/1/2/3 = G/M/C/Y or 0/1/2/3 = R/G1/B/G2
*/
#define FC(row,col)                                             \
    (filters >> ((((row) << 1 & 14) + ((col) & 1)) << 1) & 3)

static const double xyz_rgb[3][3] = {                        /* XYZ from RGB */
    { 0.412453, 0.357580, 0.180423 },
    { 0.212671, 0.715160, 0.072169 },
    { 0.019334, 0.119193, 0.950227 } };
static const double d65_white[3] = { 0.950456, 1, 1.088754 };

static void cam_to_cielab (guint16 cam[3], double lab[3]) /* [SA] */
{
    int c, i, j;
    double r, xyz[3];
    static double cbrt[0x10000], xyz_cam[3][4];

    if (cam == NULL) {
        for (i=0; i < 0x10000; i++) {
            r = i / 65535.0;
            cbrt[i] = r > 0.008856 ? pow(r,1/3.0) : 7.787*r + 16/116.0;
        }
        for (i=0; i < 3; i++)
            for (j=0; j < 3; j++)                           /* [SA] */
                xyz_cam[i][j] = xyz_rgb[i][j] / d65_white[i]; /* [SA] */
    } else {
        xyz[0] = xyz[1] = xyz[2] = 0.5;
        FORC3 { /* [SA] */
            xyz[0] += xyz_cam[0][c] * cam[c];
            xyz[1] += xyz_cam[1][c] * cam[c];
            xyz[2] += xyz_cam[2][c] * cam[c];
        }
        xyz[0] = cbrt[CLIPOUT16((int) xyz[0],16)];        /* [SA] */
        xyz[1] = cbrt[CLIPOUT16((int) xyz[1],16)];        /* [SA] */
        xyz[2] = cbrt[CLIPOUT16((int) xyz[2],16)];        /* [SA] */
        lab[0] = 116 * xyz[1] - 16;
        lab[1] = 500 * (xyz[0] - xyz[1]);
        lab[2] = 200 * (xyz[1] - xyz[2]);
    }
}

#define TS 256                /* Tile Size */

static void
ol_bayer_ahd_decode_uint8(const guint8 *restrict bayer,
                          guint8 *restrict dst,
                          gsize sx,
                          gsize sy,
                          OLBayerFilter filter)
{
    int i, j, top, left, row, col, tr, tc, fc, c, d, val, hm[2];
    /* the following has the same type as the image */
    guint8 (*pix)[3], (*rix)[3];      /* [SA] */
    guint16 rix16[3];                 /* [SA] */
    static const int dir[4] = { -1, 1, -TS, TS };
    unsigned ldiff[2][4], abdiff[2][4], leps, abeps;
    double flab[3];                     /* [SA] */
    guint8 (*rgb)[TS][TS][3];
    short (*lab)[TS][TS][3];
    char (*homo)[TS][TS], *buffer;

    /* start - new code for libdc1394 */
    guint32 filters;
    const int height = sy, width = sx;
    int x, y;

    if (g_once_init_enter (&ahd_inited)) {
        cam_to_cielab (NULL,NULL);
        g_once_init_leave(&ahd_inited, 1);
    }

    switch(filter) {
    case OL_BAYER_FILTER_BGGR:
        filters = 0x16161616;
        break;
    case OL_BAYER_FILTER_GRBG:
        filters = 0x61616161;
        break;
    case OL_BAYER_FILTER_RGGB:
        filters = 0x94949494;
        break;
    case OL_BAYER_FILTER_GBRG:
        filters = 0x49494949;
        break;
    default:
        g_assert_not_reached();
    }

    /* fill-in destination with known exact values */
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            int channel = FC(y,x);
            dst[(y*width+x)*3 + channel] = bayer[y*width+x];
        }
    }
    /* end - new code for libdc1394 */

    /* start - code from border_interpolate (int border) */
    {
        int border = 3;
        unsigned urow, ucol, uy, ux, f, uc, sum[8];

        for (urow=0; urow < height; urow++)
            for (ucol=0; ucol < width; ucol++) {
                if (ucol==border && urow >= border && urow < height-border)
                    ucol = width-border;
                memset (sum, 0, sizeof sum);
                for (uy=urow-1; uy != urow+2; uy++)
                    for (ux=ucol-1; ux != ucol+2; ux++)
                        if (uy < height && ux < width) {
                            f = FC(uy,ux);
                            sum[f] += dst[(uy*width+ux)*3 + f];           /* [SA] */
                            sum[f+4]++;
                        }
                f = FC(urow,ucol);
                FORUC3 if (uc != f && sum[uc+4])                     /* [SA] */
                    dst[(urow*width+ucol)*3 + uc] = sum[uc] / sum[uc+4]; /* [SA] */
            }
    }
    /* end - code from border_interpolate (int border) */


    buffer = (char *)g_malloc (26*TS*TS);                /* 1664 kB */
    /* merror (buffer, "ahd_interpolate()"); */
    rgb  = (guint8(*)[TS][TS][3]) buffer;                /* [SA] */
    lab  = (short (*)[TS][TS][3])(buffer + 12*TS*TS);
    homo = (char  (*)[TS][TS])   (buffer + 24*TS*TS);

    for (top=0; top < height; top += TS-6)
        for (left=0; left < width; left += TS-6) {
            memset (rgb, 0, 12*TS*TS);

            /*  Interpolate green horizontally and vertically:                */
            for (row = top < 2 ? 2:top; row < top+TS && row < height-2; row++) {
                col = left + (FC(row,left) == 1);
                if (col < 2) col += 2;
                for (fc = FC(row,col); col < left+TS && col < width-2; col+=2) {
                    pix = (guint8 (*)[3])dst + (row*width+col);          /* [SA] */
                    val = ((pix[-1][1] + pix[0][fc] + pix[1][1]) * 2
                           - pix[-2][fc] - pix[2][fc]) >> 2;
                    rgb[0][row-top][col-left][1] = ULIM(val,pix[-1][1],pix[1][1]);
                    val = ((pix[-width][1] + pix[0][fc] + pix[width][1]) * 2
                           - pix[-2*width][fc] - pix[2*width][fc]) >> 2;
                    rgb[1][row-top][col-left][1] = ULIM(val,pix[-width][1],pix[width][1]);
                }
            }
            /*  Interpolate red and blue, and convert to CIELab:                */
            for (d=0; d < 2; d++)
                for (row=top+1; row < top+TS-1 && row < height-1; row++)
                    for (col=left+1; col < left+TS-1 && col < width-1; col++) {
                        pix = (guint8 (*)[3])dst + (row*width+col);        /* [SA] */
                        rix = &rgb[d][row-top][col-left];
                        if ((c = 2 - FC(row,col)) == 1) {
                            c = FC(row+1,col);
                            val = pix[0][1] + (( pix[-1][2-c] + pix[1][2-c]
                                                 - rix[-1][1] - rix[1][1] ) >> 1);
                            rix[0][2-c] = CLIPOUT(val);         /* [SA] */
                            val = pix[0][1] + (( pix[-width][c] + pix[width][c]
                                                 - rix[-TS][1] - rix[TS][1] ) >> 1);
                        } else
                            val = rix[0][1] + (( pix[-width-1][c] + pix[-width+1][c]
                                                 + pix[+width-1][c] + pix[+width+1][c]
                                                 - rix[-TS-1][1] - rix[-TS+1][1]
                                                 - rix[+TS-1][1] - rix[+TS+1][1] + 1) >> 2);
                        rix[0][c] = CLIPOUT(val);             /* [SA] */
                        c = FC(row,col);
                        rix[0][c] = pix[0][c];
                        rix16[0] = rix[0][0];                 /* [SA] */
                        rix16[1] = rix[0][1];                 /* [SA] */
                        rix16[2] = rix[0][2];                 /* [SA] */
                        cam_to_cielab (rix16, flab);          /* [SA] */
                        FORC3 lab[d][row-top][col-left][c] = 64*flab[c];
                    }
            /*  Build homogeneity maps from the CIELab images:                */
            memset (homo, 0, 2*TS*TS);
            for (row=top+2; row < top+TS-2 && row < height; row++) {
                tr = row-top;
                for (col=left+2; col < left+TS-2 && col < width; col++) {
                    tc = col-left;
                    for (d=0; d < 2; d++)
                        for (i=0; i < 4; i++)
                            ldiff[d][i] = ABS(lab[d][tr][tc][0]-lab[d][tr][tc+dir[i]][0]);
                    leps = MIN(MAX(ldiff[0][0],ldiff[0][1]),
                               MAX(ldiff[1][2],ldiff[1][3]));
                    for (d=0; d < 2; d++)
                        for (i=0; i < 4; i++)
                            if (i >> 1 == d || ldiff[d][i] <= leps)
                                abdiff[d][i] = SQR(lab[d][tr][tc][1]-lab[d][tr][tc+dir[i]][1])
                                    + SQR(lab[d][tr][tc][2]-lab[d][tr][tc+dir[i]][2]);
                    abeps = MIN(MAX(abdiff[0][0],abdiff[0][1]),
                                MAX(abdiff[1][2],abdiff[1][3]));
                    for (d=0; d < 2; d++)
                        for (i=0; i < 4; i++)
                            if (ldiff[d][i] <= leps && abdiff[d][i] <= abeps)
                                homo[d][tr][tc]++;
                }
            }
            /*  Combine the most homogenous pixels for the final result:        */
            for (row=top+3; row < top+TS-3 && row < height-3; row++) {
                tr = row-top;
                for (col=left+3; col < left+TS-3 && col < width-3; col++) {
                    tc = col-left;
                    for (d=0; d < 2; d++)
                        for (hm[d]=0, i=tr-1; i <= tr+1; i++)
                            for (j=tc-1; j <= tc+1; j++)
                                hm[d] += homo[d][i][j];
                    if (hm[0] != hm[1])
                        FORC3 dst[(row*width+col)*3 + c] = CLIPOUT(rgb[hm[1] > hm[0]][tr][tc][c]); /* [SA] */
                    else
                        FORC3 dst[(row*width+col)*3 + c] =
                            CLIPOUT((rgb[0][tr][tc][c] + rgb[1][tr][tc][c]) >> 1);      /* [SA] */
                }
            }
        }
    g_free (buffer);
}


static void
ol_bayer_ahd_decode_uint16(const guint16 *restrict bayer,
                           guint16 *restrict dst,
                           gsize sx,
                           gsize sy,
                           OLBayerFilter filter,
                           int bits)
{
    int i, j, top, left, row, col, tr, tc, fc, c, d, val, hm[2];
    /* the following has the same type as the image */
    guint16 (*pix)[3], (*rix)[3];      /* [SA] */
    static const int dir[4] = { -1, 1, -TS, TS };
    unsigned ldiff[2][4], abdiff[2][4], leps, abeps;
    double flab[3];
    guint16 (*rgb)[TS][TS][3];         /* [SA] */
    short (*lab)[TS][TS][3];
    char (*homo)[TS][TS], *buffer;

    /* start - new code for libdc1394 */
    guint32 filters;
    const int height = sy, width = sx;
    int x, y;

    if (g_once_init_enter (&ahd_inited)) {
        cam_to_cielab (NULL,NULL);
        g_once_init_leave(&ahd_inited, 1);
    }

    switch(filter) {
    case OL_BAYER_FILTER_BGGR:
        filters = 0x16161616;
        break;
    case OL_BAYER_FILTER_GRBG:
        filters = 0x61616161;
        break;
    case OL_BAYER_FILTER_RGGB:
        filters = 0x94949494;
        break;
    case OL_BAYER_FILTER_GBRG:
        filters = 0x49494949;
        break;
    default:
        g_assert_not_reached();
    }

    /* fill-in destination with known exact values */
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            int channel = FC(y,x);
            dst[(y*width+x)*3 + channel] = bayer[y*width+x];
        }
    }
    /* end - new code for libdc1394 */

    /* start - code from border_interpolate(int border) */
    {
        int border = 3;
        unsigned urow, ucol, uy, ux, f, uc, sum[8];

        for (urow=0; urow < height; urow++)
            for (ucol=0; ucol < width; ucol++) {
                if (ucol==border && urow >= border && urow < height-border)
                    ucol = width-border;
                memset (sum, 0, sizeof sum);
                for (uy=urow-1; uy != urow+2; uy++)
                    for (ux=ucol-1; ux != ucol+2; ux++)
                        if (uy < height && ux < width) {
                            f = FC(uy,ux);
                            sum[f] += dst[(uy*width+ux)*3 + f];           /* [SA] */
                            sum[f+4]++;
                        }
                f = FC(urow,ucol);
                FORUC3 if (uc != f && sum[uc+4])                     /* [SA] */
                    dst[(urow*width+ucol)*3 + uc] = sum[uc] / sum[uc+4]; /* [SA] */
            }
    }
    /* end - code from border_interpolate(int border) */


    buffer = (char *) g_malloc (26*TS*TS);                /* 1664 kB */
    /* merror (buffer, "ahd_interpolate()"); */
    rgb  = (guint16(*)[TS][TS][3]) buffer;               /* [SA] */
    lab  = (short (*)[TS][TS][3])(buffer + 12*TS*TS);
    homo = (char  (*)[TS][TS])   (buffer + 24*TS*TS);

    for (top=0; top < height; top += TS-6)
        for (left=0; left < width; left += TS-6) {
            memset (rgb, 0, 12*TS*TS);

            /*  Interpolate green horizontally and vertically:                */
            for (row = top < 2 ? 2:top; row < top+TS && row < height-2; row++) {
                col = left + (FC(row,left) == 1);
                if (col < 2) col += 2;
                for (fc = FC(row,col); col < left+TS && col < width-2; col+=2) {
                    pix = (guint16 (*)[3])dst + (row*width+col);          /* [SA] */
                    val = ((pix[-1][1] + pix[0][fc] + pix[1][1]) * 2
                           - pix[-2][fc] - pix[2][fc]) >> 2;
                    rgb[0][row-top][col-left][1] = ULIM(val,pix[-1][1],pix[1][1]);
                    val = ((pix[-width][1] + pix[0][fc] + pix[width][1]) * 2
                           - pix[-2*width][fc] - pix[2*width][fc]) >> 2;
                    rgb[1][row-top][col-left][1] = ULIM(val,pix[-width][1],pix[width][1]);
                }
            }
            /*  Interpolate red and blue, and convert to CIELab:                */
            for (d=0; d < 2; d++)
                for (row=top+1; row < top+TS-1 && row < height-1; row++)
                    for (col=left+1; col < left+TS-1 && col < width-1; col++) {
                        pix = (guint16 (*)[3])dst + (row*width+col);        /* [SA] */
                        rix = &rgb[d][row-top][col-left];
                        if ((c = 2 - FC(row,col)) == 1) {
                            c = FC(row+1,col);
                            val = pix[0][1] + (( pix[-1][2-c] + pix[1][2-c]
                                                 - rix[-1][1] - rix[1][1] ) >> 1);
                            rix[0][2-c] = CLIPOUT16(val, bits); /* [SA] */
                            val = pix[0][1] + (( pix[-width][c] + pix[width][c]
                                                 - rix[-TS][1] - rix[TS][1] ) >> 1);
                        } else
                            val = rix[0][1] + (( pix[-width-1][c] + pix[-width+1][c]
                                                 + pix[+width-1][c] + pix[+width+1][c]
                                                 - rix[-TS-1][1] - rix[-TS+1][1]
                                                 - rix[+TS-1][1] - rix[+TS+1][1] + 1) >> 2);
                        rix[0][c] = CLIPOUT16(val, bits);     /* [SA] */
                        c = FC(row,col);
                        rix[0][c] = pix[0][c];
                        cam_to_cielab (rix[0], flab);
                        FORC3 lab[d][row-top][col-left][c] = 64*flab[c];
                    }
            /*  Build homogeneity maps from the CIELab images:                */
            memset (homo, 0, 2*TS*TS);
            for (row=top+2; row < top+TS-2 && row < height; row++) {
                tr = row-top;
                for (col=left+2; col < left+TS-2 && col < width; col++) {
                    tc = col-left;
                    for (d=0; d < 2; d++)
                        for (i=0; i < 4; i++)
                            ldiff[d][i] = ABS(lab[d][tr][tc][0]-lab[d][tr][tc+dir[i]][0]);
                    leps = MIN(MAX(ldiff[0][0],ldiff[0][1]),
                               MAX(ldiff[1][2],ldiff[1][3]));
                    for (d=0; d < 2; d++)
                        for (i=0; i < 4; i++)
                            if (i >> 1 == d || ldiff[d][i] <= leps)
                                abdiff[d][i] = SQR(lab[d][tr][tc][1]-lab[d][tr][tc+dir[i]][1])
                                    + SQR(lab[d][tr][tc][2]-lab[d][tr][tc+dir[i]][2]);
                    abeps = MIN(MAX(abdiff[0][0],abdiff[0][1]),
                                MAX(abdiff[1][2],abdiff[1][3]));
                    for (d=0; d < 2; d++)
                        for (i=0; i < 4; i++)
                            if (ldiff[d][i] <= leps && abdiff[d][i] <= abeps)
                                homo[d][tr][tc]++;
                }
            }
            /*  Combine the most homogenous pixels for the final result:        */
            for (row=top+3; row < top+TS-3 && row < height-3; row++) {
                tr = row-top;
                for (col=left+3; col < left+TS-3 && col < width-3; col++) {
                    tc = col-left;
                    for (d=0; d < 2; d++)
                        for (hm[d]=0, i=tr-1; i <= tr+1; i++)
                            for (j=tc-1; j <= tc+1; j++)
                                hm[d] += homo[d][i][j];
                    if (hm[0] != hm[1])
                        FORC3 dst[(row*width+col)*3 + c] = CLIPOUT16(rgb[hm[1] > hm[0]][tr][tc][c], bits); /* [SA] */
                    else
                        FORC3 dst[(row*width+col)*3 + c] =
                            CLIPOUT16((rgb[0][tr][tc][c] + rgb[1][tr][tc][c]) >> 1, bits); /* [SA] */
                }
            }
        }
    g_free (buffer);
}
