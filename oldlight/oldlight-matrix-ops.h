/*
 * oldlight-matrix-ops.h: matrix processing
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#define OL_SPLICE_I(a, b) a ## b
#define OL_SPLICE(a, b) OL_SPLICE_I(a, b)
#define OL_SWAP(a,b) do { OL_MATRIX_TYPE t = a; a = b; b = t; } while (0)

static void OL_SPLICE(ol_matrix_copy_region_, OL_MATRIX_TYPE)(OLMatrix *src,
                                                              OLMatrix *dst,
                                                              gsize x,
                                                              gsize y,
                                                              gsize width,
                                                              gsize height)
{
    gsize i;
    for (i = 0; i < height; i++) {
        OL_MATRIX_TYPE *srcdata = src->data.OL_MATRIX_FIELD + (src->width * (i + y)) + x;
        OL_MATRIX_TYPE *dstdata = dst->data.OL_MATRIX_FIELD + (dst->width * i);
        memcpy(dstdata, srcdata, sizeof(OL_MATRIX_TYPE) * width);
    }
}

static void OL_SPLICE(ol_matrix_copy_with_border_, OL_MATRIX_TYPE)(OLMatrix *src,
                                                                   OLMatrix *dst,
                                                                   gsize left,
                                                                   gsize top)
{
    gsize i;
    for (i = 0; i < src->height; i++) {
        OL_MATRIX_TYPE *srcdata = src->data.OL_MATRIX_FIELD + (src->width * i);
        OL_MATRIX_TYPE *dstdata = dst->data.OL_MATRIX_FIELD + (dst->width * (i + top)) + left;
        memcpy(dstdata, srcdata, sizeof(OL_MATRIX_TYPE) * src->width);
    }
}

static void OL_SPLICE(ol_matrix_clear_border_, OL_MATRIX_TYPE)(OLMatrix *matrix,
                                                               gsize left,
                                                               gsize right,
                                                               gsize top,
                                                               gsize bottom)
{
    gsize i;
    for (i = top; i < (matrix->height - bottom); i++) {
        OL_MATRIX_TYPE *srcdata = matrix->data.OL_MATRIX_FIELD + (matrix->width * i);
        memset(srcdata, 0, sizeof(OL_MATRIX_TYPE) * left);
        memset(srcdata + matrix->width - right, 0, sizeof(OL_MATRIX_TYPE) * right);
    }
    for (i = 0; i < top; i++) {
        OL_MATRIX_TYPE *srcdata = matrix->data.OL_MATRIX_FIELD + (matrix->width * i);
        memset(srcdata, 0, sizeof(OL_MATRIX_TYPE) * matrix->width);
    }
    for (i = matrix->height - bottom; i < matrix->height; i++) {
        OL_MATRIX_TYPE *srcdata = matrix->data.OL_MATRIX_FIELD + (matrix->width * i);
        memset(srcdata, 0, sizeof(OL_MATRIX_TYPE) * matrix->width);
    }
}

static void OL_SPLICE(ol_matrix_abs_, OL_MATRIX_TYPE)(OLMatrix *matrix)
{
    OL_MATRIX_TYPE *data = matrix->data.OL_MATRIX_FIELD;
    gsize i;
    for (i = 0; i < matrix->len; i++) {
        *data = OL_ABS(*data);
        data++;
    }
}

static gdouble OL_SPLICE(ol_matrix_sum_, OL_MATRIX_TYPE)(OLMatrix *matrix)
{
    gdouble sum = 0.0;
    OL_MATRIX_TYPE *data = matrix->data.OL_MATRIX_FIELD;
    gsize i;
    for (i = 0; i < matrix->len; i++) {
        sum += (gdouble)*data;
        data++;
    }
    return sum;
}

static gdouble OL_SPLICE(ol_matrix_sum_abs_, OL_MATRIX_TYPE)(OLMatrix *matrix)
{
    gdouble sum = 0.0;
    OL_MATRIX_TYPE *data = matrix->data.OL_MATRIX_FIELD;
    gsize i;
    for (i = 0; i < matrix->len; i++) {
        sum += OL_ABS(*data);
        data++;
    }
    return sum;
}

static gdouble OL_SPLICE(ol_matrix_sum_masked_, OL_MATRIX_TYPE)(OLMatrix *matrix,
                                                                OLMatrix *mask)
{
    gdouble sum = 0.0;
    OL_MATRIX_TYPE *data = matrix->data.OL_MATRIX_FIELD;
    guint8 *on = mask->data.u8;
    gsize i;
    for (i = 0; i < matrix->len; i++) {
        if (*on)
            sum += (gdouble)*data;
        data++;
        on++;
    }
    return sum;
}

static void OL_SPLICE(ol_matrix_sum_xaxis_, OL_MATRIX_TYPE)(OLMatrix *matrix,
                                                            OLMatrix *output)
{
    OL_MATRIX_TYPE *src = matrix->data.OL_MATRIX_FIELD;
    OL_MATRIX_TYPE *dst = output->data.OL_MATRIX_FIELD;
    gsize x, y;
    for (y = 0; y < matrix->height; y++) {
        for (x = 0; x < matrix->width; x++) {
            dst[x] += src[y * matrix->width + x];
        }
    }
}

static void OL_SPLICE(ol_matrix_sum_yaxis_, OL_MATRIX_TYPE)(OLMatrix *matrix,
                                                            OLMatrix *output)
{
    OL_MATRIX_TYPE *src = matrix->data.OL_MATRIX_FIELD;
    OL_MATRIX_TYPE *dst = output->data.OL_MATRIX_FIELD;
    gsize x, y;
    for (y = 0; y < matrix->height; y++) {
        for (x = 0; x < matrix->width; x++) {
            dst[y] += src[y * matrix->width + x];
        }
    }
}

static gdouble OL_SPLICE(ol_matrix_median_, OL_MATRIX_TYPE)(OLMatrix *matrix)
{
    OLMatrix *tmp = ol_matrix_copy(matrix);
    gsize low = 0;
    gsize high = matrix->len - 1;
    gsize median = (low + high) / 2;
    gsize middle;
    gsize ll;
    gsize hh;
    gdouble ret;
    OL_MATRIX_TYPE *data = tmp->data.OL_MATRIX_FIELD;

    for (;;) {
        if (high <= low) { /* One element only */
            goto cleanup;
        }
        if (high == low + 1) { /* Two elements only */
            if (data[low] > data[high])
                OL_SWAP(data[low], data[high]);

            goto cleanup;
        }

        /* Find median of low, middle and high items; swap into position low */
        middle = (low + high) / 2;
        if (data[middle] > data[high])
            OL_SWAP(data[middle], data[high]);

        if (data[low] > data[high])
            OL_SWAP(data[low], data[high]);

        if (data[middle] > data[low])
            OL_SWAP(data[middle], data[low]);

        /* Swap low item (now in position middle) into position (low+1) */
        OL_SWAP(data[middle], data[low+1]);

        /* Nibble from each end towards middle, swapping items when stuck */
        ll = low + 1;
        hh = high;
        for (;;) {
            do {
                ll++;
            } while (data[low] > data[ll]);

            do {
                hh--;
            } while (data[hh] > data[low]);

            if (hh < ll)
                break;

            OL_SWAP(data[ll], data[hh]);
        }

        /* Swap middle item (in position low) back into correct position */
        OL_SWAP(data[low], data[hh]);

        /* Re-set active partition */
        if (hh <= median)
            low = ll;
        if (hh >= median)
            high = hh - 1;
    }

 cleanup:
    ret = data[median];
    ol_matrix_free(tmp);
    return ret;
}

static gdouble OL_SPLICE(ol_matrix_maximum_, OL_MATRIX_TYPE)(OLMatrix *matrix)
{
    OL_MATRIX_TYPE *src = matrix->data.OL_MATRIX_FIELD;
    OL_MATRIX_TYPE max = 0;
    gsize x, y;
    for (y = 0; y < matrix->height; y++) {
        for (x = 0; x < matrix->width; x++) {
            OL_MATRIX_TYPE val = src[y * matrix->width + x];
            if (val > max)
                max = val;
        }
    }
    return max;
}

static void OL_SPLICE(ol_matrix_maximum_filter_, OL_MATRIX_TYPE)(OLMatrix *input,
                                                                 OLMatrix *mask,
                                                                 OLMatrix *output)
{
    gssize x, y, i, j;
    int mask_yrad = (int)floor(mask->height / 2.0);
    int mask_xrad = (int)floor(mask->width / 2.0);
    gsize *offsets = g_new0(gsize, mask->width * mask->height);
    gsize noffsets = 0;

    /* Pre-compute offsets, so we don't need todo any real math
     * in the innermost loop */
    for (j = 0; j < mask->height; j++) {
        for (i = 0; i < mask->width; i++) {
            if (mask->data.u8[(j * mask->width) + i]) {
                offsets[noffsets++] = (i - mask_xrad) +
                    ((j - mask_yrad) * input->width);
            }
        }
    }

    OL_MATRIX_TYPE *dtmp = input->data.OL_MATRIX_FIELD + (mask_yrad * input->width) + mask_xrad;
    OL_MATRIX_TYPE *otmp = output->data.OL_MATRIX_FIELD + (mask_yrad * output->width) + mask_xrad;
    for (y = mask_yrad; y < input->height - mask_yrad; y++) {
        for (x = mask_xrad; x < input->width - mask_xrad; x++) {
            OL_MATRIX_TYPE max = 0.0;
            gsize *oo = offsets;

            for (i = 0; i < noffsets; i++) {
                OL_MATRIX_TYPE dval = dtmp[*oo];
                oo++;
                if (dval > max)
                    max = dval;
            }
            *otmp = max;
            dtmp++;
            otmp++;
        }
        dtmp += mask_xrad + mask_xrad;
        otmp += mask_xrad + mask_xrad;
    }
    g_free(offsets);
}

static void OL_SPLICE(ol_matrix_convolve_, OL_MATRIX_TYPE)(OLMatrix *input,
                                                           OLMatrix *kern,
                                                           OLMatrix *output)
{
    gssize x, y;
    gssize i, j;
    int kern_xrad = (int)floor(kern->width / 2.0);
    int kern_yrad = (int)floor(kern->height / 2.0);
    gsize *offsets = g_new0(gsize, kern->len);
    gsize noffsets = 0;

    /* Pre-compute offsets, so we don't need todo any real math
     * in the innermost loop */
    for (j = 0; j < kern->height; j++) {
        for (i = 0; i < kern->width; i++) {
            offsets[noffsets++] = (i - kern_xrad) +
                ((j - kern_yrad) * input->width);
        }
    }

    OL_MATRIX_TYPE *dtmp = input->data.OL_MATRIX_FIELD + (kern_yrad * input->width) + kern_xrad;
    double *otmp = output->data.f64 + (kern_yrad * input->width) + kern_xrad;
    for (y = kern_yrad; y < input->height - kern_yrad; y++) {
        for (x = kern_xrad; x < input->width - kern_xrad; x++) {
            gdouble sum = 0.0;

            gdouble *tmp_kern = kern->data.f64;
            gsize *oo = offsets;

            for (i = 0; i < noffsets; i++) {
                OL_MATRIX_TYPE dval = dtmp[*oo];
                sum += (gdouble)dval * *tmp_kern;
                oo++;
                tmp_kern++;
            }
            *otmp = sum;
            otmp++;
            dtmp++;
        }
        dtmp += kern_xrad + kern_xrad;
        otmp += kern_xrad + kern_xrad;
    }

    g_free(offsets);
}

static void OL_SPLICE(ol_matrix_product_, OL_MATRIX_TYPE)(OLMatrix *matrix,
                                                          OLMatrix *other,
                                                          OLMatrix *output)
{
    gsize x, y, z;

    for (y = 0; y < matrix->height; y++) {
        for (x = 0; x < other->height; x++) {
            OL_MATRIX_TYPE sum = 0;
            for (z = 0; z < matrix->width; z++) {
                sum +=
                    *(matrix->data.OL_MATRIX_FIELD + (y * matrix->width) + z) *
                    *(other->data.OL_MATRIX_FIELD + (z * other->width) + x);
            }
            *(output->data.OL_MATRIX_FIELD + (y * output->width) + x) = sum;
        }
    }
}


static void OL_SPLICE(ol_matrix_multiply_, OL_MATRIX_TYPE)(OLMatrix *matrix,
                                                           OLMatrix *other,
                                                           OLMatrix *output)
{
    gsize i;

    for (i = 0; i < matrix->len; i++) {
        *(output->data.OL_MATRIX_FIELD + i) =
            *(matrix->data.OL_MATRIX_FIELD + i) *
            *(other->data.OL_MATRIX_FIELD + i);
    }
}


static const ol_matrix_ops OL_SPLICE(ol_matrix_ops_, OL_MATRIX_TYPE) = {
    .copy_region = OL_SPLICE(ol_matrix_copy_region_, OL_MATRIX_TYPE),
    .copy_with_border = OL_SPLICE(ol_matrix_copy_with_border_, OL_MATRIX_TYPE),
    .clear_border = OL_SPLICE(ol_matrix_clear_border_, OL_MATRIX_TYPE),
    .abs = OL_SPLICE(ol_matrix_abs_, OL_MATRIX_TYPE),
    .sum = OL_SPLICE(ol_matrix_sum_, OL_MATRIX_TYPE),
    .sum_abs = OL_SPLICE(ol_matrix_sum_abs_, OL_MATRIX_TYPE),
    .sum_masked = OL_SPLICE(ol_matrix_sum_masked_, OL_MATRIX_TYPE),
    .sum_xaxis = OL_SPLICE(ol_matrix_sum_xaxis_, OL_MATRIX_TYPE),
    .sum_yaxis = OL_SPLICE(ol_matrix_sum_yaxis_, OL_MATRIX_TYPE),
    .median = OL_SPLICE(ol_matrix_median_, OL_MATRIX_TYPE),
    .maximum = OL_SPLICE(ol_matrix_maximum_, OL_MATRIX_TYPE),
    .maximum_filter = OL_SPLICE(ol_matrix_maximum_filter_, OL_MATRIX_TYPE),
    .convolve = OL_SPLICE(ol_matrix_convolve_, OL_MATRIX_TYPE),
    .product = OL_SPLICE(ol_matrix_product_, OL_MATRIX_TYPE),
    .multiply = OL_SPLICE(ol_matrix_multiply_, OL_MATRIX_TYPE),
};

#undef OL_SWAP
#undef OL_OL_SPLICE_I
#undef OL_OL_SPLICE
