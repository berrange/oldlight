/*
 * oldlight-image-reader.c: image reader
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-reader.h"


G_DEFINE_TYPE(OLImageReader, ol_image_reader, OL_TYPE_IMAGE_IO);


static void ol_image_reader_class_init(OLImageReaderClass *klass G_GNUC_UNUSED)
{

}


static void ol_image_reader_init(OLImageReader *reader G_GNUC_UNUSED)
{
}


OLImage *ol_image_reader_load(OLImageReader *reader,
                              GError **err)
{
    OLImage *image;
    if (!ol_image_io_open(OL_IMAGE_IO(reader), err))
        return FALSE;
    image = ol_image_reader_load_payload(reader, 0, 0,
                                         ol_image_io_get_width(OL_IMAGE_IO(reader)),
                                         ol_image_io_get_height(OL_IMAGE_IO(reader)),
                                         err);
    if (!ol_image_io_close(OL_IMAGE_IO(reader), image ? err : NULL)) {
        if (image) {
            g_object_unref(image);
            image = NULL;
        }
    }
    return image;
}


OLImage *ol_image_reader_load_payload(OLImageReader *reader,
                                      guint x, guint y,
                                      guint width, guint height,
                                      GError **err)
{
    return OL_IMAGE_READER_GET_CLASS(reader)->load_payload(reader,
                                                           x, y,
                                                           width, height,
                                                           err);
}
