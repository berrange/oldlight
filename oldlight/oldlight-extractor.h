/*
 * oldlight-extractor.h: detection of stars
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_EXTRACTOR_H__
#define OL_EXTRACTOR_H__

#include <glib-object.h>

#include "oldlight/oldlight-image.h"
#include "oldlight/oldlight-catalog.h"

G_BEGIN_DECLS

#define OL_TYPE_EXTRACTOR            (ol_extractor_get_type ())

G_DECLARE_FINAL_TYPE(OLExtractor, ol_extractor, OL, EXTRACTOR, GObject);

OLExtractor *ol_extractor_new(void);

OLCatalog *ol_extractor_find_stars_daofind(OLExtractor *ext,
                                           OLImage *image,
                                           double threshold,
                                           double fwhm,
                                           double ratio,
                                           double theta,
                                           double sigma_radius,
                                           double sharplo,
                                           double sharphi,
                                           double roundlo,
                                           double roundhi,
                                           double sky,
                                           gboolean exclude_border);

G_END_DECLS

#endif /* OL_EXTRACTOR_H__ */
