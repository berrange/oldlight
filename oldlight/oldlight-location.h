/*
 * oldlight-location.h: position in image
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_LOCATION_H__
#define OL_LOCATION_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define OL_TYPE_LOCATION            (ol_location_get_type ())

typedef struct _OLLocation OLLocation;

struct _OLLocation
{
    gdouble x;
    gdouble y;
};

GType ol_location_get_type(void);

OLLocation *ol_location_new(gdouble x,
                            gdouble y);

OLLocation *ol_location_copy(OLLocation *location);

void ol_location_free(OLLocation *location);

gboolean ol_location_equal(OLLocation *location,
                           OLLocation *other);

gdouble ol_location_distance(OLLocation *location,
                             OLLocation *other);

G_END_DECLS

#endif /* OL_LOCATION_H__ */
