/*
 * oldlight-bayer.h: bayer processing
 *
 * This file is derived from bayer.c in libdc1394,
 * written by Damien Douxchamps and Frederic Devernay
 * The original VNG and AHD Bayer decoding are from Dave Coffin's DCRAW.
 *
 * The oldlight changes are:
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 *
 */

#include "oldlight/oldlight-bayer-nearest-neighbour.h"
#include "oldlight/oldlight-bayer-simple.h"
#include "oldlight/oldlight-bayer-bilinear.h"
#include "oldlight/oldlight-bayer-hqlinear.h"
#include "oldlight/oldlight-bayer-downsample.h"
#include "oldlight/oldlight-bayer-edge-sense.h"
#include "oldlight/oldlight-bayer-vng.h"
#include "oldlight/oldlight-bayer-ahd.h"

#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


typedef struct _OLBayerPrivate OLBayerPrivate;
struct _OLBayerPrivate
{
    GObject parent;
    OLBayerFilter filter;
};

G_DEFINE_TYPE_WITH_PRIVATE(OLBayer, ol_bayer, G_TYPE_OBJECT);

#define OL_BAYER_ERROR ol_bayer_error_quark()

static GQuark
ol_bayer_error_quark(void)
{
    return g_quark_from_static_string("ol-bayer");
}


enum {
    PROP_0,
    PROP_FILTER,
};


static void ol_bayer_get_property(GObject *object,
                                  guint prop_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
    OLBayer *bayer = OL_BAYER(object);
    OLBayerPrivate *priv = ol_bayer_get_instance_private(bayer);

    switch (prop_id) {
    case PROP_FILTER:
        g_value_set_enum(value, priv->filter);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_bayer_set_property(GObject *object,
                                  guint prop_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
    OLBayer *bayer = OL_BAYER(object);
    OLBayerPrivate *priv = ol_bayer_get_instance_private(bayer);

    switch (prop_id) {
    case PROP_FILTER:
        priv->filter = g_value_get_enum(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_bayer_class_init(OLBayerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->get_property = ol_bayer_get_property;
    object_class->set_property = ol_bayer_set_property;

    g_object_class_install_property(object_class,
                                    PROP_FILTER,
                                    g_param_spec_enum("filter",
                                                      "Filter",
                                                      "Pixel filter",
                                                      OL_TYPE_BAYER_FILTER,
                                                      OL_BAYER_FILTER_RGGB,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_CONSTRUCT_ONLY |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));
}


static void ol_bayer_init(OLBayer *bayer G_GNUC_UNUSED)
{
}


OLBayerFilter ol_bayer_get_filter(OLBayer *bayer)
{
    OLBayerPrivate *priv = ol_bayer_get_instance_private(bayer);

    return priv->filter;
}


OLImage *ol_bayer_decode(OLBayer *bayer, OLImage *src, GError **err)
{
    OLBayerPrivate *priv = ol_bayer_get_instance_private(bayer);

    if (src->channels != OL_IMAGE_CHANNELS_GRAY) {
        g_set_error(err, OL_BAYER_ERROR, 0,
                    "Source image for bayer decode should be gray scale");
        return NULL;
    }

    if (src->matrix->format == OL_MATRIX_FORMAT_UINT8) {
        OLImage *ret = ol_image_new(OL_MATRIX_FORMAT_UINT8,
                                    OL_IMAGE_CHANNELS_RGB,
                                    src->width,
                                    src->height);
        OL_BAYER_GET_CLASS(bayer)->decode8(src->matrix->data.u8,
                                           ret->matrix->data.u8,
                                           src->width,
                                           src->height,
                                           priv->filter);
        return ret;
    } else if (src->matrix->format == OL_MATRIX_FORMAT_UINT16) {
        OLImage *ret = ol_image_new(OL_MATRIX_FORMAT_UINT16,
                                    OL_IMAGE_CHANNELS_RGB,
                                    src->width,
                                    src->height);
        OL_BAYER_GET_CLASS(bayer)->decode16(src->matrix->data.u16,
                                            ret->matrix->data.u16,
                                            src->width,
                                            src->height,
                                            priv->filter,
                                            16);
        return ret;
    } else {
        g_set_error(err, OL_BAYER_ERROR, 0,
                    "Source image for bayer decode should be uint8/uint16");
        return NULL;
    }
}


OLBayer *ol_bayer_new(OLBayerFilter filter,
                      OLBayerMethod method)
{
    GType typ;

    switch (method) {
    case OL_BAYER_METHOD_NEAREST_NEIGHBOUR:
        typ = OL_TYPE_BAYER_NEAREST_NEIGHBOUR;
        break;

    case OL_BAYER_METHOD_SIMPLE:
        typ = OL_TYPE_BAYER_SIMPLE;
        break;

    case OL_BAYER_METHOD_BILINEAR:
        typ = OL_TYPE_BAYER_BILINEAR;
        break;

    case OL_BAYER_METHOD_HQLINEAR:
        typ = OL_TYPE_BAYER_HQLINEAR;
        break;

    case OL_BAYER_METHOD_DOWNSAMPLE:
        typ = OL_TYPE_BAYER_DOWNSAMPLE;
        break;

    case OL_BAYER_METHOD_EDGE_SENSE:
        typ = OL_TYPE_BAYER_EDGE_SENSE;
        break;

    case OL_BAYER_METHOD_VNG:
        typ = OL_TYPE_BAYER_VNG;
        break;

    case OL_BAYER_METHOD_AHD:
        typ = OL_TYPE_BAYER_AHD;
        break;

    default:
        g_assert_not_reached();
    }

    return OL_BAYER(g_object_new(typ,
                                 "filter", filter,
                                 NULL));
}
