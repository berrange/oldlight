/*
 * oldlight-catalog.h: list of stars
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_CATALOG_H__
#define OL_CATALOG_H__

#include <glib-object.h>

#include "oldlight/oldlight-star.h"
#include "oldlight/oldlight-identity.h"
#include "oldlight/oldlight-image.h"

G_BEGIN_DECLS

#define OL_TYPE_CATALOG            (ol_catalog_get_type ())

G_DECLARE_FINAL_TYPE(OLCatalog, ol_catalog, OL, CATALOG, GObject);

OLCatalog *ol_catalog_new(gsize width,
                          gsize height);

gsize ol_catalog_get_nstars(OLCatalog *catalog);

OLStar *ol_catalog_get_star(OLCatalog *catalog, gsize i);

OLStar **ol_catalog_get_stars(OLCatalog *catalog);

void ol_catalog_add_star(OLCatalog *catalog,
                         OLStar *star);

void ol_catalog_sort_flux(OLCatalog *catalog, gboolean asc);

void ol_catalog_render(OLCatalog *catalog,
                       OLImage *image);

typedef gboolean (*OLCatalogFilter)(OLStar *star, gpointer opaque);

OLCatalog *ol_catalog_copy(OLCatalog *catalog,
                           OLCatalogFilter filter,
                           gpointer opaque);

OLCatalog *ol_catalog_copy_transform(OLCatalog *catalog,
                                     OLTransform *transform);

gsize ol_catalog_make_quads(OLCatalog *catalog,
                            OLIdentity *identity,
                            gsize segments,
                            gsize maxstars,
                            gdouble mindist);

gboolean ol_catalog_save(OLCatalog *catalog,
                         const gchar *filename,
                         GError **error);

gboolean ol_catalog_load(OLCatalog *catalog,
                         const gchar *filename,
                         GError **error);

G_END_DECLS

#endif /* OL_CATALOG_H__ */
