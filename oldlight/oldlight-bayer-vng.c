/*
 * oldlight-bayer-vng.h: bayer processing vng algorithm
 *
 * This file is derived from bayer.c in libdc1394,
 * written by Damien Douxchamps and Frederic Devernay
 * The original VNG Bayer decoding is from Dave Coffin's DCRAW.
 *
 * The oldlight changes are:
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Variable Number of Gradients, from dcraw <http://www.cybercom.net/~dcoffin/dcraw/>
 *
 * This algorithm is officially called:
 *
 * "Interpolation using a Threshold-based variable number of gradients"
 *
 * described in http://www-ise.stanford.edu/~tingchen/algodep/vargra.html
 *
 * I've extended the basic idea to work with non-Bayer filter arrays.
 * Gradients are numbered clockwise from NW=0 to W=7.
 */

#include "oldlight/oldlight-bayer-vng.h"

#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


struct _OLBayerVNG
{
    OLBayerBilinear parent;
};

G_DEFINE_TYPE(OLBayerVNG, ol_bayer_vng, OL_TYPE_BAYER_BILINEAR);


static void
ol_bayer_vng_decode_uint8(const guint8 *restrict bayer,
                          guint8 *restrict dst,
                          gsize sx,
                          gsize sy,
                          OLBayerFilter filter);
static void
ol_bayer_vng_decode_uint16(const guint16 *restrict bayer,
                           guint16 *restrict dst,
                           gsize sx,
                           gsize sy,
                           OLBayerFilter filter,
                           int bits);


static void ol_bayer_vng_class_init(OLBayerVNGClass *klass)
{
    OLBayerClass *bayer_class = OL_BAYER_CLASS(klass);

    bayer_class->decode8 = ol_bayer_vng_decode_uint8;
    bayer_class->decode16 = ol_bayer_vng_decode_uint16;
}


static void ol_bayer_vng_init(OLBayerVNG *bayer G_GNUC_UNUSED)
{
}


#define CLIP(in, out)                           \
    in = in < 0 ? 0 : in;                       \
    in = in > 255 ? 255 : in;                   \
    out=in;

#define CLIP16(in, out, bits)                           \
    in = in < 0 ? 0 : in;                               \
    in = in > ((1<<bits)-1) ? ((1<<bits)-1) : in;       \
    out=in;

/*
  In order to inline this calculation, I make the risky
  assumption that all filter patterns can be described
  by a repeating pattern of eight rows and two columns

  Return values are either 0/1/2/3 = G/M/C/Y or 0/1/2/3 = R/G1/B/G2
*/
#define FC(row,col)                                             \
    (filters >> ((((row) << 1 & 14) + ((col) & 1)) << 1) & 3)

static const signed char bayervng_terms[] = {
    -2,-2,+0,-1,0,0x01, -2,-2,+0,+0,1,0x01, -2,-1,-1,+0,0,0x01,
    -2,-1,+0,-1,0,0x02, -2,-1,+0,+0,0,0x03, -2,-1,+0,+1,1,0x01,
    -2,+0,+0,-1,0,0x06, -2,+0,+0,+0,1,0x02, -2,+0,+0,+1,0,0x03,
    -2,+1,-1,+0,0,0x04, -2,+1,+0,-1,1,0x04, -2,+1,+0,+0,0,0x06,
    -2,+1,+0,+1,0,0x02, -2,+2,+0,+0,1,0x04, -2,+2,+0,+1,0,0x04,
    -1,-2,-1,+0,0,0x80, -1,-2,+0,-1,0,0x01, -1,-2,+1,-1,0,0x01,
    -1,-2,+1,+0,1,0x01, -1,-1,-1,+1,0,0x88, -1,-1,+1,-2,0,0x40,
    -1,-1,+1,-1,0,0x22, -1,-1,+1,+0,0,0x33, -1,-1,+1,+1,1,0x11,
    -1,+0,-1,+2,0,0x08, -1,+0,+0,-1,0,0x44, -1,+0,+0,+1,0,0x11,
    -1,+0,+1,-2,1,0x40, -1,+0,+1,-1,0,0x66, -1,+0,+1,+0,1,0x22,
    -1,+0,+1,+1,0,0x33, -1,+0,+1,+2,1,0x10, -1,+1,+1,-1,1,0x44,
    -1,+1,+1,+0,0,0x66, -1,+1,+1,+1,0,0x22, -1,+1,+1,+2,0,0x10,
    -1,+2,+0,+1,0,0x04, -1,+2,+1,+0,1,0x04, -1,+2,+1,+1,0,0x04,
    +0,-2,+0,+0,1,0x80, +0,-1,+0,+1,1,0x88, +0,-1,+1,-2,0,0x40,
    +0,-1,+1,+0,0,0x11, +0,-1,+2,-2,0,0x40, +0,-1,+2,-1,0,0x20,
    +0,-1,+2,+0,0,0x30, +0,-1,+2,+1,1,0x10, +0,+0,+0,+2,1,0x08,
    +0,+0,+2,-2,1,0x40, +0,+0,+2,-1,0,0x60, +0,+0,+2,+0,1,0x20,
    +0,+0,+2,+1,0,0x30, +0,+0,+2,+2,1,0x10, +0,+1,+1,+0,0,0x44,
    +0,+1,+1,+2,0,0x10, +0,+1,+2,-1,1,0x40, +0,+1,+2,+0,0,0x60,
    +0,+1,+2,+1,0,0x20, +0,+1,+2,+2,0,0x10, +1,-2,+1,+0,0,0x80,
    +1,-1,+1,+1,0,0x88, +1,+0,+1,+2,0,0x08, +1,+0,+2,-1,0,0x40,
    +1,+0,+2,+1,0,0x10
}, bayervng_chood[] = { -1,-1, -1,0, -1,+1, 0,+1, +1,+1, +1,0, +1,-1, 0,-1 };


static void
ol_bayer_vng_decode_uint8(const guint8 *restrict bayer,
                          guint8 *restrict dst,
                          gsize sx,
                          gsize sy,
                          OLBayerFilter filter)
{
    const int height = sy, width = sx;
    static const signed char *cp;
    /* the following has the same type as the image */
    guint8 (*brow[5])[3], *pix;          /* [FD] */
    int code[8][2][320], *ip, gval[8], gmin, gmax, sum[4];
    int row, col, x, y, x1, x2, y1, y2, t, weight, grads, color, diag;
    int g, diff, thold, num, c;
    guint32 filters;                     /* [FD] */

    /* first, use bilinear bayer decoding */
    OL_BAYER_CLASS(ol_bayer_vng_parent_class)->decode8(bayer, dst, sx, sy, filter);

    switch(filter) {
    case OL_BAYER_FILTER_BGGR:
        filters = 0x16161616;
        break;
    case OL_BAYER_FILTER_GRBG:
        filters = 0x61616161;
        break;
    case OL_BAYER_FILTER_RGGB:
        filters = 0x94949494;
        break;
    case OL_BAYER_FILTER_GBRG:
        filters = 0x49494949;
        break;
    default:
        g_assert_not_reached();
    }

    for (row=0; row < 8; row++) {                /* Precalculate for VNG */
        for (col=0; col < 2; col++) {
            ip = code[row][col];
            for (cp=bayervng_terms, t=0; t < 64; t++) {
                y1 = *cp++;  x1 = *cp++;
                y2 = *cp++;  x2 = *cp++;
                weight = *cp++;
                grads = *cp++;
                color = FC(row+y1,col+x1);
                if (FC(row+y2,col+x2) != color) continue;
                diag = (FC(row,col+1) == color && FC(row+1,col) == color) ? 2:1;
                if (abs(y1-y2) == diag && abs(x1-x2) == diag) continue;
                *ip++ = (y1*width + x1)*3 + color; /* [FD] */
                *ip++ = (y2*width + x2)*3 + color; /* [FD] */
                *ip++ = weight;
                for (g=0; g < 8; g++)
                    if (grads & 1<<g) *ip++ = g;
                *ip++ = -1;
            }
            *ip++ = INT_MAX;
            for (cp=bayervng_chood, g=0; g < 8; g++) {
                y = *cp++;  x = *cp++;
                *ip++ = (y*width + x) * 3;      /* [FD] */
                color = FC(row,col);
                if (FC(row+y,col+x) != color && FC(row+y*2,col+x*2) == color)
                    *ip++ = (y*width + x) * 6 + color; /* [FD] */
                else
                    *ip++ = 0;
            }
        }
    }
    brow[4] = g_malloc0 (width*3 * sizeof **brow);
    //merror (brow[4], "vng_interpolate()");
    for (row=0; row < 3; row++)
        brow[row] = brow[4] + row*width;
    for (row=2; row < height-2; row++) {                /* Do VNG interpolation */
        for (col=2; col < width-2; col++) {
            pix = dst + (row*width+col)*3;        /* [FD] */
            ip = code[row & 7][col & 1];
            memset (gval, 0, sizeof gval);
            while ((g = ip[0]) != INT_MAX) {                /* Calculate gradients */
                diff = ABS(pix[g] - pix[ip[1]]) << ip[2];
                gval[ip[3]] += diff;
                ip += 5;
                if ((g = ip[-1]) == -1) continue;
                gval[g] += diff;
                while ((g = *ip++) != -1)
                    gval[g] += diff;
            }
            ip++;
            gmin = gmax = gval[0];                        /* Choose a threshold */
            for (g=1; g < 8; g++) {
                if (gmin > gval[g]) gmin = gval[g];
                if (gmax < gval[g]) gmax = gval[g];
            }
            if (gmax == 0) {
                memcpy (brow[2][col], pix, 3 * sizeof *dst); /* [FD] */
                continue;
            }
            thold = gmin + (gmax >> 1);
            memset (sum, 0, sizeof sum);
            color = FC(row,col);
            for (num=g=0; g < 8; g++,ip+=2) {                /* Average the neighbors */
                if (gval[g] <= thold) {
                    for (c=0; c < 3; c++)         /* [FD] */
                        if (c == color && ip[1])
                            sum[c] += (pix[c] + pix[ip[1]]) >> 1;
                        else
                            sum[c] += pix[ip[0] + c];
                    num++;
                }
            }
            for (c=0; c < 3; c++) {               /* [FD] Save to buffer */
                t = pix[color];
                if (c != color)
                    t += (sum[c] - sum[color]) / num;
                CLIP(t,brow[2][col][c]);          /* [FD] */
            }
        }
        if (row > 3)                                /* Write buffer to image */
            memcpy (dst + 3*((row-2)*width+2), brow[0]+2, (width-4)*3*sizeof *dst); /* [FD] */
        for (g=0; g < 4; g++)
            brow[(g-1) & 3] = brow[g];
    }
    memcpy (dst + 3*((row-2)*width+2), brow[0]+2, (width-4)*3*sizeof *dst);
    memcpy (dst + 3*((row-1)*width+2), brow[1]+2, (width-4)*3*sizeof *dst);
    g_free (brow[4]);
}


static void
ol_bayer_vng_decode_uint16(const guint16 *restrict bayer,
                           guint16 *restrict dst,
                           gsize sx,
                           gsize sy,
                           OLBayerFilter filter,
                           int bits)
{
    const int height = sy, width = sx;
    static const signed char *cp;
    /* the following has the same type as the image */
    guint16 (*brow[5])[3], *pix;          /* [FD] */
    int code[8][2][320], *ip, gval[8], gmin, gmax, sum[4];
    int row, col, x, y, x1, x2, y1, y2, t, weight, grads, color, diag;
    int g, diff, thold, num, c;
    guint32 filters;                     /* [FD] */

    /* first, use bilinear bayer decoding */
    OL_BAYER_CLASS(ol_bayer_vng_parent_class)->decode16(bayer, dst, sx, sy, filter, bits);

    switch(filter) {
    case OL_BAYER_FILTER_BGGR:
        filters = 0x16161616;
        break;
    case OL_BAYER_FILTER_GRBG:
        filters = 0x61616161;
        break;
    case OL_BAYER_FILTER_RGGB:
        filters = 0x94949494;
        break;
    case OL_BAYER_FILTER_GBRG:
        filters = 0x49494949;
        break;
    default:
        g_assert_not_reached();
    }

    for (row=0; row < 8; row++) {                /* Precalculate for VNG */
        for (col=0; col < 2; col++) {
            ip = code[row][col];
            for (cp=bayervng_terms, t=0; t < 64; t++) {
                y1 = *cp++;  x1 = *cp++;
                y2 = *cp++;  x2 = *cp++;
                weight = *cp++;
                grads = *cp++;
                color = FC(row+y1,col+x1);
                if (FC(row+y2,col+x2) != color) continue;
                diag = (FC(row,col+1) == color && FC(row+1,col) == color) ? 2:1;
                if (abs(y1-y2) == diag && abs(x1-x2) == diag) continue;
                *ip++ = (y1*width + x1)*3 + color; /* [FD] */
                *ip++ = (y2*width + x2)*3 + color; /* [FD] */
                *ip++ = weight;
                for (g=0; g < 8; g++)
                    if (grads & 1<<g) *ip++ = g;
                *ip++ = -1;
            }
            *ip++ = INT_MAX;
            for (cp=bayervng_chood, g=0; g < 8; g++) {
                y = *cp++;  x = *cp++;
                *ip++ = (y*width + x) * 3;      /* [FD] */
                color = FC(row,col);
                if (FC(row+y,col+x) != color && FC(row+y*2,col+x*2) == color)
                    *ip++ = (y*width + x) * 6 + color; /* [FD] */
                else
                    *ip++ = 0;
            }
        }
    }
    brow[4] = g_malloc0 (width*3* sizeof **brow);
    //merror (brow[4], "vng_interpolate()");
    for (row=0; row < 3; row++)
        brow[row] = brow[4] + row*width;
    for (row=2; row < height-2; row++) {                /* Do VNG interpolation */
        for (col=2; col < width-2; col++) {
            pix = dst + (row*width+col)*3;  /* [FD] */
            ip = code[row & 7][col & 1];
            memset (gval, 0, sizeof gval);
            while ((g = ip[0]) != INT_MAX) {                /* Calculate gradients */
                diff = ABS(pix[g] - pix[ip[1]]) << ip[2];
                gval[ip[3]] += diff;
                ip += 5;
                if ((g = ip[-1]) == -1) continue;
                gval[g] += diff;
                while ((g = *ip++) != -1)
                    gval[g] += diff;
            }
            ip++;
            gmin = gmax = gval[0];                        /* Choose a threshold */
            for (g=1; g < 8; g++) {
                if (gmin > gval[g]) gmin = gval[g];
                if (gmax < gval[g]) gmax = gval[g];
            }
            if (gmax == 0) {
                memcpy (brow[2][col], pix, 3 * sizeof *dst); /* [FD] */
                continue;
            }
            thold = gmin + (gmax >> 1);
            memset (sum, 0, sizeof sum);
            color = FC(row,col);
            for (num=g=0; g < 8; g++,ip+=2) {                /* Average the neighbors */
                if (gval[g] <= thold) {
                    for (c=0; c < 3; c++)         /* [FD] */
                        if (c == color && ip[1])
                            sum[c] += (pix[c] + pix[ip[1]]) >> 1;
                        else
                            sum[c] += pix[ip[0] + c];
                    num++;
                }
            }
            for (c=0; c < 3; c++) {           /* [FD] Save to buffer */
                t = pix[color];
                if (c != color)
                    t += (sum[c] - sum[color]) / num;
                CLIP16(t,brow[2][col][c],bits);        /* [FD] */
            }
        }
        if (row > 3)                                /* Write buffer to image */
            memcpy (dst + 3*((row-2)*width+2), brow[0]+2, (width-4)*3*sizeof *dst); /* [FD] */
        for (g=0; g < 4; g++)
            brow[(g-1) & 3] = brow[g];
    }
    memcpy (dst + 3*((row-2)*width+2), brow[0]+2, (width-4)*3*sizeof *dst);
    memcpy (dst + 3*((row-1)*width+2), brow[1]+2, (width-4)*3*sizeof *dst);
    g_free (brow[4]);
}
