/*
 * oldlight-image-reader-raw.h: Raw image reader
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IMAGE_READER_RAW_H__
#define OL_IMAGE_READER_RAW_H__

#include "oldlight/oldlight-image-reader.h"
#include "oldlight/oldlight-raw-params.h"

G_BEGIN_DECLS

#define OL_TYPE_IMAGE_READER_RAW ol_image_reader_raw_get_type()

G_DECLARE_FINAL_TYPE(OLImageReaderRaw, ol_image_reader_raw, OL, IMAGE_READER_RAW, OLImageReader);

OLImageReaderRaw *ol_image_reader_raw_new(const gchar *filename);

OLRawParams *ol_image_reader_raw_get_params(OLImageReaderRaw *raw);

G_END_DECLS

#endif /* OL_IMAGE_READER_RAW_H___ */
