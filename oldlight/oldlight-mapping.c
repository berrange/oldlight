/*
 * oldlight-mapping.c: list of asterisms
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <math.h>

#include "oldlight/oldlight-mapping.h"
#include "oldlight/oldlight-matrix.h"

typedef struct _OLMappingPair OLMappingPair;

struct _OLMappingPair
{
    GObject parent;

    OLQuad *ref;
    OLQuad *match;
};

struct _OLMapping
{
    OLMappingPair *pairs;
    gsize npairs;
    gsize width;
    gsize height;
};

G_DEFINE_TYPE(OLMapping, ol_mapping, G_TYPE_OBJECT);


enum {
    PROP_0,
    PROP_WIDTH,
    PROP_HEIGHT,
};


static void ol_mapping_get_property(GObject *object,
                                    guint prop_id,
                                    GValue *value,
                                    GParamSpec *pspec)
{
    OLMapping *mapping = OL_MAPPING(object);

    switch (prop_id) {
    case PROP_WIDTH:
        g_value_set_int(value, mapping->width);
        break;

    case PROP_HEIGHT:
        g_value_set_int(value, mapping->height);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_mapping_set_property(GObject *object,
                                    guint prop_id,
                                    const GValue *value,
                                    GParamSpec *pspec)
{
    OLMapping *mapping = OL_MAPPING(object);

    switch (prop_id) {
    case PROP_WIDTH:
        mapping->width = g_value_get_int(value);
        break;

    case PROP_HEIGHT:
        mapping->height = g_value_get_int(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_mapping_finalize(GObject *object)
{
    OLMapping *mapping = OL_MAPPING(object);
    gsize i;

    for (i = 0; i < mapping->npairs; i++) {
        ol_quad_free(mapping->pairs[i].ref);
        ol_quad_free(mapping->pairs[i].match);
    }
    g_free(mapping->pairs);

    G_OBJECT_CLASS(ol_mapping_parent_class)->finalize(object);
}

static void ol_mapping_class_init(OLMappingClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = ol_mapping_finalize;
    object_class->get_property = ol_mapping_get_property;
    object_class->set_property = ol_mapping_set_property;

    g_object_class_install_property(object_class,
                                    PROP_WIDTH,
                                    g_param_spec_int("width",
                                                     "Image width",
                                                     "Image width",
                                                     0, 65536, 0,
                                                     G_PARAM_READWRITE |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_HEIGHT,
                                    g_param_spec_int("height",
                                                     "Image height",
                                                     "Image height",
                                                     0, 65536, 0,
                                                     G_PARAM_READWRITE |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB));
}

static void ol_mapping_init(OLMapping *mapping G_GNUC_UNUSED)
{
}


OLMapping *ol_mapping_new(gsize width,
                          gsize height)
{
    return OL_MAPPING(g_object_new(OL_TYPE_MAPPING,
                                   "width", width,
                                   "height", height,
                                   NULL));
}

gsize ol_mapping_get_nquad_pairs(OLMapping *mapping)
{
    return mapping->npairs;
}

OLQuad *ol_mapping_get_quad_ref(OLMapping *mapping, gsize i)
{
    if (i >= mapping->npairs)
        return NULL;

    return mapping->pairs[i].ref;
}

OLQuad *ol_mapping_get_quad_match(OLMapping *mapping, gsize i)
{
    if (i >= mapping->npairs)
        return NULL;

    return mapping->pairs[i].match;
}

void ol_mapping_add_quad_pair(OLMapping *mapping,
			      OLQuad *ref,
			      OLQuad *match)
{
    mapping->pairs = g_renew(OLMappingPair, mapping->pairs, mapping->npairs + 1);
    mapping->pairs[mapping->npairs].ref = ol_quad_copy(ref);
    mapping->pairs[mapping->npairs].match = ol_quad_copy(match);
    mapping->npairs++;
}

void ol_mapping_render_ref(OLMapping *mapping,
                           OLImage *image)
{
    gsize i;
    for (i = 0; i < mapping->npairs; i++) {
        ol_quad_render(mapping->pairs[i].ref, image);
    }
}

void ol_mapping_render_match(OLMapping *mapping,
                             OLImage *image)
{
    gsize i;
    for (i = 0; i < mapping->npairs; i++) {
        ol_quad_render(mapping->pairs[i].match, image);
    }
}

OLTransform *ol_mapping_build_transform(OLMapping *mapping)
{
    gsize i;
    OLMatrix *dxl = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, mapping->npairs, 1);
    OLMatrix *dyl = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, mapping->npairs, 1);
    OLMatrix *rotl = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, mapping->npairs, 1);
    gdouble totdx = 0, totdy = 0, totrot = 0;

    for (i = 0; i < mapping->npairs; i++) {
        gdouble dx, dy, rot;
        gdouble dxref, dyref, dxmatch, dymatch;
        gdouble rotref, rotmatch;
        gdouble mx, my;

        dxref = mapping->pairs[i].ref->stars[1].x - mapping->pairs[i].ref->stars[0].x;
        dyref = mapping->pairs[i].ref->stars[1].y - mapping->pairs[i].ref->stars[0].y;

        rotref = atan(dyref/dxref);

        dxmatch = mapping->pairs[i].match->stars[1].x - mapping->pairs[i].match->stars[0].x;
        dymatch = mapping->pairs[i].match->stars[1].y - mapping->pairs[i].match->stars[0].y;

        rotmatch = atan(dymatch/dxmatch);

        rot = rotref - rotmatch;

        mx = (mapping->pairs[i].match->stars[0].x * cos(rot)) -
             (mapping->pairs[i].match->stars[0].y * sin(rot));
        my = (mapping->pairs[i].match->stars[0].x * sin(rot)) +
             (mapping->pairs[i].match->stars[0].y * cos(rot));

        dx = mapping->pairs[i].ref->stars[0].x - mx;
        dy = mapping->pairs[i].ref->stars[0].y - my;

        dxl->data.f64[i] = dx;
        dyl->data.f64[i] = dy;
        rotl->data.f64[i] = rot;
    }

    double rotmed = ol_matrix_median(rotl);

    for (i = 0; i < mapping->npairs; i++) {
        if (fabs(rotl->data.f64[i] - rotmed) > 1) {
            continue;
        }

        totdx += dxl->data.f64[i];
        totdy += dyl->data.f64[i];
        totrot += rotl->data.f64[i];
    }

    ol_matrix_free(dxl);
    ol_matrix_free(dyl);
    ol_matrix_free(rotl);

    return ol_transform_new(totrot / mapping->npairs,
                            totdx / mapping->npairs,
                            totdy / mapping->npairs);
}
