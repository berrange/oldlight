/*
 * oldlight-image-reader-pnm.c: Pnm image reader
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-reader-pnm.h"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

struct _OLImageReaderPnm
{
    OLImageReader parent;
    int fd;
    off_t payload;
};


G_DEFINE_TYPE(OLImageReaderPnm, ol_image_reader_pnm, OL_TYPE_IMAGE_READER);


static gboolean
ol_image_reader_pnm_open(OLImageIO *io,
                         GError **err);
static gboolean
ol_image_reader_pnm_close(OLImageIO *io,
                          GError **err);
static OLImage *
ol_image_reader_pnm_load_payload(OLImageReader *reader,
                                 guint x,
                                 guint y,
                                 guint width,
                                 guint height,
                                 GError **err);


#define OL_IMAGE_READER_PNM_ERROR ol_image_reader_pnm_error_quark()

static GQuark
ol_image_reader_pnm_error_quark(void)
{
    return g_quark_from_static_string("ol-image-reader-pnm");
}


static void ol_image_reader_pnm_finalize(GObject *object)
{
    ol_image_io_close(OL_IMAGE_IO(object), NULL);

    G_OBJECT_CLASS(ol_image_reader_pnm_parent_class)->finalize(object);
}


static void ol_image_reader_pnm_class_init(OLImageReaderPnmClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    OLImageIOClass *io_class = OL_IMAGE_IO_CLASS(klass);
    OLImageReaderClass *reader_class = OL_IMAGE_READER_CLASS(klass);

    object_class->finalize = ol_image_reader_pnm_finalize;

    io_class->open = ol_image_reader_pnm_open;
    io_class->close = ol_image_reader_pnm_close;
    reader_class->load_payload = ol_image_reader_pnm_load_payload;
}


static void ol_image_reader_pnm_init(OLImageReaderPnm *reader)
{
    reader->fd = -1;
}


OLImageReaderPnm *ol_image_reader_pnm_new(const gchar *filename)
{
    return OL_IMAGE_READER_PNM(g_object_new(OL_TYPE_IMAGE_READER_PNM,
                                            "filename", filename,
                                            NULL));
}


static gboolean ol_image_reader_set_error(const gchar *filename,
                                          const gchar *fmt,
                                          int ret,
                                          GError **err)
{
    const gchar *msg;
    if (ret == 0)
        return FALSE;

    msg = strerror(ret);
    g_set_error(err, OL_IMAGE_READER_PNM_ERROR, 0,
                fmt, filename, msg);
    return TRUE;
}


static gboolean
ol_image_reader_pnm_open(OLImageIO *io,
                         GError **err)
{
    OLImageReaderPnm *pnm = OL_IMAGE_READER_PNM(io);
    const gchar *filename = ol_image_io_get_filename(io);
    int width, height;
    char header[1024];
    gsize n;
    guint64 max;
    int ret;

    pnm->fd = open(filename, O_RDONLY);
    if (pnm->fd < 0) {
        ol_image_reader_set_error(filename, "Unable to open file %s: %s", errno, err);
        return FALSE;
    }

    ret = read(pnm->fd, header, sizeof(header) - 1);
    if (ret < 0) {
        ol_image_reader_set_error(filename, "Unable to read file header in %s: %s", EINVAL, err);
        return FALSE;
    }
    header[ret] = '\0';

#define SKIP_COMMENT_AND_SPACE                  \
    do {                                        \
        if (header[n] == '#') {                 \
            while (header[n] != '\n')           \
                n++;                            \
        } else {                                \
            if (!isspace(header[n++]))          \
                goto parseerr;                  \
        }                                       \
        while (isspace(header[n]))              \
            n++;                                \
    } while (0)

#define READ_NUM(var)                           \
    do {                                        \
        var = 0;                                \
        while (isdigit(header[n])) {            \
            var *= 10;                          \
            var += header[n++] - '0';           \
        }                                       \
    } while (0)

    n = 0;
    if (header[n++] != 'P')
        goto parseerr;
    switch (header[n++]) {
    case '5':
        ol_image_io_set_channels(io, OL_IMAGE_CHANNELS_GRAY);
        break;
    case '6':
        ol_image_io_set_channels(io, OL_IMAGE_CHANNELS_RGB);
        break;
    default:
        g_set_error(err, OL_IMAGE_READER_PNM_ERROR, 0,
                    "Unsupported image format 'P%c'", header[n-1]);
        return FALSE;
    }

    SKIP_COMMENT_AND_SPACE;
    READ_NUM(width);
    ol_image_io_set_width(io, width);

    SKIP_COMMENT_AND_SPACE;
    READ_NUM(height);
    ol_image_io_set_height(io, height);

    SKIP_COMMENT_AND_SPACE;
    READ_NUM(max);

    if (max == 255) {
        ol_image_io_set_format(io, OL_MATRIX_FORMAT_UINT8);
    } if (max == 65535) {
        ol_image_io_set_format(io, OL_MATRIX_FORMAT_UINT16);
    } else if (max == 4294967295) {
        ol_image_io_set_format(io, OL_MATRIX_FORMAT_UINT32);
    } else if (max == 18446744073709551615ULL ) {
        ol_image_io_set_format(io, OL_MATRIX_FORMAT_UINT64);
    } else {
        g_set_error(err, OL_IMAGE_READER_PNM_ERROR, 0,
                    "Unexpected max value %" G_GUINT64_FORMAT " in %s", max, filename);
        return FALSE;
    }

    if (!isspace(header[n++]))
        goto parseerr;

    pnm->payload = n;

    return TRUE;

 parseerr:
    ol_image_reader_set_error(filename, "Unable to parse header in %s: %s", EINVAL, err);
    return FALSE;
}


static gboolean
ol_image_reader_pnm_close(OLImageIO *io,
                          GError **err G_GNUC_UNUSED)
{
    OLImageReaderPnm *pnm = OL_IMAGE_READER_PNM(io);
    if (pnm->fd != -1)
        close(pnm->fd);
    pnm->fd = -1;
    return TRUE;
}


static OLImage *
ol_image_reader_pnm_load_payload(OLImageReader *reader,
                                 guint x,
                                 guint y,
                                 guint width,
                                 guint height,
                                 GError **err)
{
    OLImageReaderPnm *pnm = OL_IMAGE_READER_PNM(reader);
    OLImage *img;
    const gchar *filename = ol_image_io_get_filename(OL_IMAGE_IO(reader));
    guint iwidth, iheight;
    off_t start;
    int ret;
    gsize i;
    guint8 *buf;
    gsize buflen;

    iwidth = ol_image_io_get_width(OL_IMAGE_IO(reader));
    iheight = ol_image_io_get_height(OL_IMAGE_IO(reader));

    if (((x + width) > iwidth) ||
        ((y + height) > iheight)) {
        ol_image_reader_set_error(filename, "Data region out of range loading %s: %s", EINVAL, err);
        return NULL;
    }

    img = ol_image_new(ol_image_io_get_format(OL_IMAGE_IO(reader)),
                       ol_image_io_get_channels(OL_IMAGE_IO(reader)),
                       width, height);

    if (x == 0 && width == iwidth) {
        start = (y * (img->bpp * width * height));
        buf = img->matrix->data.u8;
        buflen = img->bpp * width * height;
        while (buflen) {
            ret = pread(pnm->fd, buf, buflen, pnm->payload + start);
            if (ret > 0) {
                buflen -= ret;
                start += ret;
                buf += ret;
            } else if (ret == 0) {
                ol_image_free(img);
                g_set_error(err, OL_IMAGE_READER_PNM_ERROR, 0,
                            "Unexpected end of file reading image data in '%s'",
                            filename);
                return NULL;
            } else {
                ol_image_free(img);
                g_set_error(err, OL_IMAGE_READER_PNM_ERROR, 0,
                            "Unable to read image data in '%s': %s",
                            filename,
                            strerror(errno));
                return NULL;
            }
        }
    } else {
        for (i = 0; i < height; i++) {
            start = ((y + i) * (img->bpp * width * height));
            buf = img->matrix->data.u8 + (i * img->bpp * width);
            buflen = img->bpp * width;
            while (buflen) {
                ret = pread(pnm->fd, buf, buflen, pnm->payload + start);
                if (ret > 0) {
                    buflen -= ret;
                    start += ret;
                    buf += ret;
                } else if (ret == 0) {
                    ol_image_free(img);
                    g_set_error(err, OL_IMAGE_READER_PNM_ERROR, 0,
                                "Unexpected end of file reading image data in '%s'",
                                filename);
                    return NULL;
                } else {
                    ol_image_free(img);
                    g_set_error(err, OL_IMAGE_READER_PNM_ERROR, 0,
                                "Unable to read image data in '%s': %s",
                                filename,
                                strerror(errno));
                    return NULL;
                }
            }
        }
    }

    ol_image_data_be_to_host(img);

    return img;
}
