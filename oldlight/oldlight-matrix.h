/*
 * oldlight-matrix.h: matrix processing
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_MATRIX_H__
#define OL_MATRIX_H__

#include "oldlight/oldlight-enums.h"

G_BEGIN_DECLS

#define OL_TYPE_MATRIX            (ol_matrix_get_type ())

typedef enum {
    OL_MATRIX_FORMAT_INT8,
    OL_MATRIX_FORMAT_INT16,
    OL_MATRIX_FORMAT_INT32,
    OL_MATRIX_FORMAT_INT64,
    OL_MATRIX_FORMAT_UINT8,
    OL_MATRIX_FORMAT_UINT16,
    OL_MATRIX_FORMAT_UINT32,
    OL_MATRIX_FORMAT_UINT64,
    OL_MATRIX_FORMAT_FLOAT32,
    OL_MATRIX_FORMAT_FLOAT64,
} OLMatrixFormat;

typedef struct _OLMatrix OLMatrix;

struct _OLMatrix
{
    OLMatrixFormat format;
    gsize width;
    gsize height;
    gsize len;
    union {
        gint8 *i8;
        gint16 *i16;
        gint32 *i32;
        gint64 *i64;
        guint8 *u8;
        guint16 *u16;
        guint32 *u32;
        guint64 *u64;
        gfloat *f32;
        gdouble *f64;
    } data;
};

GType ol_matrix_get_type(void);

OLMatrix *ol_matrix_new(OLMatrixFormat format,
			gsize width,
			gsize height);
OLMatrix *ol_matrix_new_maximum_filter(OLMatrix *matrix,
                                       OLMatrix *mask);

OLMatrix *ol_matrix_new_convolve(OLMatrix *matrix,
                                 OLMatrix *kern);

OLMatrix *ol_matrix_new_product(OLMatrix *matrix,
                                OLMatrix *other);

OLMatrix *ol_matrix_new_multiply(OLMatrix *matrix,
                                 OLMatrix *other);

OLMatrix *ol_matrix_copy(OLMatrix *matrix);
OLMatrix *ol_matrix_copy_region(OLMatrix *matrix,
                                gsize x,
                                gsize y,
                                gsize width,
                                gsize height);
OLMatrix *ol_matrix_copy_with_border(OLMatrix *matrix,
                                     gsize left,
                                     gsize right,
                                     gsize top,
                                     gsize bottom);

void ol_matrix_free(OLMatrix *matrix);

void ol_matrix_clear_border(OLMatrix *matrix,
                            gsize left,
                            gsize right,
                            gsize top,
                            gsize bottom);

void ol_matrix_abs(OLMatrix *matrix);

gdouble ol_matrix_sum(OLMatrix *matrix);
gdouble ol_matrix_sum_abs(OLMatrix *matrix);
gdouble ol_matrix_sum_masked(OLMatrix *matrix, OLMatrix *mask);
OLMatrix *ol_matrix_sum_xaxis(OLMatrix *matrix);
OLMatrix *ol_matrix_sum_yaxis(OLMatrix *matrix);
gdouble ol_matrix_mean(OLMatrix *matrix);
gdouble ol_matrix_median(OLMatrix *matrix);
gdouble ol_matrix_median_abs_deviation(OLMatrix *matrix);
gdouble ol_matrix_median_abs_deviation_std(OLMatrix *matrix);
gdouble ol_matrix_maximum(OLMatrix *matrix);

void ol_matrix_maximum_filter(OLMatrix *matrix,
                              OLMatrix *mask,
                              OLMatrix *output);

void ol_matrix_convolve(OLMatrix *matrix,
                        OLMatrix *kern,
                        OLMatrix *output);

void ol_matrix_product(OLMatrix *matrix,
                       OLMatrix *other,
                       OLMatrix *ouput);

void ol_matrix_multiply(OLMatrix *matrix,
                        OLMatrix *other,
                        OLMatrix *output);

G_END_DECLS

#endif /* OL_MATRIX_H__ */
