/*
 * oldlight.c: deep sky star field image stacking
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-stacker.h"


int main(int argc, char **argv)
{
    GError *err = NULL;
    GOptionContext *context;
    gboolean verbose = FALSE;
    const char *methodstr = "mean";
    const char *output_file = NULL;
    GOptionEntry options [] = {
        { "verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, "Display progress info", 0 },
        { "method", 'm', 0, G_OPTION_ARG_STRING, &methodstr, "Stacking method", 0 },
        { "output-file", 'o', 0, G_OPTION_ARG_FILENAME, &output_file, "Output file", 0 },
        { NULL, 0, 0, G_OPTION_ARG_NONE, NULL, NULL, 0 }
    };
    gsize i;
    OLImageList *input = NULL;
    OLStacker *stacker = NULL;
    int method;
    int ret = 1;

    /* Setup command line options */
    context = g_option_context_new("");
    g_option_context_add_main_entries(context, options, NULL);
    g_option_context_parse(context, &argc, &argv, &err);
    if (err) {
        g_print ("%s\n"
                 "Run '%s --help' to see a full list of available command line options\n",
                 err->message,
                 argv[0]);
        g_error_free(err);
        err = NULL;
        goto cleanup;
    }
    g_option_context_free(context);

    if (argc < 2) {
        g_printerr("syntax: %s [OPTIONS] INPUT-FILE [INPUT-FILE...]\n", argv[0]);
        goto cleanup;
    }

    if (!ol_stacker_method_from_string(methodstr, &method, &err))
        goto cleanup;

    if (output_file == NULL) {
        if (strstr(argv[1], "pgm"))
            output_file = "stack.pgm";
        else
            output_file = "stack.ppm";
    }
    input = ol_image_list_new();
    for (i = 1; i < argc; i++) {
        if (!ol_image_list_add_file(input, argv[i], &err))
            goto cleanup;
    }

    stacker = ol_stacker_new(method);

    if (!ol_stacker_run(stacker, input, output_file, &err))
        goto cleanup;

    ret = 0;
 cleanup:
    if (stacker)
        g_object_unref(stacker);
    if (input)
        g_object_unref(input);
    if (ret && err) {
        g_printerr("%s: %s\n", argv[0], err->message);
        g_error_free(err);
    }
    return ret;
}
