/*
 * oldlight.c: deep sky star field image stacking
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-reader-raw.h"
#include "oldlight/oldlight-image-writer-pnm.h"
#include "oldlight/oldlight-bayer.h"
#include "oldlight/oldlight-raw-processor.h"

#include <unistd.h>

typedef struct {
    gboolean verbose;
    gboolean rgb;
    const char *output_dir;
    OLBayerMethod bayer_method;
    OLRawProcessorWB white_balance;
    OLMatrix *custom_white_balance;
    gboolean stretch;
    OLImage *bias_image;
    OLImage *dark_image;
    OLImage *flat_image;
    gdouble flat_mean;
    GMutex lock;
    GError *err;
} OLRawExtractData;


#define OL_RAW_EXTRACT_ERROR ol_raw_extract_error_quark()

static GQuark
ol_raw_extract_error_quark(void)
{
    return g_quark_from_static_string("ol-raw-extract");
}


static gboolean
ol_enum_from_string(const char *method,
                    GType typ,
                    int *value,
                    GError **err)
{
    GEnumClass *enum_class;
    GEnumValue *enum_value;

    *value = -1;

    enum_class = g_type_class_ref(typ);
    enum_value = g_enum_get_value_by_nick(enum_class, method);
    g_type_class_unref(enum_class);

    if (enum_value == NULL) {
        g_set_error(err, OL_RAW_EXTRACT_ERROR, 0,
                    "Unknown bayer method '%s'", method);
        return FALSE;
    }

    *value = enum_value->value;
    return TRUE;
}

static void
ol_raw_extract_worker(gpointer job, gpointer opaque)
{
    OLRawExtractData *data = opaque;
    const char *src_file = job;
    char *dst_file = NULL;
    GError *err = NULL;
    OLImage *image = NULL;
    gchar *offset;
    gchar *fname = NULL;
    gchar *fbase = NULL;
    gchar *dname = NULL;
    OLImageReaderRaw *raw = NULL;

    g_mutex_lock(&data->lock);
    if (data->err) {
        g_mutex_unlock(&data->lock);
        return;
    }
    g_mutex_unlock(&data->lock);

    fbase = g_path_get_basename(src_file);
    offset = strrchr(fbase, '.');
    if (!offset) {
        g_set_error(&err, OL_RAW_EXTRACT_ERROR, 0,
                    "Missing file extension in '%s'", src_file);
        goto cleanup;
    }
    *offset = '\0';
    fname = g_strdup_printf("%s.%s", fbase, data->rgb ? "ppm" : "pgm");
    if (data->output_dir) {
        dst_file = g_build_filename(data->output_dir, fname, NULL);
    } else {
        dname = g_path_get_dirname(src_file);
        dst_file = g_build_filename(dname, fname, NULL);
    }

    if (data->verbose)
        g_printerr("Creating %s\n", dst_file);

    raw = ol_image_reader_raw_new(src_file);

    if (!ol_image_io_open(OL_IMAGE_IO(raw), &err))
        goto cleanup;

    if (!(image = ol_image_reader_load_payload(
              OL_IMAGE_READER(raw), 0, 0,
              ol_image_io_get_width(OL_IMAGE_IO(raw)),
              ol_image_io_get_height(OL_IMAGE_IO(raw)),
              &err)))
        goto cleanup;

    if (data->bias_image)
        ol_image_subtract(image, data->bias_image);

    if (data->dark_image)
        ol_image_subtract(image, data->dark_image);

    if (data->flat_image)
        ol_image_divide_mean(image, data->flat_image, data->flat_mean);

    if (data->rgb) {
        OLRawProcessor *proc = ol_raw_processor_new(data->bayer_method,
                                                    data->white_balance,
                                                    data->custom_white_balance,
                                                    data->stretch);
        OLImage *rgb = ol_raw_processor_run(proc,
                                            image,
                                            ol_image_reader_raw_get_params(raw),
                                            &err);
        if (!rgb)
            goto cleanup;
        ol_image_free(image);
        image = rgb;
    }

    if (!ol_image_save(image, dst_file, &err))
        goto cleanup;

 cleanup:
    g_free(dname);
    g_free(fbase);
    g_free(fname);
    if (raw)
        g_object_unref(raw);
    ol_image_free(image);
    g_free(dst_file);
    g_mutex_lock(&data->lock);
    if (err) {
        if (!data->err) {
            data->err = err;
        } else {
            g_error_free(err);
        }
    }
    g_mutex_unlock(&data->lock);
}


int main(int argc, char **argv)
{
    GError *err = NULL;
    GOptionContext *context;
    const char *bias_file = NULL;
    const char *dark_file = NULL;
    const char *flat_file = NULL;
    OLRawExtractData data = {0};
    const char *bayer_method = NULL;
    const char *white_balance = NULL;
    int workers = sysconf(_SC_NPROCESSORS_ONLN);
    GOptionEntry options [] = {
        { "verbose", 'v', 0, G_OPTION_ARG_NONE, &data.verbose, "Display progress info", 0 },
        { "bias-file", 'b', 0, G_OPTION_ARG_FILENAME, &bias_file, "Subtract bias image", 0 },
        { "dark-file", 'd', 0, G_OPTION_ARG_FILENAME, &dark_file, "Subtract dark image", 0 },
        { "flat-file", 'f', 0, G_OPTION_ARG_FILENAME, &flat_file, "Subtract flat image", 0 },
        { "rgb", 'r', 0, G_OPTION_ARG_NONE, &data.rgb, "Save an RGB image instead of bayer data", 0 },
        { "bayer-method", 'm', 0, G_OPTION_ARG_STRING, &bayer_method, "Bayer processing algorithm", 0 },
        { "white-balance", 'w', 0, G_OPTION_ARG_STRING, &white_balance, "White balance (none, image, camera)", 0 },
        { "output-dir", 'o', 0, G_OPTION_ARG_FILENAME, &data.output_dir, "Directory to save extracted files", 0},
        { "workers", 'k', 0, G_OPTION_ARG_INT, &workers, "Number of worker threads", 0 },
        { "stretch", 's', 0, G_OPTION_ARG_NONE, &data.stretch, "Strech colors", 0 },
        { NULL, 0, 0, G_OPTION_ARG_NONE, NULL, NULL, 0 }
    };
    gsize i;
    int ret = 1;
    GThreadPool *pool = NULL;

    /* Setup command line options */
    context = g_option_context_new("");
    g_option_context_add_main_entries(context, options, NULL);
    g_option_context_parse(context, &argc, &argv, &err);
    if (err) {
        g_print ("%s\n"
                 "Run '%s --help' to see a full list of available command line options\n",
                 err->message,
                 argv[0]);
        g_error_free(err);
        err = NULL;
        goto cleanup;
    }
    g_option_context_free(context);

    if (argc < 2) {
        g_printerr("syntax: %s RAW-FILE [RAW-FILE...]\n", argv[0]);
        goto cleanup;
    }

    if (bayer_method) {
        if (!ol_enum_from_string(bayer_method,
                                 OL_TYPE_BAYER_METHOD,
                                 (int *)&data.bayer_method,
                                 &err))
            goto cleanup;
    } else {
        data.bayer_method = OL_BAYER_METHOD_AHD;
    }

    if (white_balance) {
        if (g_str_has_prefix(white_balance, "custom:")) {
            char **fields = g_strsplit(white_balance + 7, ",", 0);
            data.custom_white_balance = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT32, 4, 1);
            data.white_balance = OL_RAW_PROCESSOR_WB_CUSTOM;
            for (i = 0; fields[i] != NULL && i < 4 ; i++) {
                char *end = NULL;
                if (!fields[i][0]) {
                    g_set_error(&err, OL_RAW_EXTRACT_ERROR, 0,
                                "Cannot parse white balance '%s'", fields[i]);
                    goto cleanup;
                }
                data.custom_white_balance->data.f32[i] = strtod(fields[i], &end);
                if (end && *end != '\0') {
                    g_set_error(&err, OL_RAW_EXTRACT_ERROR, 0,
                                "Cannot parse white balance '%s'", fields[i]);
                    goto cleanup;
                }
            }
            if (i < 3 || fields[i] != NULL) {
                g_set_error(&err, OL_RAW_EXTRACT_ERROR, 0,
                            "Custom white balance requires 3/4 values");
                goto cleanup;
            }
            if (i < 4)
                data.custom_white_balance->data.f32[3] = data.custom_white_balance->data.f32[1];
            g_strfreev(fields);
        } else {
            if (!ol_enum_from_string(white_balance,
                                     OL_TYPE_RAW_PROCESSOR_WB,
                                     (int *)&data.white_balance,
                                 &err))
                goto cleanup;

            if (data.white_balance == OL_RAW_PROCESSOR_WB_CUSTOM) {
                g_set_error(&err, OL_RAW_EXTRACT_ERROR, 0,
                            "Custom white balance requires 3/4 values");
                goto cleanup;
            }
        }
    } else {
        data.white_balance = OL_RAW_PROCESSOR_WB_IMAGE;
    }

    if (bias_file &&
        !(data.bias_image = ol_image_load(bias_file, &err)))
        goto cleanup;

    if (dark_file &&
        !(data.dark_image = ol_image_load(dark_file, &err)))
        goto cleanup;

    if (flat_file) {
        gsize x, y;
        OLImage *f;
        if (!(data.flat_image = ol_image_load(flat_file, &err)))
            goto cleanup;
        data.flat_mean = ol_matrix_mean(data.flat_image->matrix);

        f = data.flat_image;
        /* Take mean across the CFA */
        for (y = 0; y < f->height; y += 2) {
            for (x = 0; x < f->width; x += 2) {
                guint32 val =
                    f->matrix->data.u16[(f->width * y) + x] +
                    f->matrix->data.u16[(f->width * y) + x + 1] +
                    f->matrix->data.u16[(f->width * (y + 1)) + x] +
                    f->matrix->data.u16[(f->width * (y + 1)) + x + 1];
                val /= 4;
                f->matrix->data.u16[(f->width * y) + x] =
                    f->matrix->data.u16[(f->width * y) + x + 1] =
                    f->matrix->data.u16[(f->width * (y + 1)) + x] =
                    f->matrix->data.u16[(f->width * (y + 1)) + x + 1] = val;
            }
        }
    }

    pool = g_thread_pool_new(ol_raw_extract_worker,
                             &data,
                             workers,
                             TRUE,
                             &err);
    if (err)
        goto cleanup;

    for (i = 1; i < argc; i++) {
        if (!g_thread_pool_push(pool, argv[i], &err))
            goto cleanup;
    }

    g_thread_pool_free(pool, FALSE, TRUE);
    pool = NULL;
    if (data.err) {
        err = data.err;
        goto cleanup;
    }

    ret = 0;
 cleanup:
    ol_matrix_free(data.custom_white_balance);
    if (pool)
        g_thread_pool_free(pool, TRUE, TRUE);
    if (data.bias_image)
        ol_image_free(data.bias_image);
    if (data.dark_image)
        ol_image_free(data.dark_image);
    if (data.flat_image)
        ol_image_free(data.flat_image);
    if (ret && err) {
        g_printerr("%s: %s\n", argv[0], err->message);
        g_error_free(err);
    }
    return ret;
}
