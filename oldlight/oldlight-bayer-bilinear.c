/*
 * oldlight-bayer-bilinear.h: bayer processing bilinear algorithm
 *
 * This file is derived from bayer.c in libdc1394,
 * written by Damien Douxchamps and Frederic Devernay
 *
 * The oldlight changes are:
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * OpenCV's Bayer decoding
 */

#include "oldlight/oldlight-bayer-bilinear.h"

#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


G_DEFINE_TYPE(OLBayerBilinear, ol_bayer_bilinear, OL_TYPE_BAYER);


static void
ol_bayer_bilinear_decode_uint8(const guint8 *restrict bayer,
                               guint8 *restrict rgb,
                               gsize sx,
                               gsize sy,
                               OLBayerFilter filter);
static void
ol_bayer_bilinear_decode_uint16(const guint16 *restrict bayer,
                                guint16 *restrict rgb,
                                gsize sx,
                                gsize sy,
                                OLBayerFilter filter,
                                int bits G_GNUC_UNUSED);


static void ol_bayer_bilinear_class_init(OLBayerBilinearClass *klass)
{
    OLBayerClass *bayer_class = OL_BAYER_CLASS(klass);

    bayer_class->decode8 = ol_bayer_bilinear_decode_uint8;
    bayer_class->decode16 = ol_bayer_bilinear_decode_uint16;
}


static void ol_bayer_bilinear_init(OLBayerBilinear *bayer G_GNUC_UNUSED)
{
}


static void
ClearBorders(guint8 *rgb, int sx, int sy, int w)
{
    int i, j;
    // black edges are added with a width w:
    i = 3 * sx * w - 1;
    j = 3 * sx * sy - 1;
    while (i >= 0) {
        rgb[i--] = 0;
        rgb[j--] = 0;
    }

    int low = sx * (w - 1) * 3 - 1 + w * 3;
    i = low + sx * (sy - w * 2 + 1) * 3;
    while (i > low) {
        j = 6 * w;
        while (j > 0) {
            rgb[i--] = 0;
            j--;
        }
        i -= (sx - 2 * w) * 3;
    }
}


static void
ClearBorders_uint16(guint16 * rgb, int sx, int sy, int w)
{
    int i, j;

    // black edges:
    i = 3 * sx * w - 1;
    j = 3 * sx * sy - 1;
    while (i >= 0) {
        rgb[i--] = 0;
        rgb[j--] = 0;
    }

    int low = sx * (w - 1) * 3 - 1 + w * 3;
    i = low + sx * (sy - w * 2 + 1) * 3;
    while (i > low) {
        j = 6 * w;
        while (j > 0) {
            rgb[i--] = 0;
            j--;
        }
        i -= (sx - 2 * w) * 3;
    }

}


static void
ol_bayer_bilinear_decode_uint8(const guint8 *restrict bayer,
                               guint8 *restrict rgb,
                               gsize sx,
                               gsize sy,
                               OLBayerFilter filter)
{
    const int bayerStep = sx;
    const int rgbStep = 3 * sx;
    int width = sx;
    int height = sy;
    /*
      the two letters  of the OpenCV name are respectively
      the 4th and 3rd letters from the blinky name,
      and we also have to switch R and B (OpenCV is BGR)

      CV_BayerBG2BGR <-> OL_BAYER_FILTER_BGGR
      CV_BayerGB2BGR <-> OL_BAYER_FILTER_GBRG
      CV_BayerGR2BGR <-> OL_BAYER_FILTER_GRBG

      int blue = filter == CV_BayerBG2BGR || filter == CV_BayerGB2BGR ? -1 : 1;
      int start_with_green = filter == CV_BayerGB2BGR || filter == CV_BayerGR2BGR;
    */
    int blue = filter == OL_BAYER_FILTER_BGGR
        || filter == OL_BAYER_FILTER_GBRG ? -1 : 1;
    int start_with_green = filter == OL_BAYER_FILTER_GBRG
        || filter == OL_BAYER_FILTER_GRBG;

    ClearBorders(rgb, sx, sy, 1);
    rgb += rgbStep + 3 + 1;
    height -= 2;
    width -= 2;

    for (; height--; bayer += bayerStep, rgb += rgbStep) {
        int t0, t1;
        const guint8 *bayerEnd = bayer + width;

        if (start_with_green) {
            /* OpenCV has a bug in the next line, which was
               t0 = (bayer[0] + bayer[bayerStep * 2] + 1) >> 1; */
            t0 = (bayer[1] + bayer[bayerStep * 2 + 1] + 1) >> 1;
            t1 = (bayer[bayerStep] + bayer[bayerStep + 2] + 1) >> 1;
            rgb[-blue] = (guint8) t0;
            rgb[0] = bayer[bayerStep + 1];
            rgb[blue] = (guint8) t1;
            bayer++;
            rgb += 3;
        }

        if (blue > 0) {
            for (; bayer <= bayerEnd - 2; bayer += 2, rgb += 6) {
                t0 = (bayer[0] + bayer[2] + bayer[bayerStep * 2] +
                      bayer[bayerStep * 2 + 2] + 2) >> 2;
                t1 = (bayer[1] + bayer[bayerStep] +
                      bayer[bayerStep + 2] + bayer[bayerStep * 2 + 1] +
                      2) >> 2;
                rgb[-1] = (guint8) t0;
                rgb[0] = (guint8) t1;
                rgb[1] = bayer[bayerStep + 1];

                t0 = (bayer[2] + bayer[bayerStep * 2 + 2] + 1) >> 1;
                t1 = (bayer[bayerStep + 1] + bayer[bayerStep + 3] +
                      1) >> 1;
                rgb[2] = (guint8) t0;
                rgb[3] = bayer[bayerStep + 2];
                rgb[4] = (guint8) t1;
            }
        } else {
            for (; bayer <= bayerEnd - 2; bayer += 2, rgb += 6) {
                t0 = (bayer[0] + bayer[2] + bayer[bayerStep * 2] +
                      bayer[bayerStep * 2 + 2] + 2) >> 2;
                t1 = (bayer[1] + bayer[bayerStep] +
                      bayer[bayerStep + 2] + bayer[bayerStep * 2 + 1] +
                      2) >> 2;
                rgb[1] = (guint8) t0;
                rgb[0] = (guint8) t1;
                rgb[-1] = bayer[bayerStep + 1];

                t0 = (bayer[2] + bayer[bayerStep * 2 + 2] + 1) >> 1;
                t1 = (bayer[bayerStep + 1] + bayer[bayerStep + 3] +
                      1) >> 1;
                rgb[4] = (guint8) t0;
                rgb[3] = bayer[bayerStep + 2];
                rgb[2] = (guint8) t1;
            }
        }

        if (bayer < bayerEnd) {
            t0 = (bayer[0] + bayer[2] + bayer[bayerStep * 2] +
                  bayer[bayerStep * 2 + 2] + 2) >> 2;
            t1 = (bayer[1] + bayer[bayerStep] +
                  bayer[bayerStep + 2] + bayer[bayerStep * 2 + 1] +
                  2) >> 2;
            rgb[-blue] = (guint8) t0;
            rgb[0] = (guint8) t1;
            rgb[blue] = bayer[bayerStep + 1];
            bayer++;
            rgb += 3;
        }

        bayer -= width;
        rgb -= width * 3;

        blue = -blue;
        start_with_green = !start_with_green;
    }
}


static void
ol_bayer_bilinear_decode_uint16(const guint16 *restrict bayer,
                                guint16 *restrict rgb,
                                gsize sx,
                                gsize sy,
                                OLBayerFilter filter,
                                int bits G_GNUC_UNUSED)
{
    const int bayerStep = sx;
    const int rgbStep = 3 * sx;
    int width = sx;
    int height = sy;
    int blue = filter == OL_BAYER_FILTER_BGGR
        || filter == OL_BAYER_FILTER_GBRG ? -1 : 1;
    int start_with_green = filter == OL_BAYER_FILTER_GBRG
        || filter == OL_BAYER_FILTER_GRBG;

    ClearBorders_uint16(rgb, sx, sy, 1);
    rgb += rgbStep + 3 + 1;
    height -= 2;
    width -= 2;

    for (; height--; bayer += bayerStep, rgb += rgbStep) {
        int t0, t1;
        const guint16 *bayerEnd = bayer + width;

        if (start_with_green) {
            /* OpenCV has a bug in the next line, which was
               t0 = (bayer[0] + bayer[bayerStep * 2] + 1) >> 1; */
            t0 = (bayer[1] + bayer[bayerStep * 2 + 1] + 1) >> 1;
            t1 = (bayer[bayerStep] + bayer[bayerStep + 2] + 1) >> 1;
            rgb[-blue] = (guint16) t0;
            rgb[0] = bayer[bayerStep + 1];
            rgb[blue] = (guint16) t1;
            bayer++;
            rgb += 3;
        }

        if (blue > 0) {
            for (; bayer <= bayerEnd - 2; bayer += 2, rgb += 6) {
                t0 = (bayer[0] + bayer[2] + bayer[bayerStep * 2] +
                      bayer[bayerStep * 2 + 2] + 2) >> 2;
                t1 = (bayer[1] + bayer[bayerStep] +
                      bayer[bayerStep + 2] + bayer[bayerStep * 2 + 1] +
                      2) >> 2;
                rgb[-1] = (guint16) t0;
                rgb[0] = (guint16) t1;
                rgb[1] = bayer[bayerStep + 1];

                t0 = (bayer[2] + bayer[bayerStep * 2 + 2] + 1) >> 1;
                t1 = (bayer[bayerStep + 1] + bayer[bayerStep + 3] +
                      1) >> 1;
                rgb[2] = (guint16) t0;
                rgb[3] = bayer[bayerStep + 2];
                rgb[4] = (guint16) t1;
            }
        } else {
            for (; bayer <= bayerEnd - 2; bayer += 2, rgb += 6) {
                t0 = (bayer[0] + bayer[2] + bayer[bayerStep * 2] +
                      bayer[bayerStep * 2 + 2] + 2) >> 2;
                t1 = (bayer[1] + bayer[bayerStep] +
                      bayer[bayerStep + 2] + bayer[bayerStep * 2 + 1] +
                      2) >> 2;
                rgb[1] = (guint16) t0;
                rgb[0] = (guint16) t1;
                rgb[-1] = bayer[bayerStep + 1];

                t0 = (bayer[2] + bayer[bayerStep * 2 + 2] + 1) >> 1;
                t1 = (bayer[bayerStep + 1] + bayer[bayerStep + 3] +
                      1) >> 1;
                rgb[4] = (guint16) t0;
                rgb[3] = bayer[bayerStep + 2];
                rgb[2] = (guint16) t1;
            }
        }

        if (bayer < bayerEnd) {
            t0 = (bayer[0] + bayer[2] + bayer[bayerStep * 2] +
                  bayer[bayerStep * 2 + 2] + 2) >> 2;
            t1 = (bayer[1] + bayer[bayerStep] +
                  bayer[bayerStep + 2] + bayer[bayerStep * 2 + 1] +
                  2) >> 2;
            rgb[-blue] = (guint16) t0;
            rgb[0] = (guint16) t1;
            rgb[blue] = bayer[bayerStep + 1];
            bayer++;
            rgb += 3;
        }

        bayer -= width;
        rgb -= width * 3;

        blue = -blue;
        start_with_green = !start_with_green;
    }
}
