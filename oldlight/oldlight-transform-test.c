/*
 * oldlight-transform-test.c: transformation between two images
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight-transform.h"

#include <math.h>


static void test_affine(void)
{
    OLTransform *t = ol_transform_new(30,
                                      25,
                                      75);

    g_assert(t->affine->data.f64[0] == cos(30));
    g_assert(t->affine->data.f64[1] == sin(30));
    g_assert(t->affine->data.f64[2] == 0.0);

    g_assert(t->affine->data.f64[3] == -sin(30));
    g_assert(t->affine->data.f64[4] == cos(30));
    g_assert(t->affine->data.f64[5] == 0.0);

    g_assert(t->affine->data.f64[6] == 25.0);
    g_assert(t->affine->data.f64[7] == 75.0);
    g_assert(t->affine->data.f64[8] == 1.0);

    ol_transform_free(t);
}


static void test_shift(void)
{
    OLTransform *t = ol_transform_new(0, -25,-75);
    OLLocation *src = ol_location_new(125, 300);
    OLLocation *dst = ol_transform_apply(t, src);

    g_assert(dst->x == 100);
    g_assert(dst->y == 225);

    ol_transform_free(t);
}


static void test_rotate(void)
{
    OLTransform *t = ol_transform_new(15 * G_PI / 180.0, 0, 0);
    OLLocation *src = ol_location_new(100, 50);
    OLLocation *dst = ol_transform_apply(t, src);

    g_assert(fabs(dst->x - 83.651630) < 0.001);
    g_assert(fabs(dst->y - 74.178195) < 0.001);

    ol_transform_free(t);
}


int main(int argc, char **argv)
{
    g_test_init(&argc, &argv, NULL);

    g_test_add_func("/transform/affine", test_affine);
    g_test_add_func("/transform/shift", test_shift);
    g_test_add_func("/transform/rotate", test_rotate);

    return g_test_run();
}
