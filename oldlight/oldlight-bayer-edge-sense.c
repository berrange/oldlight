/*
 * oldlight-bayer-edge-sense.h: bayer processing edge sense algorithm
 *
 * This file is derived from bayer.c in libdc1394,
 * written by Damien Douxchamps and Frederic Devernay
 *
 * The oldlight changes are:
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Edge Sensing Interpolation II from http://www-ise.stanford.edu/~tingchen/
 *   (Laroche,Claude A.  "Apparatus and method for adaptively
 *     interpolating a full color image utilizing chrominance gradients"
 *    U.S. Patent 5,373,322)
 */

#include "oldlight/oldlight-bayer-edge-sense.h"

#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


struct _OLBayerEdgeSense
{
    OLBayer parent;
};

G_DEFINE_TYPE(OLBayerEdgeSense, ol_bayer_edge_sense, OL_TYPE_BAYER);


static void
ol_bayer_edge_sense_decode_uint8(const guint8 *restrict bayer,
                                 guint8 *restrict rgb,
                                 gsize sx,
                                 gsize sy,
                                 OLBayerFilter filter);
static void
ol_bayer_edge_sense_decode_uint16(const guint16 *restrict bayer,
                                  guint16 *restrict rgb,
                                  gsize sx,
                                  gsize sy,
                                  OLBayerFilter filter,
                                  int bits);

static void ol_bayer_edge_sense_class_init(OLBayerEdgeSenseClass *klass)
{
    OLBayerClass *bayer_class = OL_BAYER_CLASS(klass);

    bayer_class->decode8 = ol_bayer_edge_sense_decode_uint8;
    bayer_class->decode16 = ol_bayer_edge_sense_decode_uint16;
}


static void ol_bayer_edge_sense_init(OLBayerEdgeSense *bayer G_GNUC_UNUSED)
{
}



#define CLIP(in, out)                           \
    in = in < 0 ? 0 : in;                       \
    in = in > 255 ? 255 : in;                   \
    out=in;

#define CLIP16(in, out, bits)                           \
    in = in < 0 ? 0 : in;                               \
    in = in > ((1<<bits)-1) ? ((1<<bits)-1) : in;       \
    out=in;

static void
ClearBorders(guint8 *rgb, int sx, int sy, int w)
{
    int i, j;
    // black edges are added with a width w:
    i = 3 * sx * w - 1;
    j = 3 * sx * sy - 1;
    while (i >= 0) {
        rgb[i--] = 0;
        rgb[j--] = 0;
    }

    int low = sx * (w - 1) * 3 - 1 + w * 3;
    i = low + sx * (sy - w * 2 + 1) * 3;
    while (i > low) {
        j = 6 * w;
        while (j > 0) {
            rgb[i--] = 0;
            j--;
        }
        i -= (sx - 2 * w) * 3;
    }
}

static void
ClearBorders_uint16(guint16 * rgb, int sx, int sy, int w)
{
    int i, j;

    // black edges:
    i = 3 * sx * w - 1;
    j = 3 * sx * sy - 1;
    while (i >= 0) {
        rgb[i--] = 0;
        rgb[j--] = 0;
    }

    int low = sx * (w - 1) * 3 - 1 + w * 3;
    i = low + sx * (sy - w * 2 + 1) * 3;
    while (i > low) {
        j = 6 * w;
        while (j > 0) {
            rgb[i--] = 0;
            j--;
        }
        i -= (sx - 2 * w) * 3;
    }

}


static void
ol_bayer_edge_sense_decode_uint8(const guint8 *restrict bayer,
                                 guint8 *restrict rgb,
                                 gsize sx,
                                 gsize sy,
                                 OLBayerFilter filter)
{
    guint8 *outR, *outG, *outB;
    register int i3, j3, base;
    int i, j;
    int dh, dv;
    int tmp;
    int sx3=sx*3;

    // sx and sy should be even
    switch (filter) {
    case OL_BAYER_FILTER_GRBG:
    case OL_BAYER_FILTER_BGGR:
        outR = &rgb[0];
        outG = &rgb[1];
        outB = &rgb[2];
        break;
    case OL_BAYER_FILTER_GBRG:
    case OL_BAYER_FILTER_RGGB:
        outR = &rgb[2];
        outG = &rgb[1];
        outB = &rgb[0];
        break;
    default:
        g_assert_not_reached();
    }

    switch (filter) {
    case OL_BAYER_FILTER_GRBG:        //---------------------------------------------------------
    case OL_BAYER_FILTER_GBRG:
        // copy original RGB data to output images
        for (i = 0, i3=0; i < sy*sx; i += (sx<<1), i3 += (sx3<<1)) {
            for (j = 0, j3=0; j < sx; j += 2, j3+=6) {
                base=i3+j3;
                outG[base]           = bayer[i + j];
                outG[base + sx3 + 3] = bayer[i + j + sx + 1];
                outR[base + 3]       = bayer[i + j + 1];
                outB[base + sx3]     = bayer[i + j + sx];
            }
        }
        // process GREEN channel
        for (i3= 3*sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=6; j3 < sx3 - 9; j3+=6) {
                base=i3+j3;
                dh = abs(((outB[base - 6] +
                           outB[base + 6]) >> 1) -
                         outB[base]);
                dv = abs(((outB[base - (sx3<<1)] +
                           outB[base + (sx3<<1)]) >> 1) -
                         outB[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP(tmp, outG[base]);
            }
        }
		
        for (i3=2*sx3; i3 < (sy - 3)*sx3; i3 += (sx3<<1)) {
            for (j3=9; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                dh = abs(((outR[base - 6] +
                           outR[base + 6]) >>1 ) -
                         outR[base]);
                dv = abs(((outR[base - (sx3<<1)] +
                           outR[base + (sx3<<1)]) >>1 ) -
                         outR[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP(tmp, outG[base]);
            }
        }
        // process RED channel
        for (i3=0; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {
            for (j3=6; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - 3] -
                      outG[base - 3] +
                      outR[base + 3] -
                      outG[base + 3]) >> 1);
                CLIP(tmp, outR[base]);
            }
        }
        for (i3=sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=3; j3 < sx3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3] -
                      outG[base - sx3] +
                      outR[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP(tmp, outR[base]);
            }
            for (j3=6; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outR[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outR[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outR[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP(tmp, outR[base]);
            }
        }

        // process BLUE channel
        for (i3=sx3; i3 < sy*sx3; i3 += (sx3<<1)) {
            for (j3=3; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - 3] -
                      outG[base - 3] +
                      outB[base + 3] -
                      outG[base + 3]) >> 1);
                CLIP(tmp, outB[base]);
            }
        }
        for (i3=2*sx3; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {
            for (j3=0; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3] -
                      outG[base - sx3] +
                      outB[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP(tmp, outB[base]);
            }
            for (j3=3; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outB[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outB[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outB[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP(tmp, outB[base]);
            }
        }
        break;

    case OL_BAYER_FILTER_BGGR:        //---------------------------------------------------------
    case OL_BAYER_FILTER_RGGB:
        // copy original RGB data to output images
        for (i = 0, i3=0; i < sy*sx; i += (sx<<1), i3 += (sx3<<1)) {
            for (j = 0, j3=0; j < sx; j += 2, j3+=6) {
                base=i3+j3;
                outB[base] = bayer[i + j];
                outR[base + sx3 + 3] = bayer[i + sx + (j + 1)];
                outG[base + 3] = bayer[i + j + 1];
                outG[base + sx3] = bayer[i + sx + j];
            }
        }
        // process GREEN channel
        for (i3=2*sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=6; j3 < sx3 - 9; j3+=6) {
                base=i3+j3;
                dh = abs(((outB[base - 6] +
                           outB[base + 6]) >> 1) -
                         outB[base]);
                dv = abs(((outB[base - (sx3<<1)] +
                           outB[base + (sx3<<1)]) >> 1) -
                         outB[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP(tmp, outG[base]);
            }
        }
        for (i3=3*sx3; i3 < (sy - 3)*sx3; i3 += (sx3<<1)) {
            for (j3=9; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                dh = abs(((outR[base - 6] +
                           outR[base + 6]) >> 1) -
                         outR[base]);
                dv = abs(((outR[base - (sx3<<1)] +
                           outR[base + (sx3<<1)]) >> 1) -
                         outR[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP(tmp, outG[base]);
            }
        }
        // process RED channel
        for (i3=sx3; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {        // G-points (1/2)
            for (j3=6; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - 3] -
                      outG[base - 3] +
                      outR[base + 3] -
                      outG[base + 3]) >>1);
                CLIP(tmp, outR[base]);
            }
        }
        for (i3=2*sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=3; j3 < sx3; j3+=6) {        // G-points (2/2)
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3] -
                      outG[base - sx3] +
                      outR[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP(tmp, outR[base]);
            }
            for (j3=6; j3 < sx3 - 3; j3+=6) {        // B-points
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outR[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outR[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outR[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP(tmp, outR[base]);
            }
        }

        // process BLUE channel
        for (i = 0,i3=0; i < sy*sx; i += (sx<<1), i3 += (sx3<<1)) {
            for (j = 1, j3=3; j < sx - 2; j += 2, j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - 3] -
                      outG[base - 3] +
                      outB[base + 3] -
                      outG[base + 3]) >> 1);
                CLIP(tmp, outB[base]);
            }
        }
        for (i3=sx3; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {
            for (j3=0; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3] -
                      outG[base - sx3] +
                      outB[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP(tmp, outB[base]);
            }
            for (j3=3; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outB[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outB[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outB[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP(tmp, outB[base]);
            }
        }
        break;
    default:
        g_assert_not_reached();
    }

    ClearBorders(rgb, sx, sy, 3);
}


static void
ol_bayer_edge_sense_decode_uint16(const guint16 *restrict bayer,
                                  guint16 *restrict rgb,
                                  gsize sx,
                                  gsize sy,
                                  OLBayerFilter filter,
                                  int bits)
{
    guint16 *outR, *outG, *outB;
    register int i3, j3, base;
    int i, j;
    int dh, dv;
    int tmp;
    int sx3=sx*3;

    // sx and sy should be even
    switch (filter) {
    case OL_BAYER_FILTER_GRBG:
    case OL_BAYER_FILTER_BGGR:
        outR = &rgb[0];
        outG = &rgb[1];
        outB = &rgb[2];
        break;
    case OL_BAYER_FILTER_GBRG:
    case OL_BAYER_FILTER_RGGB:
        outR = &rgb[2];
        outG = &rgb[1];
        outB = &rgb[0];
        break;
    default:
        g_assert_not_reached();
    }

    switch (filter) {
    case OL_BAYER_FILTER_GRBG:        //---------------------------------------------------------
    case OL_BAYER_FILTER_GBRG:
        // copy original RGB data to output images
        for (i = 0, i3=0; i < sy*sx; i += (sx<<1), i3 += (sx3<<1)) {
            for (j = 0, j3=0; j < sx; j += 2, j3+=6) {
                base=i3+j3;
                outG[base]           = bayer[i + j];
                outG[base + sx3 + 3] = bayer[i + j + sx + 1];
                outR[base + 3]       = bayer[i + j + 1];
                outB[base + sx3]     = bayer[i + j + sx];
            }
        }
        // process GREEN channel
        for (i3= 3*sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=6; j3 < sx3 - 9; j3+=6) {
                base=i3+j3;
                dh = abs(((outB[base - 6] +
                           outB[base + 6]) >> 1) -
                         outB[base]);
                dv = abs(((outB[base - (sx3<<1)] +
                           outB[base + (sx3<<1)]) >> 1) -
                         outB[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP16(tmp, outG[base], bits);
            }
        }
		
        for (i3=2*sx3; i3 < (sy - 3)*sx3; i3 += (sx3<<1)) {
            for (j3=9; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                dh = abs(((outR[base - 6] +
                           outR[base + 6]) >>1 ) -
                         outR[base]);
                dv = abs(((outR[base - (sx3<<1)] +
                           outR[base + (sx3<<1)]) >>1 ) -
                         outR[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP16(tmp, outG[base], bits);
            }
        }
        // process RED channel
        for (i3=0; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {
            for (j3=6; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - 3] -
                      outG[base - 3] +
                      outR[base + 3] -
                      outG[base + 3]) >> 1);
                CLIP16(tmp, outR[base], bits);
            }
        }
        for (i3=sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=3; j3 < sx3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3] -
                      outG[base - sx3] +
                      outR[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP16(tmp, outR[base], bits);
            }
            for (j3=6; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outR[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outR[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outR[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP16(tmp, outR[base], bits);
            }
        }

        // process BLUE channel
        for (i3=sx3; i3 < sy*sx3; i3 += (sx3<<1)) {
            for (j3=3; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - 3] -
                      outG[base - 3] +
                      outB[base + 3] -
                      outG[base + 3]) >> 1);
                CLIP16(tmp, outB[base], bits);
            }
        }
        for (i3=2*sx3; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {
            for (j3=0; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3] -
                      outG[base - sx3] +
                      outB[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP16(tmp, outB[base], bits);
            }
            for (j3=3; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outB[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outB[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outB[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP16(tmp, outB[base], bits);
            }
        }
        break;

    case OL_BAYER_FILTER_BGGR:        //---------------------------------------------------------
    case OL_BAYER_FILTER_RGGB:
        // copy original RGB data to output images
        for (i = 0, i3=0; i < sy*sx; i += (sx<<1), i3 += (sx3<<1)) {
            for (j = 0, j3=0; j < sx; j += 2, j3+=6) {
                base=i3+j3;
                outB[base] = bayer[i + j];
                outR[base + sx3 + 3] = bayer[i + sx + (j + 1)];
                outG[base + 3] = bayer[i + j + 1];
                outG[base + sx3] = bayer[i + sx + j];
            }
        }
        // process GREEN channel
        for (i3=2*sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=6; j3 < sx3 - 9; j3+=6) {
                base=i3+j3;
                dh = abs(((outB[base - 6] +
                           outB[base + 6]) >> 1) -
                         outB[base]);
                dv = abs(((outB[base - (sx3<<1)] +
                           outB[base + (sx3<<1)]) >> 1) -
                         outB[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP16(tmp, outG[base], bits);
            }
        }
        for (i3=3*sx3; i3 < (sy - 3)*sx3; i3 += (sx3<<1)) {
            for (j3=9; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                dh = abs(((outR[base - 6] +
                           outR[base + 6]) >> 1) -
                         outR[base]);
                dv = abs(((outR[base - (sx3<<1)] +
                           outR[base + (sx3<<1)]) >> 1) -
                         outR[base]);
                tmp = (((outG[base - 3]   + outG[base + 3]) >> 1) * (dh<=dv) +
                       ((outG[base - sx3] + outG[base + sx3]) >> 1) * (dh>dv));
                //tmp = (dh==dv) ? tmp>>1 : tmp;
                CLIP16(tmp, outG[base], bits);
            }
        }
        // process RED channel
        for (i3=sx3; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {        // G-points (1/2)
            for (j3=6; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - 3] -
                      outG[base - 3] +
                      outR[base + 3] -
                      outG[base + 3]) >>1);
                CLIP16(tmp, outR[base], bits);
            }
        }
        for (i3=2*sx3; i3 < (sy - 2)*sx3; i3 += (sx3<<1)) {
            for (j3=3; j3 < sx3; j3+=6) {        // G-points (2/2)
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3] -
                      outG[base - sx3] +
                      outR[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP16(tmp, outR[base], bits);
            }
            for (j3=6; j3 < sx3 - 3; j3+=6) {        // B-points
                base=i3+j3;
                tmp = outG[base] +
                    ((outR[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outR[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outR[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outR[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP16(tmp, outR[base], bits);
            }
        }

        // process BLUE channel
        for (i = 0,i3=0; i < sy*sx; i += (sx<<1), i3 += (sx3<<1)) {
            for (j = 1, j3=3; j < sx - 2; j += 2, j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - 3] -
                      outG[base - 3] +
                      outB[base + 3] -
                      outG[base + 3]) >> 1);
                CLIP16(tmp, outB[base], bits);
            }
        }
        for (i3=sx3; i3 < (sy - 1)*sx3; i3 += (sx3<<1)) {
            for (j3=0; j3 < sx3 - 3; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3] -
                      outG[base - sx3] +
                      outB[base + sx3] -
                      outG[base + sx3]) >> 1);
                CLIP16(tmp, outB[base], bits);
            }
            for (j3=3; j3 < sx3 - 6; j3+=6) {
                base=i3+j3;
                tmp = outG[base] +
                    ((outB[base - sx3 - 3] -
                      outG[base - sx3 - 3] +
                      outB[base - sx3 + 3] -
                      outG[base - sx3 + 3] +
                      outB[base + sx3 - 3] -
                      outG[base + sx3 - 3] +
                      outB[base + sx3 + 3] -
                      outG[base + sx3 + 3]) >> 2);
                CLIP16(tmp, outB[base], bits);
            }
        }
        break;
    default:
        g_assert_not_reached();
    }

    ClearBorders_uint16(rgb, sx, sy, 3);
}
