/*
 * oldlight-image-io.h: image io
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IMAGE_IO_H__
#define OL_IMAGE_IO_H__

#include "oldlight/oldlight-image.h"

G_BEGIN_DECLS

#define OL_TYPE_IMAGE_IO ol_image_io_get_type()

G_DECLARE_DERIVABLE_TYPE(OLImageIO, ol_image_io, OL, IMAGE_IO, GObject);

struct _OLImageIOClass {
    GObjectClass parent_class;

    gboolean (*open)(OLImageIO *io,
                     GError **err);
    gboolean (*close)(OLImageIO *io,
                      GError **err);
};


const gchar *ol_image_io_get_filename(OLImageIO *io);
guint ol_image_io_get_width(OLImageIO *io);
guint ol_image_io_get_height(OLImageIO *io);
OLMatrixFormat ol_image_io_get_format(OLImageIO *io);
OLImageChannels ol_image_io_get_channels(OLImageIO *io);

void ol_image_io_set_width(OLImageIO *io,
                           guint width);
void ol_image_io_set_height(OLImageIO *io,
                            guint height);
void ol_image_io_set_format(OLImageIO *io,
                            OLMatrixFormat format);
void ol_image_io_set_channels(OLImageIO *io,
                              OLImageChannels channels);

gboolean ol_image_io_open(OLImageIO *io,
                          GError **err);

gboolean ol_image_io_close(OLImageIO *io,
                           GError **err);

G_END_DECLS

#endif /* OL_IMAGE_IO_H___ */
