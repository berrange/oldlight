/*
 * oldlight-catalog.c: list of stars
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <math.h>

#include "oldlight/oldlight-catalog.h"

struct _OLCatalog
{
    GObject parent;

    OLStar **stars;
    gsize nstars;

    gsize width;
    gsize height;
};

G_DEFINE_TYPE(OLCatalog, ol_catalog, G_TYPE_OBJECT);

enum {
    PROP_0,
    PROP_WIDTH,
    PROP_HEIGHT,
};


static void ol_catalog_get_property(GObject *object,
                                    guint prop_id,
                                    GValue *value,
                                    GParamSpec *pspec)
{
    OLCatalog *catalog = OL_CATALOG(object);

    switch (prop_id) {
    case PROP_WIDTH:
        g_value_set_int(value, catalog->width);
        break;

    case PROP_HEIGHT:
        g_value_set_int(value, catalog->height);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_catalog_set_property(GObject *object,
                                    guint prop_id,
                                    const GValue *value,
                                    GParamSpec *pspec)
{
    OLCatalog *catalog = OL_CATALOG(object);

    switch (prop_id) {
    case PROP_WIDTH:
        catalog->width = g_value_get_int(value);
        break;

    case PROP_HEIGHT:
        catalog->height = g_value_get_int(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_catalog_finalize(GObject *object)
{
    OLCatalog *catalog = OL_CATALOG(object);
    gsize i;

    for (i = 0; i < catalog->nstars; i++)
        ol_star_free(catalog->stars[i]);
    g_free(catalog->stars);

    G_OBJECT_CLASS(ol_catalog_parent_class)->finalize(object);
}

static void ol_catalog_class_init(OLCatalogClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = ol_catalog_finalize;
    object_class->get_property = ol_catalog_get_property;
    object_class->set_property = ol_catalog_set_property;

    g_object_class_install_property(object_class,
                                    PROP_WIDTH,
                                    g_param_spec_int("width",
                                                     "Image width",
                                                     "Image width",
                                                     0, 65536, 0,
                                                     G_PARAM_READWRITE |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_HEIGHT,
                                    g_param_spec_int("height",
                                                     "Image height",
                                                     "Image height",
                                                     0, 65536, 0,
                                                     G_PARAM_READWRITE |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB));
}

static void ol_catalog_init(OLCatalog *catalog G_GNUC_UNUSED)
{
}


OLCatalog *ol_catalog_new(gsize width, gsize height)
{
    return OL_CATALOG(g_object_new(OL_TYPE_CATALOG,
                                   "width", width,
                                   "height", height,
                                   NULL));
}

gsize ol_catalog_get_nstars(OLCatalog *catalog)
{
    return catalog->nstars;
}

OLStar *ol_catalog_get_star(OLCatalog *catalog, gsize i)
{
    if (i >= catalog->nstars)
        return NULL;

    return catalog->stars[i];
}

OLStar **ol_catalog_get_stars(OLCatalog *catalog)
{
    return catalog->stars;
}


void ol_catalog_add_star(OLCatalog *catalog,
                         OLStar *star)
{
    catalog->stars = g_renew(OLStar *, catalog->stars, catalog->nstars + 1);
    catalog->stars[catalog->nstars++] = star;
}


OLCatalog *ol_catalog_copy(OLCatalog *catalog,
                           OLCatalogFilter filter,
                           gpointer opaque)
{
    OLCatalog *fcatalog = ol_catalog_new(catalog->width, catalog->height);
    gsize i;

    for (i = 0; i < catalog->nstars; i++) {
        if (!filter || filter(catalog->stars[i], opaque)) {
            ol_catalog_add_star(fcatalog,
                                ol_star_copy(catalog->stars[i]));
        }
    }

    return fcatalog;
}


OLCatalog *ol_catalog_copy_transform(OLCatalog *catalog,
                                     OLTransform *transform)
{
    OLCatalog *fcatalog = ol_catalog_new(catalog->width, catalog->height);
    gsize i;

    for (i = 0; i < catalog->nstars; i++) {
        ol_catalog_add_star(fcatalog,
                            ol_star_copy_transform(catalog->stars[i], transform));
    }

    return fcatalog;
}


void ol_catalog_render(OLCatalog *catalog,
                       OLImage *image)
{
    gsize s;

    for (s = 0; s < catalog->nstars; s++) {
        gushort pixel[3] = {
            random() * 65536 / RAND_MAX,
            random() * 65536 / RAND_MAX,
            random() * 65536 / RAND_MAX,
        };
        double x1, x2, y1, y2;
        x1 = catalog->stars[s]->location.x - 20;
        x2 = catalog->stars[s]->location.x + 20;
        y1 = catalog->stars[s]->location.y - 20;
        y2 = catalog->stars[s]->location.y + 20;

        ol_image_draw_rect(image, x1, y1, x2, y2, (guchar *)pixel);
    }
}


static int ol_catalog_compare_flux(const void *a, const void *b)
{
    OLStar *const *as = a;
    OLStar *const *bs = b;

    double rel = ((*as)->flux - (*bs)->flux);
    if (rel < 0) {
        return -1;
    } else if (rel > 0) {
        return 1;
    } else {
        return 0;
    }
}

static int ol_catalog_compare_rflux(const void *a, const void *b)
{
    return -1 * ol_catalog_compare_flux(a, b);
}

void ol_catalog_sort_flux(OLCatalog *catalog, gboolean asc)
{
    qsort(catalog->stars, catalog->nstars, sizeof(*catalog->stars),
          asc ? ol_catalog_compare_flux : ol_catalog_compare_rflux);
}


struct OLCatalogRegion {
    OLLocation location;
    gdouble radius;
};

static gboolean ol_catalog_filter_region(OLStar *star,
                                         gpointer opaque)
{
    struct OLCatalogRegion *region = opaque;

    return ol_location_distance(&star->location, &region->location) < region->radius;
}

gsize ol_catalog_make_quads(OLCatalog *catalog,
                            OLIdentity *identity,
                            gsize segments,
                            gsize maxstars,
                            gdouble mindist)
{
    OLCatalog **cats = 0;
    gsize ncats = segments * segments;
    gsize radius = MAX(catalog->width, catalog->height) / (segments + 1);
    gsize x, y, i;

    radius = sqrt(radius * radius * 2);

    cats = g_new(OLCatalog *, ncats);
    if (segments == 1) {
        cats[0] = ol_catalog_copy(catalog, NULL, NULL);
    } else {
        for (y = 0; y < segments; y++) {
            for (x = 0; x < segments; x++) {
                struct OLCatalogRegion region = {
                    .location = {
                        .x = (gdouble)catalog->width / (segments + 1) * x,
                        .y = (gdouble)catalog->height / (segments + 1) * y,
                    },
                    .radius = radius,
                };
                cats[y * segments + x] = ol_catalog_copy(catalog, ol_catalog_filter_region, &region);
            }
        }
    }

    gsize totquads = 0;
    for (i = 0; i < ncats ; i++) {
        ol_catalog_sort_flux(cats[i], FALSE);
        if (cats[i]->nstars < 4)
            continue;

        gsize a = 0, b = 1, c = 2, d = 3;
        gsize last = MIN(cats[i]->nstars, maxstars);
        do {
            OLStar *as = cats[i]->stars[a];
            OLStar *bs = cats[i]->stars[b];
            OLStar *cs = cats[i]->stars[c];
            OLStar *ds = cats[i]->stars[d];

            if (ol_star_distance(as, bs) > mindist) {
                OLQuad *quad = ol_quad_new(&as->location,
                                           &bs->location,
                                           &cs->location,
                                           &ds->location);
                ol_identity_add_quad(identity, quad);
                totquads++;
            }

            d++;
            if (d == last) {
                c++;
                if (c == last - 1) {
                    b++;
                    if (b == last - 2) {
                        a++;
                        b = a + 1;
                    }
                    c = b + 1;
                }
                d = c + 1;
            }
        } while (a < (last - 3));
    }

    for (i = 0; i < ncats ; i++) {
        g_object_unref(cats[i]);
    }
    g_free(cats);

    return totquads;
}


gboolean ol_catalog_save(OLCatalog *catalog,
                         const gchar *filename,
                         GError **error)
{
    GKeyFile *file = g_key_file_new();
    gboolean ret;
    gsize i;

    g_key_file_set_integer(file, "image", "width", catalog->width);
    g_key_file_set_integer(file, "image", "height", catalog->height);

    g_key_file_set_int64(file, "stars", "count", catalog->nstars);
    for (i = 0; i < catalog->nstars; i++) {
        OLStar *star = catalog->stars[i];
        gchar *group = g_strdup_printf("star%zd", i);

        g_key_file_set_double(file, group, "x", star->location.x);
        g_key_file_set_double(file, group, "y", star->location.y);
        g_key_file_set_double(file, group, "roundness1", star->roundness1);
        g_key_file_set_double(file, group, "roundness2", star->roundness2);
        g_key_file_set_double(file, group, "sharpness", star->sharpness);
        g_key_file_set_double(file, group, "flux", star->flux);
        g_key_file_set_double(file, group, "magnitude", star->magnitude);

        g_free(group);
    }

    ret = g_key_file_save_to_file(file, filename, error);
    g_key_file_free(file);
    return ret;
}


gboolean ol_catalog_load(OLCatalog *catalog,
                         const gchar *filename,
                         GError **error)
{
    GKeyFile *file = g_key_file_new();
    gboolean ret = FALSE;
    GError *localerr = NULL;
    gchar *group = NULL;
    gsize i;

    for (i = 0; i < catalog->nstars; i++)
        ol_star_free(catalog->stars[i]);
    g_free(catalog->stars);

    if (!g_key_file_load_from_file(file, filename, G_KEY_FILE_NONE, error))
        goto cleanup;

    catalog->width = g_key_file_get_integer(file, "image", "width", &localerr);
    if (localerr)
        goto cleanup;

    catalog->height = g_key_file_get_integer(file, "image", "height", &localerr);
    if (localerr)
        goto cleanup;

    catalog->nstars = g_key_file_get_int64(file, "stars", "count", &localerr);
    if (localerr)
        goto cleanup;

    catalog->stars = g_new0(OLStar *, catalog->nstars);

    for (i = 0; i < catalog->nstars; i++) {
        group = g_strdup_printf("star%zd", i);

        catalog->stars[i] = g_new0(OLStar, 1);

        catalog->stars[i]->location.x = g_key_file_get_double(file, group, "x", &localerr);
        if (localerr)
            goto cleanup;

        catalog->stars[i]->location.y = g_key_file_get_double(file, group, "y", &localerr);
        if (localerr)
            goto cleanup;

        catalog->stars[i]->roundness1 = g_key_file_get_double(file, group, "roundness1", &localerr);
        if (localerr)
            goto cleanup;

        catalog->stars[i]->roundness2 = g_key_file_get_double(file, group, "roundness2", &localerr);
        if (localerr)
            goto cleanup;

        catalog->stars[i]->sharpness = g_key_file_get_double(file, group, "sharpness", &localerr);
        if (localerr)
            goto cleanup;

        catalog->stars[i]->flux = g_key_file_get_double(file, group, "flux", &localerr);
        if (localerr)
            goto cleanup;

        catalog->stars[i]->magnitude = g_key_file_get_double(file, group, "magnitude", &localerr);
        if (localerr)
            goto cleanup;

        g_free(group);
        group = NULL;
    }

    ret = TRUE;

 cleanup:
    if (!ret) {
        for (i = 0; i < catalog->nstars; i++)
            ol_star_free(catalog->stars[i]);
        g_free(catalog->stars);
    }
    if (localerr)
        g_propagate_error(error, localerr);
    g_free(group);
    g_key_file_free(file);
    return ret;
}
