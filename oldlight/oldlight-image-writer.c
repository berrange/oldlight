/*
 * oldlight-image-writer.c: image writer
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-writer.h"


#include <unistd.h>

G_DEFINE_TYPE(OLImageWriter, ol_image_writer, OL_TYPE_IMAGE_IO);


static void ol_image_writer_class_init(OLImageWriterClass *klass G_GNUC_UNUSED)
{
}


static void ol_image_writer_init(OLImageWriter *writer G_GNUC_UNUSED)
{
}


gboolean ol_image_writer_save(OLImageWriter *writer,
                              OLImage *image,
                              GError **err)
{
    gboolean ret;
    if (!ol_image_io_open(OL_IMAGE_IO(writer), err))
        return FALSE;
    ret = ol_image_writer_save_payload(writer, image, 0, 0,
                                       ol_image_io_get_width(OL_IMAGE_IO(writer)),
                                       ol_image_io_get_height(OL_IMAGE_IO(writer)),
                                       err);
    if (!ol_image_io_close(OL_IMAGE_IO(writer), ret ? err : NULL)) {
        ret = FALSE;
    }
    if (!ret)
        unlink(ol_image_io_get_filename(OL_IMAGE_IO(writer)));
    return TRUE;
}


gboolean ol_image_writer_save_payload(OLImageWriter *writer,
                                      OLImage *image,
                                      guint x, guint y,
                                      guint width, guint height,
                                      GError **err)
{
    return OL_IMAGE_WRITER_GET_CLASS(writer)->save_payload(writer,
                                                           image,
                                                           x, y,
                                                           width, height,
                                                           err);
}
