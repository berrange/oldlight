/*
 * oldlight-image-list.h: image list
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IMAGE_LIST_H__
#define OL_IMAGE_LIST_H__

#include "oldlight/oldlight-image.h"

G_BEGIN_DECLS

#define OL_TYPE_IMAGE_LIST ol_image_list_get_type()

G_DECLARE_FINAL_TYPE(OLImageList, ol_image_list, OL, IMAGE_LIST, GObject);

OLImageList *ol_image_list_new(void);

gboolean ol_image_list_add_file(OLImageList *list,
                                const gchar *file,
                                GError **err);

gboolean ol_image_list_add_dir(OLImageList *list,
                               const gchar *dir,
                               GError **err);

GList *ol_image_list_get_files(OLImageList *list);


gboolean ol_image_list_open(OLImageList *list,
                            GError **err);

guint ol_image_list_get_width(OLImageList *list);
guint ol_image_list_get_height(OLImageList *list);
OLImageChannels ol_image_list_get_channels(OLImageList *list);
OLMatrixFormat ol_image_list_get_format(OLImageList *list);
gsize ol_image_list_get_count(OLImageList *list);

OLImage **ol_image_list_load_lines(OLImageList *list, guint line, guint count, GError **err);

G_END_DECLS

#endif /* OL_IMAGE_LIST_H___ */
