/*
 * oldlight.c: deep sky star field image stacking
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-star.h"
#include "oldlight/oldlight-image.h"
#include "oldlight/oldlight-extractor.h"

int main(int argc, char **argv)
{
    GError *err = NULL;
    GOptionContext *context;
    gboolean verbose = FALSE;
    gboolean load_cache = FALSE;
    gboolean save_identity = FALSE;
    gboolean save_catalog = FALSE;
    gboolean save_annotated = FALSE;
    gboolean save_mapping = FALSE;
    gchar **images = NULL;
    GOptionEntry options [] = {
        { "verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, "Display progress info", 0 },
        { "load-cache", 0, 0, G_OPTION_ARG_NONE, &load_cache, "Load cached state", 0 },
        { "save-catalog", 0, 0, G_OPTION_ARG_NONE, &save_catalog, "Save star catalog", 0 },
        { "save-identity", 0, 0, G_OPTION_ARG_NONE, &save_identity, "Save image identity", 0 },
        { "save-annotated", 0, 0, G_OPTION_ARG_NONE, &save_annotated, "Save annotated image", 0 },
        { "save-mapping", 0, 0, G_OPTION_ARG_NONE, &save_mapping, "Save mapping images", 0 },
        { G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &images, "Image filenames", 0 },
        { NULL, 0, 0, G_OPTION_ARG_NONE, NULL, NULL, 0 }
    };
    gsize i, x, y;

    /* Setup command line options */
    context = g_option_context_new("");
    g_option_context_add_main_entries(context, options, NULL);
    g_option_context_parse(context, &argc, &argv, &err);
    if (err) {
        g_printerr("%s\n"
                   "Run '%s --help' to see a full list of available command line options\n",
                   err->message, argv[0]);
        g_error_free(err);
        return 1;
    }
    g_option_context_free(context);

    OLExtractor *ext = ol_extractor_new();
    OLIdentity *refident = NULL;
    OLImage *refimg = NULL;
    for (i = 0; images && images[i]; i++) {
        const char *filename = images[i];
        OLImage *imgin = NULL;
        OLImage *imgout = NULL;
        OLCatalog *cat = NULL;
        gsize nquads = 0;
        OLIdentity *ident = NULL;
        OLMapping *mapping = NULL;
        guchar white[] = { 255, 255, 255, 255, 255, 255 };
        gdouble mindist;
        gchar *tmp = strrchr(filename, '.');
        gchar *barename;

        if (!tmp) {
            g_printerr("Missing filename extension in '%s'\n", filename);
            continue;
        }

        barename = g_strndup(filename, (tmp - filename));

        if (verbose)
            g_printerr("Loading %s\n", filename);
        imgin = ol_image_load(filename, &err);
        if (!imgin) {
            g_printerr("Failed to load %s: %s\n", filename, err->message);
            g_error_free(err);
            err = NULL;
            continue;
        }
        imgout = ol_image_convert(imgin, OL_MATRIX_FORMAT_UINT16, OL_IMAGE_CHANNELS_RGB);

        if (verbose)
            g_printerr("...finding stars");
        if (load_cache) {
            gchar *fname = g_strdup_printf("%s.olc", barename);
            cat = ol_catalog_new(imgin->width, imgin->height);
            if (!ol_catalog_load(cat, fname, NULL)) {
                g_object_unref(cat);
                cat = NULL;
            }
            g_free(fname);
        }
        if (!cat)
            cat = ol_extractor_find_stars_daofind(ext, imgin,
                                                  10.0, 10.0, 1.0, 0.0, 1.5,
                                                  0.2, 1.0, -1.0, 1.0, 0, FALSE);
        if (verbose)
            g_printerr(" %zu\n", ol_catalog_get_nstars(cat));

        if (save_catalog) {
            gchar *fname = g_strdup_printf("%s.olc", barename);
            ol_catalog_save(cat, fname, &err);
            g_free(fname);
            if (err) {
                g_printerr("Unable to save catalog %s: %s", fname, err->message);
                g_error_free(err);
                goto cleanup;
            }
        }

        if (verbose)
            g_printerr("...making quads");

        if (load_cache) {
            gchar *fname = g_strdup_printf("%s.oli", barename);
            ident = ol_identity_new(imgin->width, imgin->height);
            if (!ol_identity_load(ident, fname, NULL)) {
                g_object_unref(ident);
                ident = NULL;
            } else {
                nquads = ol_identity_get_nquads(ident);
            }
            g_free(fname);
        }

        if (!ident) {
            ident = ol_identity_new(imgin->width, imgin->height);

            mindist = MIN((gdouble)MAX(imgin->width, imgin->height) / 10, 30);

            nquads = ol_catalog_make_quads(cat, ident, 1, 9, mindist);
            //nquads += ol_catalog_make_quads(cat, ident, 3, 7, mindist);
            //nquads += ol_catalog_make_quads(cat, ident, 5, 5, mindist);
            //nquads += ol_catalog_make_quads(cat, ident, 9, 5, mindist);
        }

        if (verbose)
            g_printerr(" %zu\n", nquads);

        if (save_identity) {
            gchar *fname = g_strdup_printf("%s.oli", barename);
            ol_identity_save(ident, fname, &err);
            g_free(fname);
            if (err) {
                g_printerr("Unable to save catalog %s: %s", fname, err->message);
                g_error_free(err);
                goto cleanup;
            }
        }

        if (refident == NULL) {
            refident = g_object_ref(ident);
            refimg = ol_image_copy(imgout);
        } else {
            if (verbose)
                g_printerr("...matching quads");
            mapping = ol_identity_match(refident, ident, 0.01);
            if (mapping) {
                g_printerr(" %zd\n", ol_mapping_get_nquad_pairs(mapping));
                if (save_mapping) {
                    OLImage *refout = ol_image_copy(refimg);
                    OLImage *matchout = ol_image_copy(imgout);
                    gchar *fname;

                    ol_mapping_render_ref(mapping, refout);
                    ol_mapping_render_match(mapping, matchout);

                    fname = g_strdup_printf("%s-ref.pgm", barename);
                    ol_image_save(refout, fname, NULL);
                    g_free(fname);

                    fname = g_strdup_printf("%s-match.pgm", barename);
                    ol_image_save(matchout, fname, NULL);
                    g_free(fname);

                    ol_image_free(refout);
                    ol_image_free(matchout);
                }
                OLTransform *trans = ol_mapping_build_transform(mapping);
                g_printerr("...offset dx=%f dy=%f rot=%f \n",
                           trans->xshift, trans->yshift, trans->rotation * 180 / G_PI);
                ol_transform_free(trans);
            } else {
                g_printerr(" 0\n");
            }
        }

        if (save_annotated) {
            gchar *fname = g_strdup_printf("%s-annotated.pgm", barename);
            gsize regions = 4;

            for (x = 1; x < regions; x++) {
                ol_image_draw_line(imgout,
                                   (double)imgin->width * x / regions,
                                   0,
                                   (double)imgin->width * x / regions,
                                   imgin->height,
                                   white);
            }
            for (y = 1; y < regions; y++) {
                ol_image_draw_line(imgout,
                                   0,
                                   (double)imgin->height * y / regions,
                                   imgin->width,
                                   (double)imgin->height * y / regions,
                                   white);
            }

            ol_catalog_render(cat, imgout);
            ol_identity_render(ident, imgout);
            ol_image_save(imgout, fname, NULL);
            g_free(fname);
        }

    cleanup:
        g_object_unref(ident);
        g_object_unref(cat);
        if (mapping)
            g_object_unref(mapping);
        ol_image_free(imgin);
        if (imgout)
            ol_image_free(imgout);
        g_free(barename);
        if (err)
            break;
    }

    if (refident)
        g_object_unref(refident);
    if (refimg)
        ol_image_free(refimg);

    g_object_unref(ext);

    return err ? 1 : 0;
}
