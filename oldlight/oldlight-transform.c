/*
 * oldlight-transform.c: transformation between two locations
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>
#include <math.h>

#include "oldlight/oldlight-transform.h"

GType ol_transform_get_type(void)
{
    static GType transform_type = 0;

    if (G_UNLIKELY(transform_type == 0)) {
        transform_type = g_boxed_type_register_static
            ("OLTransform",
             (GBoxedCopyFunc)ol_transform_copy,
             (GBoxedFreeFunc)ol_transform_free);
    }

    return transform_type;
}

OLTransform *ol_transform_new(gdouble rotation,
                              gdouble xshift,
                              gdouble yshift)
{
    OLTransform *transform = g_new0(OLTransform, 1);

    transform->rotation = rotation;
    transform->xshift = xshift;
    transform->yshift = yshift;

    transform->affine = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, 3, 3);

    transform->affine->data.f64[0] = cos(rotation);
    transform->affine->data.f64[1] = sin(rotation);
    transform->affine->data.f64[2] = 0.0;

    transform->affine->data.f64[3] = -sin(rotation);
    transform->affine->data.f64[4] = cos(rotation);
    transform->affine->data.f64[5] = 0.0;

    transform->affine->data.f64[6] = xshift;
    transform->affine->data.f64[7] = yshift;
    transform->affine->data.f64[8] = 1.0;

    return transform;
}

OLTransform *ol_transform_copy(OLTransform *transform)
{
    OLTransform *newtransform = g_new0(OLTransform, 1);

    memcpy(newtransform, transform, sizeof(*transform));
    newtransform->affine = ol_matrix_copy(transform->affine);

    return newtransform;
}

void ol_transform_free(OLTransform *transform)
{
    if (!transform)
        return;

    ol_matrix_free(transform->affine);
    g_free(transform);
}

OLLocation *ol_transform_apply(OLTransform *transform,
                               OLLocation *location)
{
    OLMatrix *src = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, 3, 1);
    OLMatrix *dst;
    OLLocation *ret;

    src->data.f64[0] = location->x;
    src->data.f64[1] = location->y;
    src->data.f64[2] = 1;

    dst = ol_matrix_new_product(src, transform->affine);

    ret = ol_location_new(dst->data.f64[0], dst->data.f64[1]);

    ol_matrix_free(dst);

    return ret;
}
