/*
 * oldlight-bayer-vng.h: bayer processing vng algorithm
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_BAYER_VNG_H__
#define OL_BAYER_VNG_H__

#include "oldlight/oldlight-bayer-bilinear.h"

G_BEGIN_DECLS

#define OL_TYPE_BAYER_VNG ol_bayer_vng_get_type()

G_DECLARE_FINAL_TYPE(OLBayerVNG, ol_bayer_vng, OL, BAYER_VNG, OLBayerBilinear);

G_END_DECLS

#endif /* OL_BAYER_VNG_H__ */
