/*
 * oldlight-image.c: image processing
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image.h"
#include "oldlight/oldlight-image-reader-raw.h"
#include "oldlight/oldlight-image-reader-pnm.h"
#include "oldlight/oldlight-image-writer-pnm.h"

#include <stdio.h>
#include <endian.h>
#include <math.h>


#define OL_IMAGE_ERROR ol_image_error_quark()

static GQuark
ol_image_error_quark(void)
{
    return g_quark_from_static_string("ol-image");
}


GType ol_image_get_type(void)
{
    static GType image_type = 0;

    if (G_UNLIKELY(image_type == 0)) {
        image_type = g_boxed_type_register_static
            ("OLImage",
             (GBoxedCopyFunc)ol_image_copy,
             (GBoxedFreeFunc)ol_image_free);
    }

    return image_type;
}


OLImage *ol_image_new(OLMatrixFormat format, OLImageChannels channels,
                      gsize width, gsize height)
{
    OLImage *image = g_new0(OLImage, 1);
    static gsize format_map[] = {
        [OL_MATRIX_FORMAT_UINT8] = 1,
        [OL_MATRIX_FORMAT_UINT16] = 2,
        [OL_MATRIX_FORMAT_UINT32] = 4,
        [OL_MATRIX_FORMAT_UINT64] = 8,
    };

    g_assert(format == OL_MATRIX_FORMAT_UINT8 ||
             format == OL_MATRIX_FORMAT_UINT16 ||
             format == OL_MATRIX_FORMAT_UINT32 ||
             format == OL_MATRIX_FORMAT_UINT64);

    image->width = width;
    image->height = height;
    image->channels = channels;

    image->bpp = channels * format_map[format];
    image->matrix = ol_matrix_new(format, width * channels, height);

    return image;
}

OLImage *ol_image_copy(OLImage *image)
{
    OLImage *newimage = g_new0(OLImage, 1);

    memcpy(newimage, image, sizeof(*image));
    newimage->matrix = ol_matrix_copy(image->matrix);

    return newimage;
}

void ol_image_free(OLImage *image)
{
    if (!image)
        return;

    ol_matrix_free(image->matrix);
    g_free(image);
}


void ol_image_data_be_to_host(OLImage *image)
{
    gsize n;
    gsize len = image->width * image->height * image->channels;

    switch (image->matrix->format) {
    case OL_MATRIX_FORMAT_UINT8:
        break;

    case OL_MATRIX_FORMAT_UINT16: {
        guint16 *pixels = image->matrix->data.u16;
        for (n = 0; n < len ; n++) {
            *pixels = be16toh(*pixels);
            pixels++;
        }
    }   break;
    case OL_MATRIX_FORMAT_UINT32: {
        guint32 *pixels = image->matrix->data.u32;
        for (n = 0; n < len ; n++) {
            *pixels = be32toh(*pixels);
            pixels++;
        }
    }   break;
    case OL_MATRIX_FORMAT_UINT64: {
        guint64 *pixels = image->matrix->data.u64;
        for (n = 0; n < len ; n++) {
            *pixels = be64toh(*pixels);
            pixels++;
        }
    }   break;
    default:
        g_assert_not_reached();
    }
}


void ol_image_data_host_to_be(OLImage *image)
{
    gsize n;
    gsize len = image->width * image->height * image->channels;

    switch (image->matrix->format) {
    case OL_MATRIX_FORMAT_UINT8:
        break;

    case OL_MATRIX_FORMAT_UINT16: {
        guint16 *pixels = image->matrix->data.u16;
        for (n = 0; n < len ; n++) {
            *pixels = htobe16(*pixels);
            pixels++;
        }
    }   break;
    case OL_MATRIX_FORMAT_UINT32: {
        guint32 *pixels = image->matrix->data.u32;
        for (n = 0; n < len ; n++) {
            *pixels = htobe32(*pixels);
            pixels++;
        }
    }   break;
    case OL_MATRIX_FORMAT_UINT64: {
        guint64 *pixels = image->matrix->data.u64;
        for (n = 0; n < len ; n++) {
            *pixels = htobe64(*pixels);
            pixels++;
        }
    }   break;
    default:
        g_assert_not_reached();
    }
}


OLImage *ol_image_load(const char *filename, GError **err)
{
    OLImage *image = NULL;
    OLImageReader *reader;
    const char *ext = strrchr(filename, '.');
    char *lext;
    gsize i;
    if (!ext) {
        g_set_error(err, OL_IMAGE_ERROR, 0,
                    "Unable to identify file extension '%s'", filename);
        return NULL;
    }

    lext = g_strdup(ext + 1);
    for (i = 0; ext[i] != '\0'; i++)
        lext[i] = g_ascii_tolower(lext[i]);

    if (g_str_equal(lext, "nef") ||
        g_str_equal(lext, "cr2")) {
        reader = OL_IMAGE_READER(ol_image_reader_raw_new(filename));
    } else if (g_str_equal(lext, "pgm") ||
               g_str_equal(lext, "ppm")) {
        reader = OL_IMAGE_READER(ol_image_reader_pnm_new(filename));
    } else {
        g_free(lext);
        g_set_error(err, OL_IMAGE_ERROR, 0,
                    "Cannot load unsupported image file format %s", filename);
        return NULL;
    }
    g_free(lext);

    image = ol_image_reader_load(reader, err);

    g_object_unref(reader);
    return image;
}


gboolean ol_image_save(OLImage *image, const char *filename, GError **err)
{
    OLImageWriter *writer;
    gboolean ret = FALSE;

    if (g_str_has_suffix(filename, ".pgm") ||
        g_str_has_suffix(filename, ".ppm")) {
        writer = OL_IMAGE_WRITER(ol_image_writer_pnm_new(filename,
                                                         image->matrix->format,
                                                         image->channels,
                                                         image->width,
                                                         image->height));
    } else {
        g_set_error(err, OL_IMAGE_ERROR, 0,
                    "Cannot save unsupported image file format %s", filename);
        return FALSE;
    }

    ret = ol_image_writer_save(writer, image, err);

    g_object_unref(writer);
    return ret;
}


#define SCALE_8_TO_64 72340172838076673ULL
#define SCALE_16_TO_64 281479271743489ULL
#define SCALE_32_TO_64 4294967297ULL

OLImage *ol_image_convert(OLImage *image,
                          OLMatrixFormat format,
                          OLImageChannels channels)
{
    OLImage *ret = ol_image_new(format, channels, image->width, image->height);
    gsize len = image->width * image->height;
    gsize n;

    for (n = 0; n < len; n++) {
        guint8 *src = image->matrix->data.u8 + (n * image->bpp);
        guint8 *dst = ret->matrix->data.u8 + (n * ret->bpp);
        gdouble r, g, b;

        if (image->channels == OL_IMAGE_CHANNELS_GRAY) {
            switch (image->matrix->format) {
            case OL_MATRIX_FORMAT_UINT8:
                r = SCALE_8_TO_64 * *src;
                g = SCALE_8_TO_64 * *src;
                b = SCALE_8_TO_64 * *src;
                break;
            case OL_MATRIX_FORMAT_UINT16:
                r = SCALE_16_TO_64 * *(guint16 *)src;
                g = SCALE_16_TO_64 * *(guint16 *)src;
                b = SCALE_16_TO_64 * *(guint16 *)src;
                break;
            case OL_MATRIX_FORMAT_UINT32:
                r = SCALE_32_TO_64 * *(guint32 *)src;
                g = SCALE_32_TO_64 * *(guint32 *)src;
                b = SCALE_32_TO_64 * *(guint32 *)src;
                break;
            case OL_MATRIX_FORMAT_UINT64:
                r = *(guint64 *)src;
                g = *(guint64 *)src;
                b = *(guint64 *)src;
                break;
            default:
                g_assert_not_reached();
            }
        } else {
            switch (image->matrix->format) {
            case OL_MATRIX_FORMAT_UINT8:
                r = SCALE_8_TO_64 * *src;
                g = SCALE_8_TO_64 * *(src + 1);
                b = SCALE_8_TO_64 * *(src + 1);
                break;
            case OL_MATRIX_FORMAT_UINT16:
                r = SCALE_16_TO_64 * *(guint16 *)src;
                g = SCALE_16_TO_64 * *(((guint16 *)src) + 1);
                b = SCALE_16_TO_64 * *(((guint16 *)src) + 2);
                break;
            case OL_MATRIX_FORMAT_UINT32:
                r = SCALE_32_TO_64 * *(guint32 *)src;
                g = SCALE_32_TO_64 * *(((guint32 *)src) + 1);
                b = SCALE_32_TO_64 * *(((guint32 *)src) + 2);
                break;
            case OL_MATRIX_FORMAT_UINT64:
                r = *(guint64 *)src;
                g = *(((guint64 *)src) + 1);
                b = *(((guint64 *)src) + 2);
                break;
            default:
                g_assert_not_reached();
            }
        }

        if (channels == OL_IMAGE_CHANNELS_GRAY) {
            switch (image->matrix->format) {
            case OL_MATRIX_FORMAT_UINT8:
                *dst = round(((r + g + b) / 3) / SCALE_8_TO_64);
                break;
            case OL_MATRIX_FORMAT_UINT16:
                *(guint16 *)dst = round(((r + g + b) / 3) / SCALE_16_TO_64);
                break;
            case OL_MATRIX_FORMAT_UINT32:
                *(guint32 *)dst = round(((r + g + b) / 3) / SCALE_32_TO_64);
                break;
            case OL_MATRIX_FORMAT_UINT64:
                *(guint64 *)dst = round((((gdouble)r + g + b) / 3.0) / SCALE_32_TO_64);
                break;
            default:
                g_assert_not_reached();
            }
        } else {
            switch (image->matrix->format) {
            case OL_MATRIX_FORMAT_UINT8:
                *dst = round(r / SCALE_8_TO_64);
                *(dst+1) = round(g / SCALE_8_TO_64);
                *(dst+2) = round(b / SCALE_8_TO_64);
                break;
            case OL_MATRIX_FORMAT_UINT16:
                *(guint16 *)dst = round(r / SCALE_16_TO_64);
                *(((guint16 *)dst)+1) = round(g / SCALE_16_TO_64);
                *(((guint16 *)dst)+2) = round(b / SCALE_16_TO_64);
                break;
            case OL_MATRIX_FORMAT_UINT32:
                *(guint32 *)dst = round(r / SCALE_32_TO_64);
                *(((guint32 *)dst)+1) = round(g / SCALE_32_TO_64);
                *(((guint32 *)dst)+2) = round(b / SCALE_32_TO_64);
                break;
            case OL_MATRIX_FORMAT_UINT64:
                *(guint64 *)dst = r;
                *(((guint64 *)dst)+1) = g;
                *(((guint64 *)dst)+2) = b;
                break;
            default:
                g_assert_not_reached();
            }
        }
    }

    return ret;
}


#define SWAP(a, b)                              \
    do { a ^= b; b ^= a; a ^= b; } while (0)
#define IN_RANGE(x, y, w, h)                    \
    (x >= 0 && x < w && y >= 0 && y < h)


void ol_image_draw_rect(OLImage *image,
                        gssize x0, gssize y0, gssize x1, gssize y1,
                        guint8 *pixel)
{
    gssize x, y;
    gssize dy = y0 - y0;
    gssize dx = x0 - x0;

    if (dx < 0) {
        SWAP(x0, x0);
    }
    if (dy < 0) {
        SWAP(y0, y0);
    }

    for (x = x0; x < x1; x++) {
        guint8 *tmp;

        if (IN_RANGE(x, y0, image->width, image->height)) {
            tmp = image->matrix->data.u8 + (y0 * image->width * image->bpp) + (x * image->bpp);
            memcpy(tmp, pixel, image->bpp);
        }

        if (IN_RANGE(x, y1, image->width, image->height)) {
            tmp = image->matrix->data.u8 + (y1 * image->width * image->bpp) + (x * image->bpp);
            memcpy(tmp, pixel, image->bpp);
        }
    }

    for (y = y0; y < y1; y++) {
        guint8 *tmp;

        if (IN_RANGE(x0, y, image->width, image->height)) {
            tmp = image->matrix->data.u8 + (y * image->width * image->bpp) + (x0 * image->bpp);
            memcpy(tmp, pixel, image->bpp);
        }

        if (IN_RANGE(x1, y, image->width, image->height)) {
            tmp = image->matrix->data.u8 + (y * image->width * image->bpp) + (x1 * image->bpp);
            memcpy(tmp, pixel, image->bpp);
        }
    }
}

void ol_image_draw_line(OLImage *image,
                        gssize x0, gssize y0, gssize x1, gssize y1,
                        guint8 *pixel)
{
    gssize x, y;
    gssize inc;
    gssize err;
    gssize dy = y1 - y0;
    gssize dx = x1 - x0;

    if (labs(dy) < labs(dx)) {

        if (x0 > x1) {
            SWAP(x0, x1);
            SWAP(y0, y1);
            dy = -dy;
            dx = -dx;
        }

        inc = 1;
        if (dy < 0) {
            inc = -1;
            dy = -dy;
        }

        err = 2 * dy - dx;

        y = y0;
        for (x = x0; x < x1; x++) {
            if (IN_RANGE(x, y, image->width, image->height)) {
                guint8 *tmp = image->matrix->data.u8 + (y * image->width * image->bpp) + (x * image->bpp);
                memcpy(tmp, pixel, image->bpp);
            }
            if (err > 0) {
                y = y + inc;
                err = err - 2 * dx;
            }
            err = err + 2 * dy;
        }
    } else {
        if (y0 > y1) {
            SWAP(x0, x1);
            SWAP(y0, y1);
            dy = -dy;
            dx = -dx;
        }

        inc = 1;
        if (dx < 0) {
            inc = -1;
            dx = -dx;
        }
        err = 2 * dx - dy;

        x = x0;
        for (y = y0; y < y1 && x < image->width && y < image->height; y++) {
            if (IN_RANGE(x, y, image->width, image->height)) {
                guint8 *tmp = image->matrix->data.u8 + (y * image->width * image->bpp) + (x * image->bpp);
                memcpy(tmp, pixel, image->bpp);
            }
            if (err > 0) {
                x = x + inc;
                err = err - 2 * dy;
            }
            err = err + 2 * dx;
        }
    }
}


void ol_image_subtract(OLImage *image,
                       OLImage *other)
{
    gsize i;
    guint8 *a8, *b8;
    guint16 *a16, *b16;
    guint32 *a32, *b32;
    guint64 *a64, *b64;

    switch (image->matrix->format) {
    case OL_MATRIX_FORMAT_UINT8:
        a8 = image->matrix->data.u8;
        b8 = other->matrix->data.u8;
        for (i = 0; i < image->matrix->len; i++, a8++, b8++) {
            if (*b8 > *a8) {
                *a8 = 0;
            } else {
                *a8 -= *b8;
            }
        }
        break;
    case OL_MATRIX_FORMAT_UINT16:
        a16 = image->matrix->data.u16;
        b16 = other->matrix->data.u16;
        for (i = 0; i < image->matrix->len; i++, a16++, b16++) {
            if (*b16 > *a16) {
                *a16 = 0;
            } else {
                *a16 -= *b16;
            }
        }
        break;
    case OL_MATRIX_FORMAT_UINT32:
        a32 = image->matrix->data.u32;
        b32 = other->matrix->data.u32;
        for (i = 0; i < image->matrix->len; i++, a32++, b32++) {
            if (*b32 > *a32) {
                *a32 = 0;
            } else {
                *a32 -= *b32;
            }
        }
        break;
    case OL_MATRIX_FORMAT_UINT64:
        a64 = image->matrix->data.u64;
        b64 = other->matrix->data.u64;
        for (i = 0; i < image->matrix->len; i++, a64++, b64++) {
            if (*b64 > *a64) {
                *a64 = 0;
            } else {
                *a64 -= *b64;
            }
        }
        break;

    default:
        g_assert_not_reached();
    }
}


void ol_image_divide_mean(OLImage *image,
                          OLImage *other,
                          gdouble mean)
{
    gsize i;
    guint8 *a8, *b8;
    guint16 *a16, *b16;
    guint32 *a32, *b32;
    guint64 *a64, *b64;

    switch (image->matrix->format) {
    case OL_MATRIX_FORMAT_UINT8:
        a8 = image->matrix->data.u8;
        b8 = other->matrix->data.u8;
        for (i = 0; i < image->matrix->len; i++, a8++, b8++) {
            if (*b8) {
                *a8 = round(*a8 * mean / *b8);
            } else {
                *a8 = round(*a8 * mean);
            }
        }
        break;
    case OL_MATRIX_FORMAT_UINT16:
        a16 = image->matrix->data.u16;
        b16 = other->matrix->data.u16;
        for (i = 0; i < image->matrix->len; i++, a16++, b16++) {
            if (*b16) {
                *a16 = round(*a16 * mean / *b16);
            } else {
                *a16 = round(*a16 * mean);
            }
        }
        break;
    case OL_MATRIX_FORMAT_UINT32:
        a32 = image->matrix->data.u32;
        b32 = other->matrix->data.u32;
        for (i = 0; i < image->matrix->len; i++, a32++, b32++) {
            if (*b32) {
                *a32 = round(*a32 * mean / *b32);
            } else {
                *a32 = round(*a32 * mean);
            }
        }
        break;
    case OL_MATRIX_FORMAT_UINT64:
        a64 = image->matrix->data.u64;
        b64 = other->matrix->data.u64;
        for (i = 0; i < image->matrix->len; i++, a64++, b64++) {
            if (*b64) {
                *a64 = round(*a64 * mean / *b64);
            } else {
                *a64 = round(*a64 * mean);
            }
        }
        break;

    default:
        g_assert_not_reached();
    }
}
