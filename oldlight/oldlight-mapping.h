/*
 * oldlight-mapping.h: mapping between identities
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_MAPPING_H__
#define OL_MAPPING_H__

#include <glib-object.h>

#include "oldlight/oldlight-quad.h"
#include "oldlight/oldlight-image.h"
#include "oldlight/oldlight-transform.h"

G_BEGIN_DECLS

#define OL_TYPE_MAPPING            (ol_mapping_get_type ())

G_DECLARE_FINAL_TYPE(OLMapping, ol_mapping, OL, MAPPING, GObject);

OLMapping *ol_mapping_new(gsize width,
                          gsize height);

gsize ol_mapping_get_nquad_pairs(OLMapping *mapping);

OLQuad *ol_mapping_get_quad_ref(OLMapping *mapping, gsize i);
OLQuad *ol_mapping_get_quad_match(OLMapping *mapping, gsize i);

void ol_mapping_render_ref(OLMapping *mapping,
                           OLImage *image);
void ol_mapping_render_match(OLMapping *mapping,
                             OLImage *image);

void ol_mapping_add_quad_pair(OLMapping *mapping,
			      OLQuad *ref,
			      OLQuad *match);

OLTransform *ol_mapping_build_transform(OLMapping *mapping);

G_END_DECLS

#endif /* OL_MAPPING_H__ */
