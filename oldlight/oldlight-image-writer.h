/*
 * oldlight-image-writer.h: image writer
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IMAGE_WRITER_H__
#define OL_IMAGE_WRITER_H__

#include "oldlight/oldlight-image-io.h"
#include "oldlight/oldlight-image.h"

G_BEGIN_DECLS

#define OL_TYPE_IMAGE_WRITER ol_image_writer_get_type()

G_DECLARE_DERIVABLE_TYPE(OLImageWriter, ol_image_writer, OL, IMAGE_WRITER, OLImageIO);

struct _OLImageWriterClass {
    OLImageIOClass parent_class;

    gboolean (*save_payload)(OLImageWriter *writer,
                             OLImage *image,
                             guint x,
                             guint y,
                             guint width,
                             guint height,
                             GError **err);
};

gboolean
ol_image_writer_save(OLImageWriter *writer,
                     OLImage *image,
                     GError **err);

gboolean
ol_image_writer_save_payload(OLImageWriter *writer,
                             OLImage *image,
                             guint x,
                             guint y,
                             guint width,
                             guint height,
                             GError **err);

G_END_DECLS

#endif /* OL_IMAGE_WRITER_H___ */
