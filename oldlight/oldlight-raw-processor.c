/*
 * oldlight-raw-processor.c: raw file processor
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-raw-processor.h"

struct _OLRawProcessor
{
    GObject parent;
    OLBayerMethod bayer_method;
    OLRawProcessorWB white_balance;
    OLMatrix *custom_white_balance;
    gboolean stretch;
};


G_DEFINE_TYPE(OLRawProcessor, ol_raw_processor, G_TYPE_OBJECT);

#define OL_RAW_PROCESSOR_ERROR ol_raw_processor_error_quark()

static GQuark
ol_raw_processor_error_quark(void)
{
    return g_quark_from_static_string("ol-raw-processor");
}


enum {
    PROP_0,
    PROP_STRETCH,
    PROP_BAYER_METHOD,
    PROP_WHITE_BALANCE,
    PROP_CUSTOM_WHITE_BALANCE,
};


static void ol_raw_processor_get_property(GObject *object,
                                          guint prop_id,
                                          GValue *value,
                                          GParamSpec *pspec)
{
    OLRawProcessor *proc = OL_RAW_PROCESSOR(object);

    switch (prop_id) {
    case PROP_STRETCH:
        g_value_set_boolean(value, proc->stretch);
        break;

    case PROP_BAYER_METHOD:
        g_value_set_enum(value, proc->bayer_method);
        break;

    case PROP_WHITE_BALANCE:
        g_value_set_enum(value, proc->white_balance);
        break;

    case PROP_CUSTOM_WHITE_BALANCE:
        g_value_set_boxed(value, proc->custom_white_balance);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_raw_processor_set_property(GObject *object,
                                          guint prop_id,
                                          const GValue *value,
                                          GParamSpec *pspec)
{
    OLRawProcessor *proc = OL_RAW_PROCESSOR(object);

    switch (prop_id) {
    case PROP_STRETCH:
        proc->stretch = g_value_get_boolean(value);
        break;

    case PROP_BAYER_METHOD:
        proc->bayer_method = g_value_get_enum(value);
        break;

    case PROP_WHITE_BALANCE:
        proc->white_balance = g_value_get_enum(value);
        break;

    case PROP_CUSTOM_WHITE_BALANCE:
        ol_matrix_free(proc->custom_white_balance);
        proc->custom_white_balance = g_value_dup_boxed(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_raw_processor_class_init(OLRawProcessorClass *klass G_GNUC_UNUSED)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->get_property = ol_raw_processor_get_property;
    object_class->set_property = ol_raw_processor_set_property;

    g_object_class_install_property(object_class,
                                    PROP_STRETCH,
                                    g_param_spec_boolean("stretch",
                                                         "Stretch",
                                                         "Stretch color range",
                                                         FALSE,
                                                         G_PARAM_READWRITE |
                                                         G_PARAM_CONSTRUCT_ONLY |
                                                         G_PARAM_STATIC_NAME |
                                                         G_PARAM_STATIC_NICK |
                                                         G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_BAYER_METHOD,
                                    g_param_spec_enum("bayer-method",
                                                      "Bayer method",
                                                      "Bayer method",
                                                      OL_TYPE_BAYER_METHOD,
                                                      OL_BAYER_METHOD_AHD,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_CONSTRUCT_ONLY |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_WHITE_BALANCE,
                                    g_param_spec_enum("white-balance",
                                                      "White balance",
                                                      "White balance",
                                                      OL_TYPE_RAW_PROCESSOR_WB,
                                                      OL_RAW_PROCESSOR_WB_IMAGE,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_CONSTRUCT_ONLY |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_CUSTOM_WHITE_BALANCE,
                                    g_param_spec_boxed("custom-white-balance",
                                                       "Custom white balance",
                                                       "Custom white balance",
                                                       OL_TYPE_MATRIX,
                                                       G_PARAM_READWRITE |
                                                       G_PARAM_CONSTRUCT_ONLY |
                                                       G_PARAM_STATIC_NAME |
                                                       G_PARAM_STATIC_NICK |
                                                       G_PARAM_STATIC_BLURB));
}


static void ol_raw_processor_init(OLRawProcessor *proc G_GNUC_UNUSED)
{
}


OLRawProcessor *ol_raw_processor_new(OLBayerMethod bayer_method,
                                     OLRawProcessorWB wb,
                                     OLMatrix *custom_wb,
                                     gboolean stretch)
{
    return OL_RAW_PROCESSOR(g_object_new(OL_TYPE_RAW_PROCESSOR,
                                         "bayer-method", bayer_method,
                                         "white-balance", wb,
                                         "custom-white-balance", custom_wb,
                                         "stretch", stretch,
                                         NULL));
}


#define LIM(x, min, max) (MIN(MAX((x), min), max))
#define CLIP8(x) LIM((x), 0, 255)
#define CLIP16(x) LIM((x), 0, 65535)
#define FC(row, col) \
    (params->filters >> ((((row) << 1 & 14) | ((col) & 1)) << 1) & 3)

static gboolean
ol_raw_processor_apply_wb(OLRawProcessor *proc,
                          OLImage *image,
                          OLRawParams *params,
                          GError **err)
{
    gsize row, col;
    if (image->channels != OL_IMAGE_CHANNELS_GRAY) {
        g_set_error(err, OL_RAW_PROCESSOR_ERROR, 0,
                    "White balance can only be applied to pre-bayer image");
        return FALSE;
    }
    gfloat scale[4];
    gfloat dmax = 0;
    gfloat smax = 0, smin = FLT_MAX;

    switch (proc->white_balance) {
    case OL_RAW_PROCESSOR_WB_NONE:
        scale[0] = scale[1] = scale[2] = scale[3] = 1.0;
        break;
    case OL_RAW_PROCESSOR_WB_IMAGE:
        g_assert(G_N_ELEMENTS(scale) == params->img_wb->len);
        memcpy(scale, params->img_wb->data.f32, sizeof(scale));
        break;
    case OL_RAW_PROCESSOR_WB_CAMERA:
        g_assert(G_N_ELEMENTS(scale) == params->cam_wb->len);
        memcpy(scale, params->cam_wb->data.f32, sizeof(scale));
        break;
    case OL_RAW_PROCESSOR_WB_CUSTOM:
        if (!proc->custom_white_balance) {
            g_set_error(err, OL_RAW_PROCESSOR_ERROR, 0,
                        "Missing custom white balance values");
            return FALSE;
        }
        if (proc->custom_white_balance->format != OL_MATRIX_FORMAT_FLOAT32) {
            g_set_error(err, OL_RAW_PROCESSOR_ERROR, 0,
                        "Custom white balance values must be float32");
            return FALSE;
        }
        if (proc->custom_white_balance->len != G_N_ELEMENTS(scale)) {
            g_set_error(err, OL_RAW_PROCESSOR_ERROR, 0,
                        "Require %zd custom white balance values",
                        G_N_ELEMENTS(scale));
            return FALSE;
        }
        memcpy(scale, proc->custom_white_balance->data.f32, sizeof(scale));
        break;
    default:
        g_assert_not_reached();
    }

    scale[0] /= scale[1];
    scale[2] /= scale[1];
    scale[3] /= scale[1];
    scale[1] = 1.0;
    for (int i = 0; i < 4; i++) {
        if (scale[i] > smax)
            smax = scale[i];
        if (scale[i] < smin)
            smin = scale[i];
    }
    // XXX why
    smax = smin;
    if (proc->stretch)
        dmax = ol_matrix_maximum(image->matrix);
    for (int i = 0; i < 4; i++) {
        if (proc->stretch)
            scale[i] = scale[i] / smax * 65536 / dmax;
        else
            scale[i] = scale[i] / smax;
    }
    switch (image->matrix->format) {
    case OL_MATRIX_FORMAT_UINT8: {
        guint8 *v = image->matrix->data.u8;
        for (row = 0 ; row < image->height; row++) {
            for (col = 0; col < image->width; col++) {
                *v = CLIP8(*v * scale[FC(row, col)]);
                v++;
            }
        }
    }   break;
    case OL_MATRIX_FORMAT_UINT16: {
        guint16 *v = image->matrix->data.u16;
        for (row = 0 ; row < image->height; row++) {
            for (col = 0; col < image->width; col++) {
                *v = CLIP16(*v * scale[FC(row, col)]);
                v++;
            }
        }
    }   break;
    case OL_MATRIX_FORMAT_UINT32:
    case OL_MATRIX_FORMAT_UINT64:
        g_set_error(err, OL_RAW_PROCESSOR_ERROR, 0,
                    "White balance can only be applied to 8/16bpp image");
        return FALSE;
    default:
        g_assert_not_reached();
    }
    return TRUE;
}


static gboolean
ol_raw_processor_get_bayer_filter(OLRawParams *params,
                                  OLBayerFilter *filter,
                                  GError **err)
{
    switch (params->filters) {
    case 0x4b4b4b4b:
    case 0x49494949:
        *filter = OL_BAYER_FILTER_GBRG;
        break;
    case 0x61616161:
        *filter = OL_BAYER_FILTER_GRBG;
        break;
    case 0x16161616:
        *filter = OL_BAYER_FILTER_BGGR;
        break;
    case 0xb4b4b4b4:
    case 0x94949494:
        *filter = OL_BAYER_FILTER_RGGB;
        break;
    default:
        g_set_error(err, OL_RAW_PROCESSOR_ERROR, 0,
                    "Unsupported bayer filter 0x%x",
                    params->filters);
        return FALSE;
    }
    return TRUE;
}


OLImage *ol_raw_processor_run(OLRawProcessor *proc,
                              OLImage *gray,
                              OLRawParams *params,
                              GError **err)
{
    OLImage *rgb = NULL;
    OLBayerFilter filter;
    OLBayer *bayer = NULL;
    if (!ol_raw_processor_get_bayer_filter(params, &filter, err))
        goto cleanup;

    bayer = ol_bayer_new(filter, proc->bayer_method);
    if (!ol_raw_processor_apply_wb(proc, gray, params, err))
        goto cleanup;
    rgb = ol_bayer_decode(bayer, gray, err);
    if (!rgb)
        goto cleanup;

 cleanup:
    if (bayer)
        g_object_unref(bayer);
    return rgb;
}
