/*
 * oldlight-identity.c: list of asterisms
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>

#include "oldlight/oldlight-identity.h"

struct _OLIdentity
{
    GObject parent;

    OLQuad **quads;
    gsize nquads;

    gsize width;
    gsize height;
};

G_DEFINE_TYPE(OLIdentity, ol_identity, G_TYPE_OBJECT);


enum {
    PROP_0,
    PROP_WIDTH,
    PROP_HEIGHT,
};


static void ol_identity_get_property(GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
    OLIdentity *identity = OL_IDENTITY(object);

    switch (prop_id) {
    case PROP_WIDTH:
        g_value_set_int(value, identity->width);
        break;

    case PROP_HEIGHT:
        g_value_set_int(value, identity->height);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_identity_set_property(GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
    OLIdentity *identity = OL_IDENTITY(object);

    switch (prop_id) {
    case PROP_WIDTH:
        identity->width = g_value_get_int(value);
        break;

    case PROP_HEIGHT:
        identity->height = g_value_get_int(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_identity_finalize(GObject *object)
{
    OLIdentity *identity = OL_IDENTITY(object);
    gsize i;

    for (i = 0; i < identity->nquads; i++)
        ol_quad_free(identity->quads[i]);
    g_free(identity->quads);

    G_OBJECT_CLASS(ol_identity_parent_class)->finalize(object);
}

static void ol_identity_class_init(OLIdentityClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = ol_identity_finalize;
    object_class->get_property = ol_identity_get_property;
    object_class->set_property = ol_identity_set_property;

    g_object_class_install_property(object_class,
                                    PROP_WIDTH,
                                    g_param_spec_int("width",
                                                     "Image width",
                                                     "Image width",
                                                     0, 65536, 0,
                                                     G_PARAM_READWRITE |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_HEIGHT,
                                    g_param_spec_int("height",
                                                     "Image height",
                                                     "Image height",
                                                     0, 65536, 0,
                                                     G_PARAM_READWRITE |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB));
}

static void ol_identity_init(OLIdentity *identity G_GNUC_UNUSED)
{
}


OLIdentity *ol_identity_new(gsize width,
                            gsize height)
{
    return OL_IDENTITY(g_object_new(OL_TYPE_IDENTITY,
                                   "width", width,
                                   "height", height,
                                    NULL));
}

gsize ol_identity_get_nquads(OLIdentity *identity)
{

    return identity->nquads;
}

OLQuad *ol_identity_get_quad(OLIdentity *identity, gsize i)
{
    if (i >= identity->nquads)
        return NULL;

    return identity->quads[i];
}

OLQuad **ol_identity_get_quads(OLIdentity *identity)
{
    return identity->quads;
}


void ol_identity_add_quad(OLIdentity *identity,
                          OLQuad *quad)
{
    gsize i;

    for (i = 0; i < identity->nquads; i++) {
        if (ol_quad_equal(quad, identity->quads[i])) {
            ol_quad_free(quad);
            return;
        }
    }

    identity->quads = g_renew(OLQuad *, identity->quads, identity->nquads + 1);
    identity->quads[identity->nquads++] = quad;
}


void ol_identity_render(OLIdentity *identity,
                        OLImage *image)
{
    gsize s;

    for (s = 0; s < identity->nquads; s++) {
        ol_quad_render(identity->quads[s], image);
    }
}


static int ol_identity_sort_quad(gconstpointer a,
                                 gconstpointer b,
                                 gpointer opaque)
{
    OLQuad *qa = (OLQuad *)a;
    OLQuad *qb = (OLQuad *)b;
    OLQuad *ref = opaque;

    gdouble d1 = ol_quad_distance(ref, qa);
    gdouble d2 = ol_quad_distance(ref, qb);
    if (d1 < d2)
        return -1;
    if (d1 > d2)
        return 1;
    return 0;
}

OLMapping *ol_identity_match(OLIdentity *identity,
                             OLIdentity *unknown,
                             gdouble fuzz)
{
    OLMapping *mapping = ol_mapping_new(identity->width, identity->height);
    gsize i, j;

    for (j = 0; j < unknown->nquads; j++) {
        GList *matches = NULL;
        for (i = 0; i < identity->nquads; i++) {
            if (ol_quad_matches(unknown->quads[j], identity->quads[i], fuzz)) {
                matches = g_list_append(matches, identity->quads[i]);
            }
        }
        if (matches) {
            matches = g_list_sort_with_data(matches, ol_identity_sort_quad, identity->quads[j]);

            ol_mapping_add_quad_pair(mapping, unknown->quads[j], matches->data);

            g_list_free(matches);
        }
    }

    if (ol_mapping_get_nquad_pairs(mapping) == 0) {
        g_object_unref(mapping);
        return NULL;
    }

    return mapping;
}


gboolean ol_identity_save(OLIdentity *identity,
                          const gchar *filename,
                          GError **error)
{
    GKeyFile *file = g_key_file_new();
    gboolean ret;
    gsize i;

    g_key_file_set_int64(file, "quads", "count", identity->nquads);
#if 0
    g_key_file_set_integer(file, "image", "width", identity->width);
    g_key_file_set_integer(file, "image", "height", identity->height);
#endif

    for (i = 0; i < identity->nquads; i++) {
        OLQuad *quad = identity->quads[i];
        gchar *group = g_strdup_printf("quad%zd", i);

        g_key_file_set_double(file, group, "hash.0.x", quad->hash[0].x);
        g_key_file_set_double(file, group, "hash.0.y", quad->hash[0].y);
        g_key_file_set_double(file, group, "hash.1.x", quad->hash[1].x);
        g_key_file_set_double(file, group, "hash.1.y", quad->hash[1].y);

        g_key_file_set_double(file, group, "star.0.x", quad->stars[0].x);
        g_key_file_set_double(file, group, "star.0.y", quad->stars[0].y);
        g_key_file_set_double(file, group, "star.1.x", quad->stars[1].x);
        g_key_file_set_double(file, group, "star.1.y", quad->stars[1].y);
        g_key_file_set_double(file, group, "star.2.x", quad->stars[2].x);
        g_key_file_set_double(file, group, "star.2.y", quad->stars[2].y);
        g_key_file_set_double(file, group, "star.3.x", quad->stars[3].x);
        g_key_file_set_double(file, group, "star.3.y", quad->stars[3].y);

        g_free(group);
    }

    ret = g_key_file_save_to_file(file, filename, error);
    g_key_file_free(file);
    return ret;
}


gboolean ol_identity_load(OLIdentity *identity,
                          const gchar *filename,
                          GError **error)
{
    GKeyFile *file = g_key_file_new();
    gboolean ret = FALSE;
    GError *localerr = NULL;
    gchar *group = NULL;
    gsize i;

    for (i = 0; i < identity->nquads; i++)
        ol_quad_free(identity->quads[i]);
    g_free(identity->quads);

    if (!g_key_file_load_from_file(file, filename, G_KEY_FILE_NONE, error))
        goto cleanup;

#if 0
    identity->width = g_key_file_get_integer(file, "image", "width", &localerr);
    if (localerr)
        goto cleanup;

    identity->height = g_key_file_get_integer(file, "image", "height", &localerr);
    if (localerr)
        goto cleanup;
#endif

    identity->nquads = g_key_file_get_int64(file, "quads", "count", &localerr);
    if (localerr)
        goto cleanup;

    identity->quads = g_new0(OLQuad *, identity->nquads);

    for (i = 0; i < identity->nquads; i++) {
        group = g_strdup_printf("quad%zd", i);

        identity->quads[i] = g_new0(OLQuad, 1);

        identity->quads[i]->hash[0].x = g_key_file_get_double(file, group, "hash.0.x", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->hash[0].y = g_key_file_get_double(file, group, "hash.0.y", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->hash[1].x = g_key_file_get_double(file, group, "hash.1.x", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->hash[1].y = g_key_file_get_double(file, group, "hash.1.y", &localerr);
        if (localerr)
            goto cleanup;


        identity->quads[i]->stars[0].x = g_key_file_get_double(file, group, "star.0.x", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->stars[0].y = g_key_file_get_double(file, group, "star.0.y", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->stars[1].x = g_key_file_get_double(file, group, "star.1.x", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->stars[1].y = g_key_file_get_double(file, group, "star.1.y", &localerr);
        if (localerr)
            goto cleanup;


        identity->quads[i]->stars[2].x = g_key_file_get_double(file, group, "star.2.x", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->stars[2].y = g_key_file_get_double(file, group, "star.2.y", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->stars[3].x = g_key_file_get_double(file, group, "star.3.x", &localerr);
        if (localerr)
            goto cleanup;

        identity->quads[i]->stars[3].y = g_key_file_get_double(file, group, "star.3.y", &localerr);
        if (localerr)
            goto cleanup;


        g_free(group);
        group = NULL;
    }

    ret = TRUE;

 cleanup:
    if (!ret) {
        for (i = 0; i < identity->nquads; i++)
            ol_quad_free(identity->quads[i]);
        g_free(identity->quads);
    }
    if (localerr)
        g_propagate_error(error, localerr);
    g_free(group);
    g_key_file_free(file);
    return ret;
}
