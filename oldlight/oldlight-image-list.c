/*
 * oldlight-image-list.c: image list
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-list.h"
#include "oldlight/oldlight-image-reader-pnm.h"

#include <dirent.h>

struct _OLImageList
{
    GObject parent;
    GList *images;
    GList *readers;
    guint width;
    guint height;
    OLImageChannels channels;
    OLMatrixFormat format;
};

G_DEFINE_TYPE(OLImageList, ol_image_list, G_TYPE_OBJECT);

#define OL_IMAGE_LIST_ERROR ol_image_list_error_quark()

static GQuark
ol_image_list_error_quark(void)
{
    return g_quark_from_static_string("ol-image-list");
}


enum {
    PROP_0,
    PROP_WIDTH,
    PROP_HEIGHT,
};


static void ol_image_list_get_property(GObject *object,
                                         guint prop_id,
                                         GValue *value,
                                         GParamSpec *pspec)
{
    OLImageList *list = OL_IMAGE_LIST(object);

    switch (prop_id) {
    case PROP_WIDTH:
        g_value_set_uint(value, list->width);
        break;

    case PROP_HEIGHT:
        g_value_set_uint(value, list->height);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_image_list_set_property(GObject *object,
                                         guint prop_id,
                                         const GValue *value,
                                         GParamSpec *pspec)
{
    OLImageList *list = OL_IMAGE_LIST(object);

    switch (prop_id) {
    case PROP_WIDTH:
        list->width = g_value_get_uint(value);
        break;

    case PROP_HEIGHT:
        list->height = g_value_get_uint(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_image_list_finalize(GObject *object)
{
    OLImageList *list = OL_IMAGE_LIST(object);

    g_list_free_full(list->images, g_free);
    g_list_free_full(list->readers, g_object_unref);

    G_OBJECT_CLASS(ol_image_list_parent_class)->finalize(object);
}


static void ol_image_list_class_init(OLImageListClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = ol_image_list_finalize;
    object_class->get_property = ol_image_list_get_property;
    object_class->set_property = ol_image_list_set_property;

    g_object_class_install_property(object_class,
                                    PROP_WIDTH,
                                    g_param_spec_uint("width",
                                                      "Width",
                                                      "Image width",
                                                      0, 65565, 0,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_CONSTRUCT_ONLY |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_HEIGHT,
                                    g_param_spec_uint("height",
                                                      "Height",
                                                      "Image height",
                                                      0, 65565, 0,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_CONSTRUCT_ONLY |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));
}


static void ol_image_list_init(OLImageList *list G_GNUC_UNUSED)
{
}


OLImageList *ol_image_list_new(void)
{
    return OL_IMAGE_LIST(g_object_new(OL_TYPE_IMAGE_LIST,
                                      NULL));
}


gboolean ol_image_list_add_file(OLImageList *list,
                                const gchar *file,
                                GError **err G_GNUC_UNUSED)
{
    list->images = g_list_append(list->images, g_strdup(file));

    return TRUE;
}


gboolean ol_image_list_add_dir(OLImageList *list,
                               const gchar *dir,
                               GError **err G_GNUC_UNUSED)
{
    DIR *dh = opendir(dir);
    struct dirent *de;

    while ((de = readdir(dh))) {
        list->images = g_list_append(list->images, g_strdup_printf("%s/%s",dir, de->d_name));
    }

    closedir(dh);

    return TRUE;
}


GList *ol_image_list_get_files(OLImageList *list)
{
    return list->images;
}


gboolean ol_image_list_open(OLImageList *list,
                            GError **err)
{
    GList *images = list->images;
    GList *readers = NULL, *tmp;

    if (list->readers) {
        return TRUE;
    }

    if (!images) {
        g_set_error(err, OL_IMAGE_LIST_ERROR, 0,
                    "No images available to open");
        return FALSE;
    }

    while (images) {
        OLImageReader *reader = OL_IMAGE_READER(ol_image_reader_pnm_new(images->data));

        readers = g_list_append(readers, reader);
        if (!ol_image_io_open(OL_IMAGE_IO(reader), err))
            goto error;

        images = images->next;
    }

    list->width = ol_image_io_get_width(readers->data);
    list->height = ol_image_io_get_height(readers->data);
    list->format = ol_image_io_get_format(readers->data);
    list->channels = ol_image_io_get_channels(readers->data);

    tmp = readers->next;
    while (tmp) {
        OLImageReader *reader = OL_IMAGE_READER(readers->data);

        if (list->width != ol_image_io_get_width(OL_IMAGE_IO(reader)) ||
            list->height != ol_image_io_get_height(OL_IMAGE_IO(reader))) {
            g_set_error(err, OL_IMAGE_LIST_ERROR, 0,
                        "Image '%s' has different size (%dx%d) from initial image '%s' (%dx%d)",
                        ol_image_io_get_filename(OL_IMAGE_IO(reader)),
                        ol_image_io_get_width(OL_IMAGE_IO(reader)),
                        ol_image_io_get_height(OL_IMAGE_IO(reader)),
                        ol_image_io_get_filename(OL_IMAGE_IO(readers->data)),
                        list->width, list->height);
            goto error;
        }

        if (list->channels != ol_image_io_get_channels(OL_IMAGE_IO(reader))) {
            g_set_error(err, OL_IMAGE_LIST_ERROR, 0,
                        "Image '%s' has different channels (%d) from initial image '%s' (%d)",
                        ol_image_io_get_filename(OL_IMAGE_IO(reader)),
                        ol_image_io_get_channels(OL_IMAGE_IO(reader)),
                        ol_image_io_get_filename(OL_IMAGE_IO(readers->data)),
                        list->channels);
            goto error;
        }

        if (list->format != ol_image_io_get_format(OL_IMAGE_IO(reader))) {
            g_set_error(err, OL_IMAGE_LIST_ERROR, 0,
                        "Image '%s' has different format (%d) from initial image '%s' (%d)",
                        ol_image_io_get_filename(OL_IMAGE_IO(reader)),
                        ol_image_io_get_format(OL_IMAGE_IO(reader)),
                        ol_image_io_get_filename(OL_IMAGE_IO(readers->data)),
                        list->format);
            goto error;
        }

        tmp = tmp->next;
    }

    list->readers = readers;

    return TRUE;

 error:
    g_list_free_full(readers, g_object_unref);
    return FALSE;
}


guint ol_image_list_get_width(OLImageList *list)
{
    return list->width;
}


guint ol_image_list_get_height(OLImageList *list)
{
    return list->height;
}


OLImageChannels ol_image_list_get_channels(OLImageList *list)
{
    return list->channels;
}


OLMatrixFormat ol_image_list_get_format(OLImageList *list)
{
    return list->format;
}


gsize ol_image_list_get_count(OLImageList *list)
{
    return g_list_length(list->readers);
}


OLImage **ol_image_list_load_lines(OLImageList *list, guint line, guint count, GError **err)
{
    GList *tmp = list->readers;
    OLImage **ret = g_new0(OLImage *, g_list_length(list->readers) + 1);
    gsize i = 0;

    while (tmp) {
        OLImageReader *reader = tmp->data;
        OLImage *image = ol_image_reader_load_payload(reader,
                                                      0, line,
                                                      list->width, count,
                                                      err);
        if (!image)
            goto error;

        ret[i++] = image;
        tmp = tmp->next;
    }

    return ret;

 error:
    for (i = 0; ret[i] != NULL; i++) {
        g_object_unref(ret[i]);
    }
    g_free(ret);
    return NULL;
}
