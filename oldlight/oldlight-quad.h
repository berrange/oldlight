/*
 * oldlight-quad.h: asterism of four stars
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_QUAD_H__
#define OL_QUAD_H__

#include "oldlight/oldlight-location.h"
#include "oldlight/oldlight-image.h"

G_BEGIN_DECLS

#define OL_TYPE_QUAD            (ol_quad_get_type ())

typedef struct _OLQuad OLQuad;

struct _OLQuad
{
    OLLocation hash[2];
    OLLocation stars[4];
};

GType ol_quad_get_type(void);

OLQuad *ol_quad_new(OLLocation *a,
                    OLLocation *b,
                    OLLocation *c,
                    OLLocation *d);

OLQuad *ol_quad_copy(OLQuad *quad);

void ol_quad_free(OLQuad *quad);

gboolean ol_quad_equal(OLQuad *quad,
                       OLQuad *other);

gboolean ol_quad_matches(OLQuad *quad,
                         OLQuad *unknown,
                         gdouble fuzz);
gdouble ol_quad_distance(OLQuad *quad,
                         OLQuad *unknown);

void ol_quad_render(OLQuad *quad,
                    OLImage *image);

G_END_DECLS

#endif /* OL_QUAD_H__ */
