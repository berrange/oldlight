/*
 * oldlight-image-reader.h: image reader
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IMAGE_READER_H__
#define OL_IMAGE_READER_H__

#include "oldlight/oldlight-image-io.h"
#include "oldlight/oldlight-image.h"

G_BEGIN_DECLS

#define OL_TYPE_IMAGE_READER ol_image_reader_get_type()

G_DECLARE_DERIVABLE_TYPE(OLImageReader, ol_image_reader, OL, IMAGE_READER, OLImageIO);

struct _OLImageReaderClass {
    OLImageIOClass parent_class;

    OLImage *(*load_payload)(OLImageReader *reader,
                             guint x, guint y,
                             guint width, guint height,
                             GError **err);
};


OLImage *ol_image_reader_load(OLImageReader *reader,
                              GError **err);

OLImage *ol_image_reader_load_payload(OLImageReader *reader,
                                      guint x, guint y,
                                      guint width, guint height,
                                      GError **err);

G_END_DECLS

#endif /* OL_IMAGE_READER_H___ */
