/*
 * oldlight-stacker.h: image stacker
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_STACKER_H__
#define OL_STACKER_H__

#include "oldlight/oldlight-image-list.h"
#include "oldlight/oldlight-enums.h"

G_BEGIN_DECLS

#define OL_TYPE_STACKER ol_stacker_get_type()

G_DECLARE_FINAL_TYPE(OLStacker, ol_stacker, OL, STACKER, GObject);

typedef enum {
    OL_STACKER_METHOD_MEAN,
    OL_STACKER_METHOD_MEDIAN,
    OL_STACKER_METHOD_MAXIMUM,
} OLStackerMethod;

OLStacker *ol_stacker_new(OLStackerMethod method);

gboolean ol_stacker_run(OLStacker *stacker,
                        OLImageList *input_files,
                        const char *output_file,
                        GError **err);

gboolean ol_stacker_method_from_string(const char *method,
                                       int *value,
                                       GError **err);

G_END_DECLS

#endif /* OL_STACKER_H___ */
