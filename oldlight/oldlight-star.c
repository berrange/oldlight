/*
 * oldlight-star.c: star properties
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>

#include "oldlight/oldlight-star.h"

GType ol_star_get_type(void)
{
    static GType star_type = 0;

    if (G_UNLIKELY(star_type == 0)) {
        star_type = g_boxed_type_register_static
            ("OLStar",
             (GBoxedCopyFunc)ol_star_copy,
             (GBoxedFreeFunc)ol_star_free);
    }

    return star_type;
}

OLStar *ol_star_new(gdouble x,
                    gdouble y,
                    double roundness1,
                    double roundness2,
                    double sharpness,
                    double flux,
                    double magnitude)
{
    OLStar *star = g_new0(OLStar, 1);

    star->location.x = x;
    star->location.y = y;
    star->roundness1 = roundness1;
    star->roundness2 = roundness2;
    star->sharpness = sharpness;
    star->flux = flux;
    star->magnitude = magnitude;

    return star;
}

OLStar *ol_star_copy(OLStar *star)
{
    OLStar *newstar = g_new0(OLStar, 1);

    memcpy(newstar, star, sizeof(*star));

    return newstar;
}


OLStar *ol_star_copy_transform(OLStar *star,
                               OLTransform *transform)
{
    OLStar *newstar = ol_star_copy(star);

    ol_transform_apply(transform, &newstar->location);

    return newstar;
}


void ol_star_free(OLStar *star)
{
    g_free(star);
}

gboolean ol_star_equal(OLStar *star, OLStar *other)
{
    return star->location.x == other->location.x &&
        star->location.y == other->location.y;
}

gdouble ol_star_distance(OLStar *star, OLStar *other)
{
    return ol_location_distance(&star->location, &other->location);
}
