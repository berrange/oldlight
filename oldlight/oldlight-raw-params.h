/*
 * oldlight-raw-params.h: raw file parameters
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_RAW_PARAMS_H__
#define OL_RAW_PARAMS_H__

#include "oldlight/oldlight-matrix.h"

G_BEGIN_DECLS

#define OL_TYPE_RAW_PARAMS            (ol_raw_params_get_type ())

typedef struct _OLRawParams OLRawParams;

struct _OLRawParams
{
    guint32 filters;
    guchar colors[4];
    OLMatrix *img_wb; /* float32 4x1 */
    OLMatrix *cam_wb; /* float32 4x1 */
    gfloat black;
    gfloat img_max;
    gfloat cam_max;
    OLMatrix *cam_2_srgb; /* float32 4x3 */
    OLMatrix *cam_2_xyz; /* float32 4x3 */
};


GType ol_raw_params_get_type(void);

OLRawParams *ol_raw_params_new(void);
OLRawParams *ol_raw_params_copy(OLRawParams *params);
void ol_raw_params_free(OLRawParams *params);

G_END_DECLS

#endif /* OL_RAW_PARAMS_H__ */
