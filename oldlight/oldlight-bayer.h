/*
 * oldlight-bayer.h: bayer processing
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_BAYER_H__
#define OL_BAYER_H__

#include "oldlight/oldlight-enums.h"
#include "oldlight/oldlight-image.h"

G_BEGIN_DECLS

#define OL_TYPE_BAYER ol_bayer_get_type()

G_DECLARE_DERIVABLE_TYPE(OLBayer, ol_bayer, OL, BAYER, GObject);


typedef enum {
    OL_BAYER_METHOD_NEAREST_NEIGHBOUR,
    OL_BAYER_METHOD_SIMPLE,
    OL_BAYER_METHOD_BILINEAR,
    OL_BAYER_METHOD_HQLINEAR,
    OL_BAYER_METHOD_DOWNSAMPLE,
    OL_BAYER_METHOD_EDGE_SENSE,
    OL_BAYER_METHOD_VNG,
    OL_BAYER_METHOD_AHD,
} OLBayerMethod;


typedef enum {
    OL_BAYER_FILTER_RGGB,
    OL_BAYER_FILTER_GBRG,
    OL_BAYER_FILTER_GRBG,
    OL_BAYER_FILTER_BGGR,
} OLBayerFilter;

struct _OLBayerClass {
    GObjectClass parent_class;

    void (*decode8)(const guint8 *restrict bayer,
                    guint8 *restrict rgb,
                    gsize sx,
                    gsize sy,
                    OLBayerFilter tile);

    void (*decode16)(const guint16 *restrict bayer,
                     guint16 *restrict rgb,
                     gsize sx,
                     gsize sy,
                     OLBayerFilter tile,
                     int bits);
};


OLBayerFilter ol_bayer_get_filter(OLBayer *bayer);

OLImage *ol_bayer_decode(OLBayer *bayer,
                         OLImage *src,
                         GError **err);

OLBayer *ol_bayer_new(OLBayerFilter filter,
                      OLBayerMethod method);

G_END_DECLS

#endif /* OL_BAYER_H__ */
