/*
 * oldlight-location.c: position in image
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include <math.h>
#include <string.h>

#include "oldlight/oldlight-location.h"

GType ol_location_get_type(void)
{
    static GType location_type = 0;

    if (G_UNLIKELY(location_type == 0)) {
        location_type = g_boxed_type_register_static
            ("OLLocation",
             (GBoxedCopyFunc)ol_location_copy,
             (GBoxedFreeFunc)ol_location_free);
    }

    return location_type;
}

OLLocation *ol_location_new(double x,
                            double y)
{
    OLLocation *location = g_new0(OLLocation, 1);

    location->x = x;
    location->y = y;

    return location;
}

OLLocation *ol_location_copy(OLLocation *location)
{
    OLLocation *newlocation = g_new0(OLLocation, 1);

    memcpy(newlocation, location, sizeof(*location));

    return newlocation;
}

void ol_location_free(OLLocation *location)
{
    g_free(location);
}

gboolean ol_location_equal(OLLocation *location,
                           OLLocation *other)
{
    return location->x == other->x && location->y == other->y;
}

gdouble ol_location_distance(OLLocation *location,
                             OLLocation *other)
{
    gdouble dx = location->x - other->x;
    gdouble dy = location->y - other->y;

    return sqrt((dx * dx) + (dy * dy));
}
