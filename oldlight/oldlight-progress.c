/*
 *  oldlight-progress.c: monitor operation progress
 *
 *  Copyright (C) 2018 Daniel P. Berrange
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight-progress.h"

void ol_progress_start(OLProgress *prog, gfloat target, const gchar *msg)
{
    g_return_if_fail(OL_IS_PROGRESS(prog));

    OL_PROGRESS_GET_IFACE(prog)->start(prog, target, msg);
}

void ol_progress_update(OLProgress *prog, gfloat current)
{
    g_return_if_fail(OL_IS_PROGRESS(prog));

    OL_PROGRESS_GET_IFACE(prog)->update(prog, current);
}

void ol_progress_stop(OLProgress *prog)
{
    g_return_if_fail(OL_IS_PROGRESS(prog));

    OL_PROGRESS_GET_IFACE(prog)->stop(prog);
}

GType
ol_progress_get_type(void)
{
    static GType progress_type = 0;

    if (!progress_type) {
        progress_type =
            g_type_register_static_simple(G_TYPE_INTERFACE, "OLProgress",
                                          sizeof(OLProgressInterface),
                                          NULL, 0, NULL, 0);

        g_type_interface_add_prerequisite(progress_type, G_TYPE_OBJECT);
    }

    return progress_type;
}
