/*
 * oldlight-image-writer-pnm.h: Pnm image writer
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IMAGE_WRITER_PNM_H__
#define OL_IMAGE_WRITER_PNM_H__

#include "oldlight/oldlight-image-writer.h"

G_BEGIN_DECLS

#define OL_TYPE_IMAGE_WRITER_PNM ol_image_writer_pnm_get_type()

G_DECLARE_FINAL_TYPE(OLImageWriterPnm, ol_image_writer_pnm, OL, IMAGE_WRITER_PNM, OLImageWriter);

OLImageWriterPnm *ol_image_writer_pnm_new(const gchar *filename,
                                          OLMatrixFormat format,
                                          OLImageChannels channels,
                                          guint width,
                                          guint height);

G_END_DECLS

#endif /* OL_IMAGE_WRITER_PNM_H___ */
