/*
 * oldlight-raw-processor.h: raw file processor
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_RAW_PROCESSOR_H__
#define OL_RAW_PROCESSOR_H__

#include "oldlight/oldlight-image.h"
#include "oldlight/oldlight-bayer.h"
#include "oldlight/oldlight-raw-params.h"

G_BEGIN_DECLS

#define OL_TYPE_RAW_PROCESSOR ol_raw_processor_get_type()

G_DECLARE_FINAL_TYPE(OLRawProcessor, ol_raw_processor, OL, RAW_PROCESSOR, GObject);

typedef enum
{
    OL_RAW_PROCESSOR_WB_NONE,
    OL_RAW_PROCESSOR_WB_IMAGE,
    OL_RAW_PROCESSOR_WB_CAMERA,
    OL_RAW_PROCESSOR_WB_CUSTOM,
} OLRawProcessorWB;

OLRawProcessor *ol_raw_processor_new(OLBayerMethod bayer_method,
                                     OLRawProcessorWB wb,
                                     OLMatrix *custom_wb,
                                     gboolean stretch);

OLImage *ol_raw_processor_run(OLRawProcessor *raw,
                              OLImage *gray,
                              OLRawParams *params,
                              GError **err);

G_END_DECLS

#endif /* OL_RAW_PROCESSOR_H___ */
