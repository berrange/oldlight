/*
 * oldlight-extractor.c: detection of stars
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Many of the methods in this file are a C translation of the
 * corresponding Python code in the 'photutils' package:
 *
 * Copyright (c) 2011-14, photutils developers
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 * * Neither the name of the Astropy Team nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "oldlight/oldlight-extractor.h"
#include "oldlight/oldlight-matrix.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

struct _OLExtractor
{
    GObject parent;
};

G_DEFINE_TYPE(OLExtractor, ol_extractor, G_TYPE_OBJECT);


static void ol_extractor_finalize(GObject *object)
{
    G_OBJECT_CLASS(ol_extractor_parent_class)->finalize(object);
}

static void ol_extractor_class_init(OLExtractorClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = ol_extractor_finalize;
}

static void ol_extractor_init(OLExtractor *extractor G_GNUC_UNUSED)
{
}


OLExtractor *ol_extractor_new(void)
{
    return OL_EXTRACTOR(g_object_new(OL_TYPE_EXTRACTOR,
                                     NULL));
}


typedef struct OLExtractorKernel OLExtractorKernel;
struct OLExtractorKernel
{
    gdouble fwhm;
    gdouble sigma_radius;
    gdouble ratio;
    gdouble theta;

    gdouble xsigma;
    gdouble ysigma;

    OLMatrix *gkern;
    OLMatrix *kern;
    OLMatrix *mask;

    gsize npts;
    gdouble relerr;
};

double gaussian_sigma_to_fwhm = 2.0 * sqrt(2.0 * log(2.0));
double gaussian_fwhm_to_sigma = 1.0 / (2.0 * sqrt(2.0 * log(2.0)));


static inline double square(double val)
{
    return val * val;
}


typedef struct OLExtractorMeshGrid OLExtractorMeshGrid;

struct OLExtractorMeshGrid
{
    OLMatrix *xx;
    OLMatrix *yy;
};

static OLExtractorMeshGrid *mgrid(int width, int height)
{
    OLExtractorMeshGrid *grid = g_new0(OLExtractorMeshGrid, 1);
    gsize x, y;

    grid->xx = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, width, height);
    grid->yy = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, width, height);

    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            grid->yy->data.f64[(y * width) + x] = y;
        }
    }

    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            grid->xx->data.f64[(y * width) + x] = x;
        }
    }

    return grid;
}


static void ol_extractor_mesh_grid_free(OLExtractorMeshGrid *grid)
{
    if (!grid)
        return;
    ol_matrix_free(grid->xx);
    ol_matrix_free(grid->yy);
    g_free(grid);
}


static void ol_extractor_kernel_build(OLExtractorKernel *kernel)
{
    OLExtractorMeshGrid *grid;
    gsize i;
    OLMatrix *circrad;
    OLMatrix *ellrad;
    gdouble theta_radians = kernel->theta * M_PI / 180.0;
    kernel->xsigma = kernel->fwhm * gaussian_fwhm_to_sigma;
    kernel->ysigma = kernel->xsigma * kernel->ratio;
    gdouble xsigma2 = square(kernel->xsigma);
    gdouble ysigma2 = square(kernel->ysigma);
    gdouble cost = cos(theta_radians);
    gdouble sint = sin(theta_radians);
    gdouble a = (square(cost) / (2.0 * xsigma2)) + (square(sint) / (2.0 * ysigma2));
    gdouble b = 0.5 * cost * sint * (1.0 / xsigma2 - 1.0 / ysigma2);
    gdouble c = (square(sint) / (2.0 * xsigma2)) + (square(cost) / (2.0 * ysigma2));
    gdouble f = square(kernel->sigma_radius) / 2.0;
    gdouble denom = a * c - square(b);
    gdouble k2sum = 0;
    gdouble ksum2 = 0;
    gdouble ksum = 0;

    gdouble xrad = floor(MAX(2, sqrt(c * f / denom)));
    gdouble yrad = floor(MAX(2, sqrt(a * f / denom)));
    gdouble nx = 2 * xrad + 1;
    gdouble ny = 2 * yrad + 1;

    grid = mgrid(nx, ny);

    circrad = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nx, ny);
    ellrad = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nx, ny);
    kernel->mask = ol_matrix_new(OL_MATRIX_FORMAT_UINT8, nx, ny);
    kernel->gkern = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nx, ny);
    kernel->kern = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nx, ny);

    for (i = 0 ; i < grid->xx->len; i++) {
        circrad->data.f64[i] =
            sqrt(square((grid->xx->data.f64[i] - xrad)) +
                 square((grid->yy->data.f64[i] - yrad)));

        ellrad->data.f64[i] =
            (a * square(grid->xx->data.f64[i] - xrad)) +
            (2.0 * b * (grid->xx->data.f64[i] - xrad) * (grid->yy->data.f64[i] - yrad)) +
            (c * square(grid->yy->data.f64[i] - yrad));

        kernel->mask->data.u8[i] =
            ((ellrad->data.f64[i] < f) ||
             (circrad->data.f64[i] < 2.0)) ? TRUE : FALSE;

        kernel->npts += kernel->mask->data.u8[i] ? 1 : 0;

        kernel->gkern->data.f64[i] = exp(-ellrad->data.f64[i]);
        if (kernel->mask->data.u8[i])
            kernel->kern->data.f64[i] = kernel->gkern->data.f64[i];
    }

    for (i = 0 ; i < grid->xx->len; i++) {
        k2sum += square(kernel->kern->data.f64[i]);
        ksum += kernel->kern->data.f64[i];
    }
    ksum2 = square(ksum);

    denom = k2sum - (ksum2 / kernel->npts);

    kernel->relerr = 1.0 / sqrt(denom);

    for (i = 0 ; i < grid->xx->len; i++) {
        kernel->kern->data.f64[i] =
            (((kernel->kern->data.f64[i] - (ksum / kernel->npts)) / denom) *
             kernel->mask->data.u8[i]);
    }

    ol_extractor_mesh_grid_free(grid);
    ol_matrix_free(ellrad);
    ol_matrix_free(circrad);
}



/**
 * @fwhm: the full-width half-maximum of the 2d circular Gaussian kernel in pixels
 * @ratio: ratio of the minor to major axis standard deviations of the Gaussian kernel. Must be positive and less than or equal to 1.0. A value of 1.0 produces a circular kernel
 * @theta: the position angle in degrees of the major axis measured counter-clockwise from the positive x axis
 * @sigma_radius: the truncation radius of the gaussian kernel in units of sigma/. Sigma = fwhm / 2 * sqrt(2 * log(2))
 */
static OLExtractorKernel *ol_extractor_kernel_new(double fwhm, double ratio, double theta, double sigma_radius)
{
    OLExtractorKernel *kernel = g_new0(OLExtractorKernel, 1);

    g_assert(fwhm > 0);
    g_assert(ratio > 0 && ratio <= 1.0);
    g_assert(sigma_radius > 0);
    g_assert(theta >= 0 && theta < 360);

    kernel->fwhm = fwhm;
    kernel->sigma_radius = sigma_radius;
    kernel->ratio = ratio;
    kernel->theta = theta;

    ol_extractor_kernel_build(kernel);

    return kernel;
}


static void ol_extractor_kernel_free(OLExtractorKernel *kernel)
{
    if (!kernel)
        return;

    ol_matrix_free(kernel->mask);
    ol_matrix_free(kernel->kern);
    ol_matrix_free(kernel->gkern);

    g_free(kernel);
}


static int ol_extractor_label(double *data,
                              int data_width,
                              int data_height,
                              double threshold,
                              int *labels)
{
    int count = 0;
    int nextlabel = 1;
    int *equiv = NULL;

    gsize x, y;
    gsize l;

    for (y = 0; y < data_height; y++) {
        for (x = 0; x < data_width; x++) {
            if (data[(y * data_width) + x] <= threshold)
                continue;

            int lastlabel = 0;
            int l0 = 0, l1 = 0, l2 = 0;
            if (x > 0 && y > 0) {
                l0 = labels[((y - 1) * data_width) + (x - 1)];
            }
            if (x > 0 && y == 0) {
                l1 = labels[(y * data_width) + (x - 1)];
            }
            if (x == 0 && y > 0) {
                l2 = labels[((y - 1) * data_width) + x];
            }
            lastlabel = l0;
            if (l1 && (!lastlabel || l1 < lastlabel))
                lastlabel = l1;
            if (l2 && (!lastlabel || l2 < lastlabel))
                lastlabel = l2;

            if (lastlabel) {
                if (l0 && lastlabel != l0) {
                    if (!equiv[l0 - 1] || lastlabel < equiv[l0 - 1]) {
                        equiv[l0 - 1] = lastlabel;
                    }
                }
                if (l1 && lastlabel != l1) {
                    if (!equiv[l1 - 1] || lastlabel < equiv[l1 - 1]) {
                        equiv[l1 - 1] = lastlabel;
                    }
                }
                if (l2 && lastlabel != l2) {
                    if (!equiv[l2 - 1] || lastlabel < equiv[l2 - 1]) {
                        equiv[l2 - 1] = lastlabel;
                    }
                }
            } else {
                equiv = g_renew(int, equiv, nextlabel);
                equiv[nextlabel - 1] = 0;
                lastlabel = nextlabel++;
            }

            labels[(y * data_width) + x] = lastlabel;
        }
    }

    for (l = 1 ; l < nextlabel; l++) {
        if (equiv[l - 1]) {
            equiv[l - 1] = equiv[equiv[l - 1]];
        } else {
            equiv[l - 1] = ++count;
        }
    }

    for (y = 0; y < data_height; y++) {
        for (x = 0; x < data_width; x++) {
            int label = labels[(y * data_width) + x];
            if (!label)
                continue;

            labels[(y * data_width) + x] = equiv[label - 1];
        }
    }

    return count;
}


typedef struct OLExtractorObject OLExtractorObject;
struct OLExtractorObject
{
    gsize x;
    gsize y;
    OLMatrix *data;
    OLMatrix *convdata;
};

static void ol_extractor_find_peaks(OLMatrix *data,
                                    double threshold,
                                    OLMatrix *mask,
                                    OLExtractorObject **objs,
                                    gsize *nobjs)
{
    OLMatrix *max = ol_matrix_new_maximum_filter(data, mask);

    gssize x, y;
    gdouble *tmp = data->data.f64;
    gdouble *tmp_max = max->data.f64;

    for (y = 0; y < data->height; y++) {
        for (x = 0; x < data->width; x++) {
            if ((*tmp == *tmp_max) &&
                (*tmp > threshold)) {

                *objs = g_renew(OLExtractorObject, *objs, *nobjs + 1);

                (*objs)[*nobjs].x = x;
                (*objs)[*nobjs].y = y;
                (*nobjs)++;
            }

            tmp++;
            tmp_max++;
        }
    }

    ol_matrix_free(max);
}

static void ol_extractor_find_objs(OLMatrix *image,
                                   double threshold,
                                   OLExtractorKernel *kernel,
                                   double min_separation G_GNUC_UNUSED,
                                   gboolean exclude_border,
                                   gboolean local_peaks G_GNUC_UNUSED,
                                   OLExtractorObject **objs,
                                   gsize *nobjs)
{
    int x_kernradius = (int)floor(kernel->kern->width / 2);
    int y_kernradius = (int)floor(kernel->kern->height / 2);
    OLMatrix *image_padded = NULL;
    gsize width = image->width;
    gsize height = image->height;
    OLMatrix *output = NULL;
    gsize n;
    gsize nfilterobjs = 0;

    if (!exclude_border) {
        image_padded = ol_matrix_copy_with_border(image,
                                                  x_kernradius,
                                                  x_kernradius,
                                                  y_kernradius,
                                                  y_kernradius);
        width = image_padded->width;
        height = image_padded->height;
    } else {
        width = image->width;
        height = image->height;
    }

    output = ol_matrix_new_convolve(image_padded ? image_padded : image, kernel->kern);

    if (!exclude_border) {
        ol_matrix_clear_border(image_padded,
                               x_kernradius,
                               x_kernradius,
                               y_kernradius,
                               y_kernradius);
    }

    if (0) {
        int *labels = g_new0(int, width * height);

        ol_extractor_label(output->data.f64, width, height, threshold, labels);

        g_free(labels);
    }

    ol_extractor_find_peaks(output, threshold, kernel->mask, objs, nobjs);


    for (n = 0; n < *nobjs; n++) {
        OLExtractorObject obj = (*objs)[n];
        int x0 = obj.x - x_kernradius;
        int x1 = obj.x + x_kernradius + 1;
        int y0 = obj.y - y_kernradius;
        int y1 = obj.y + y_kernradius + 1;
        int w = x1 - x0;
        int h = y1 - y0;
        OLMatrix *convdata;
        OLMatrix *cutdata;

        if (x0 < 0 || x1 > width ||
            y0 < 0 || y1 > height)
            continue;

        cutdata = ol_matrix_copy_region(image_padded ? image_padded : image,
                                        x0, y0, w, h);
        convdata = ol_matrix_copy_region(output, x0, y0, w, h);

        if (!exclude_border) {
            obj.x -= x_kernradius;
            obj.y -= y_kernradius;
        }

        (*objs)[nfilterobjs].x = obj.x;
        (*objs)[nfilterobjs].y = obj.y;
        (*objs)[nfilterobjs].data = cutdata;
        (*objs)[nfilterobjs].convdata = convdata;

        nfilterobjs++;
    }

    *objs = g_renew(OLExtractorObject, *objs, nfilterobjs);
    *nobjs = nfilterobjs;
    g_free(output);
    if (image_padded)
        ol_matrix_free(image_padded);
}


static void ol_extractor_centroid_daofind_axis(OLMatrix *data,
                                               OLExtractorKernel *kernel,
                                               gsize axis,
                                               gdouble *pos,
                                               gdouble *size)
{
    gsize nxk = kernel->kern->width;
    gsize nyk = kernel->kern->height;
    gsize xkrad = (kernel->kern->width / 2);
    gsize ykrad = (kernel->kern->height / 2);
    OLExtractorMeshGrid *grid = mgrid(nxk, nyk);
    OLMatrix *xwt = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nxk, nyk);
    OLMatrix *ywt = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nxk, nyk);
    gsize i;

    *pos = *size = 0;

    for (i = 0; i < xwt->len; i++) {
        xwt->data.f64[i] = xkrad - fabs(grid->xx->data.f64[i] - xkrad) + 1;
    }
    for (i = 0; i < ywt->len; i++) {
        ywt->data.f64[i] = ykrad - fabs(grid->yy->data.f64[i] - ykrad) + 1;
    }

    OLMatrix *wt;
    OLMatrix *wts;
    gsize ksize;
    gdouble kernel_sigma;
    gsize krad;
    OLMatrix *sumdx_vec;

    if (axis == 0) {
        wt = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nxk, 1);
        for (i = 0; i < nxk; i++) {
            wt->data.f64[i] = xwt->data.f64[i];
        }
        wts = ywt;
        ksize = nxk;
        kernel_sigma = kernel->xsigma;
        krad = ksize / 2;
        sumdx_vec = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, ksize, 1);
        for (i = 0; i < ksize; i++) {
            sumdx_vec->data.f64[i] = (gdouble)krad - i;
        }
    } else if (axis == 1) {
        wt = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, nyk, 1);
        for (i = 0; i < nyk; i++) {
            wt->data.f64[i] = ywt->data.f64[i * ywt->width];
        }
        wts = xwt;
        ksize = nyk;
        kernel_sigma = kernel->ysigma;
        krad = ksize / 2;
        sumdx_vec = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, ksize, 1);
        for (i = 0; i < ksize; i++) {
            sumdx_vec->data.f64[i] = (gdouble)i - krad;
        }
    } else {
        g_assert_not_reached();
    }
    gdouble n = ol_matrix_sum(wt);
    (void)ksize;
    (void)kernel_sigma;
    (void)krad;
    (void)data;

    OLMatrix *s = ol_matrix_new_multiply(kernel->gkern, wts);
    OLMatrix *sg = axis == 0 ? ol_matrix_sum_xaxis(s) : ol_matrix_sum_yaxis(s);
    OLMatrix *sg2 = ol_matrix_new_multiply(sg, sg);
    OLMatrix *g = ol_matrix_new_multiply(wt, sg);
    OLMatrix *g2 = ol_matrix_new_multiply(wt, sg2);

    gdouble sumg = ol_matrix_sum(g);
    gdouble sumg2 = ol_matrix_sum(g2);

    OLMatrix *vec = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, ksize, 1);

    for (i = 0; i < ksize; i++) {
        vec->data.f64[i] = (gdouble)krad - i;
    }

    OLMatrix *dgdx = ol_matrix_new_multiply(sg, vec);
    OLMatrix *dgdx2 = ol_matrix_new_multiply(dgdx, dgdx);
    OLMatrix *msdgdx = ol_matrix_new_multiply(wt, dgdx);
    gdouble sdgdx = ol_matrix_sum(msdgdx);
    OLMatrix *msdgdx2 = ol_matrix_new_multiply(wt, dgdx2);
    gdouble sdgdx2 = ol_matrix_sum(msdgdx2);
    OLMatrix *msgdgdx = ol_matrix_new_multiply(msdgdx, sg);
    gdouble sgdgdx = ol_matrix_sum(msgdgdx);
    OLMatrix *msd = ol_matrix_new_multiply(data, wts);
    OLMatrix *sd = axis == 0 ? ol_matrix_sum_xaxis(msd) : ol_matrix_sum_yaxis(msd);
    OLMatrix *msumd = ol_matrix_new_multiply(wt, sd);
    gdouble sumd = ol_matrix_sum(msumd);
    OLMatrix *msumgd = ol_matrix_new_multiply(msumd, sg);
    gdouble sumgd = ol_matrix_sum(msumgd);
    OLMatrix *msddgdx = ol_matrix_new_multiply(msumd, dgdx);
    gdouble sddgdx = ol_matrix_sum(msddgdx);
    OLMatrix *msumdx = ol_matrix_new_multiply(msumd, sumdx_vec);
    gdouble sumdx = ol_matrix_sum(msumdx);

    // linear least-squares fit (data = sky + hx*gkernel) to find amplitudes
    gdouble denom = (n * sumg2) - (sumg * sumg);
    gdouble hx = (n * sumgd - sumg * sumd) / denom;
    // sky = (sumg2*sumd - sumg*sumgd) / denom
    gdouble dx = (sgdgdx - (sddgdx - sdgdx * sumd)) / (hx * sdgdx2 / (kernel_sigma * kernel_sigma));

    ol_matrix_free(msumdx);
    ol_matrix_free(msddgdx);
    ol_matrix_free(msumgd);
    ol_matrix_free(msumd);
    ol_matrix_free(sd);
    ol_matrix_free(msd);
    ol_matrix_free(msgdgdx);
    ol_matrix_free(msdgdx2);
    ol_matrix_free(msdgdx);
    ol_matrix_free(dgdx2);
    ol_matrix_free(dgdx);
    ol_matrix_free(vec);
    ol_matrix_free(g2);
    ol_matrix_free(g);
    ol_matrix_free(sg2);
    ol_matrix_free(sg);
    ol_matrix_free(s);
    ol_matrix_free(wt);
    ol_matrix_free(sumdx_vec);
    ol_matrix_free(ywt);
    ol_matrix_free(xwt);
    ol_extractor_mesh_grid_free(grid);

    gdouble hsize = (ksize / 2.0);
    if (fabs(dx) > hsize) {
        dx = 0;
        if (sumd == 0) {
            dx = 0.0;
        } else {
            dx = sumdx / sumd;
        }
        if (fabs(dx) > hsize) {
            dx = 0.0;
        }
    }

    *pos = dx;
    *size = hx;
}


static void ol_extractor_centroid_daofind(OLMatrix *data,
                                          OLExtractorKernel *kernel,
                                          gdouble *x,
                                          gdouble *y,
                                          gdouble *roundness)
{
    gdouble hx, hy;
    OLMatrix *ddata = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT64, data->width, data->height);
    gsize i;
    g_assert(data->format == OL_MATRIX_FORMAT_INT32);
    for (i = 0; i < data->len; i++) {
        ddata->data.f64[i] = data->data.i32[i];
    }
    ol_extractor_centroid_daofind_axis(ddata, kernel, 0, x, &hx);
    ol_extractor_centroid_daofind_axis(ddata, kernel, 1, y, &hy);
    *roundness = 2 * (hx - hy) / (hx + hy);
    ol_matrix_free(ddata);
}


static OLCatalog *ol_extractor_build_stars(gsize width,
                                           gsize height,
                                           double threshold,
                                           double sharplo,
                                           double sharphi,
                                           double roundlo,
                                           double roundhi,
                                           double sky,
                                           OLExtractorKernel *kernel,
                                           OLExtractorObject *objs,
                                           gsize nobjs)
{
    gsize i;
    int xkcen = kernel->kern->width / 2;
    int ykcen = kernel->kern->height / 2;
    int klen = kernel->kern->len;
    OLCatalog *catalog = ol_catalog_new(width, height);

    for (i = 0; i < nobjs; i++) {
        OLExtractorObject *obj = &(objs[i]);
        g_assert(klen == (obj->data->width * obj->data->height));
        OLMatrix *convdata = ol_matrix_copy(obj->convdata);
        convdata->data.f64[(ykcen * kernel->kern->height) + xkcen] = 0.0;
        OLMatrix *q1 = ol_matrix_copy_region(convdata,
                                             xkcen + 1,
                                             0,
                                             obj->data->width - (xkcen + 1),
                                             ykcen + 1);
        OLMatrix *q2 = ol_matrix_copy_region(convdata,
                                             0,
                                             0,
                                             xkcen + 1,
                                             ykcen);
        OLMatrix *q3 = ol_matrix_copy_region(convdata,
                                             0,
                                             ykcen,
                                             xkcen,
                                             obj->data->height - ykcen);
        OLMatrix *q4 = ol_matrix_copy_region(convdata,
                                             xkcen,
                                             ykcen + 1,
                                             obj->data->width - xkcen,
                                             obj->data->height - (ykcen + 1));
        double sum2 =
            -ol_matrix_sum(q1) +
            ol_matrix_sum(q2) +
            -ol_matrix_sum(q3) +
            ol_matrix_sum(q4);
        double sum4 = ol_matrix_sum_abs(convdata);
        double roundness1 = 2.0 * sum2 / sum4;


        gint32 peak = obj->data->data.i32[(ykcen * obj->data->width) + xkcen];
        double convpeak = obj->convdata->data.f64[(ykcen * obj->data->width) + xkcen];

        double mean = ol_matrix_sum_masked(obj->data, kernel->mask);
        mean = (mean - peak) / (kernel->npts - 1);
        double sharpness = (peak - mean) / convpeak;

        double flux = (convpeak / threshold) - (sky * obj->data->width * obj->data->height);

        double mag = flux <= 0.0 ? (double) NAN : -2.5 * log10(flux);

        double roundness2;
        double dx, dy;

        ol_extractor_centroid_daofind(obj->data, kernel, &dx, &dy, &roundness2);

        ol_matrix_free(q1);
        ol_matrix_free(q2);
        ol_matrix_free(q3);
        ol_matrix_free(q4);
        ol_matrix_free(convdata);

        if (roundness1 < roundlo || roundness1 > roundhi) {
            continue;
        }
        if (roundness2 < roundlo || roundness2 > roundhi) {
            continue;
        }
        if (sharpness < sharplo || sharpness > sharphi) {
            continue;
        }
        OLStar *star = ol_star_new(obj->x + dx, obj->y + dy,
                                   roundness1, roundness2, sharpness,
                                   flux, mag);

        ol_catalog_add_star(catalog, star);
    }

    return catalog;
}


OLCatalog *ol_extractor_find_stars_daofind(OLExtractor *ext G_GNUC_UNUSED,
                                           OLImage *image,
                                           double threshold,
                                           double fwhm,
                                           double ratio,
                                           double theta,
                                           double sigma_radius,
                                           double sharplo,
                                           double sharphi,
                                           double roundlo,
                                           double roundhi,
                                           double sky,
                                           gboolean exclude_border)
{
    OLExtractorKernel *kernel = ol_extractor_kernel_new(fwhm, ratio, theta, sigma_radius);
    int width = image->width;
    int height = image->height;
    guint16 *sdata = image->matrix->data.u16;
    OLMatrix *imgmat = ol_matrix_new(OL_MATRIX_FORMAT_INT32, width, height);
    double median;
    gsize len = width * height;
    gsize n;
    OLExtractorObject *objs = NULL;
    gsize nobjs = 0;
    gint32 *itmp = imgmat->data.i32;
    OLCatalog *catalog;
    gdouble mad_std;

    g_assert(image->matrix->format == OL_MATRIX_FORMAT_UINT16);
    g_assert(image->channels == OL_IMAGE_CHANNELS_GRAY);

    for (n = 0; n < len; n++) {
        *itmp = *sdata;
        itmp++;
        sdata++;
    }

    median = ol_matrix_median(imgmat);

    itmp = imgmat->data.i32;
    for (n = 0; n < len; n++) {
        *itmp -= median;
        itmp++;
    }


    mad_std = ol_matrix_median_abs_deviation_std(imgmat);

    threshold *= mad_std;
    threshold *= kernel->relerr;

    ol_extractor_find_objs(imgmat, threshold, kernel, 0.0, exclude_border, TRUE,
                           &objs, &nobjs);

    catalog = ol_extractor_build_stars(image->width, image->height,
                                       threshold, sharplo, sharphi, roundlo, roundhi,
                                       sky, kernel, objs, nobjs);
    for (n = 0; n < nobjs; n++) {
        ol_matrix_free(objs[n].convdata);
        ol_matrix_free(objs[n].data);
    }
    g_free(objs);
    ol_matrix_free(imgmat);

    ol_extractor_kernel_free(kernel);
    return catalog;
}
