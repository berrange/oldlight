/*
 * oldlight-quad.c: asterism of four stars
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>
#include <math.h>

#include "oldlight/oldlight-quad.h"

GType ol_quad_get_type(void)
{
    static GType quad_type = 0;

    if (G_UNLIKELY(quad_type == 0)) {
        quad_type = g_boxed_type_register_static
            ("OLQuad",
             (GBoxedCopyFunc)ol_quad_copy,
             (GBoxedFreeFunc)ol_quad_free);
    }

    return quad_type;
}

/**
 * Building an asterism per this:
 *
 * http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:0910.2233
 *
 * Given 4 input stars:
 *  - Identify the 2 stars furthest apart
 *  - Map them to locations (0, 0) and (1, 1)
 *  - Scale other 2 stars into same coords
 *  - Normalize the result so it is rotation invariant
 */
OLQuad *ol_quad_new(OLLocation *a,
                    OLLocation *b,
                    OLLocation *c,
                    OLLocation *d)
{
    OLQuad *quad = g_new0(OLQuad, 1);
    struct {
        OLLocation *a;
        OLLocation *b;
        OLLocation *c;
        OLLocation *d;
        gdouble distance; /* a -> b */
    } sets[] = {
        {a, b, c, d, ol_location_distance(a, b)},
        {a, c, b, d, ol_location_distance(a, c)},
        {a, d, b, c, ol_location_distance(a, d)},
        {b, c, a, d, ol_location_distance(b, c)},
        {b, d, a, c, ol_location_distance(b, d)},
        {c, d, a, b, ol_location_distance(c, d)},
    };
    gsize i;
    gsize furthest = 0;
    OLLocation points[4];
    gdouble dx, dy;
    gdouble dx02, dx03, dx12, dx13;

    /*
     * Decide on the two stars that form the
     * coordinate system, origin & limit.
     * They are the furthest apart pair.
     */
    for (i = 1; i < G_N_ELEMENTS(sets); i++) {
        if (sets[i].distance >= sets[furthest].distance) {
            furthest = i;
        }
    }

    quad->stars[0] = *sets[furthest].a;
    quad->stars[1] = *sets[furthest].b;
    quad->stars[2] = *sets[furthest].c;
    quad->stars[3] = *sets[furthest].d;

    memcpy(points, quad->stars, sizeof(points));

    dx = (points[1].x - points[0].x);
    dy = (points[1].y - points[0].y);
    /*
     * Normalize rotation so that both
     * coords of the limit star are positive
     */
    if (dx < 0 && dy < 0) {
        for (i = 0; i < 4; i++) {
            points[i].x *= -1;
            points[i].y *= -1;
        }
    } else if (dx < 0) {
        for (i = 0; i < 4; i++) {
            gdouble tmp = points[i].x * -1;
            points[i].x = points[i].y;
            points[i].y = tmp;
        }
    } else if (dy < 0) {
        for (i = 0; i < 4; i++) {
            gdouble tmp = points[i].x;
            points[i].x = points[i].y * -1;
            points[i].y = tmp;
        }
    }

    /*
     * Decide whether we need to flip the
     * coordinate system.
     *
     * Whichever star is closer to the
     * sum of the two middle stars will
     * be the origin.
     *
     * The middle star with the smaller
     * x coords will also be first
     */
    dx02 = fabs(points[0].x - points[2].x);
    dx03 = fabs(points[0].x - points[3].x);
    dx12 = fabs(points[1].x - points[2].x);
    dx13 = fabs(points[1].x - points[3].x);
    if ((dx02 + dx03) > (dx12 + dx13)) {
        OLLocation tmp = quad->stars[0];
        quad->stars[0] = quad->stars[1];
        quad->stars[1] = tmp;

        tmp = points[0];
        points[0] = points[1];
        points[1] = tmp;

        if (points[2].x < points[3].x) {
            tmp = quad->stars[2];
            quad->stars[2] = quad->stars[3];
            quad->stars[3] = tmp;

            tmp = points[2];
            points[2] = points[3];
            points[3] = tmp;
        }
    } else {
        if (points[2].x > points[3].x) {
            OLLocation tmp = quad->stars[2];
            quad->stars[2] = quad->stars[3];
            quad->stars[3] = tmp;

            tmp = points[2];
            points[2] = points[3];
            points[3] = tmp;
        }
    }

    /*
     * First translate everything so that the
     * first star is at (0, 0)
     */
    for (i = 1; i < 4; i++) {
        points[i].x -= points[0].x;
        points[i].y -= points[0].y;
    }
    points[0].x = 0;
    points[0].y = 0;

    /*
     * Finally scale everything so that the
     * limit star is at (1, 1)
     */
    for (i = 2; i < 4; i++) {
        points[i].x /= points[1].x;
        points[i].y /= points[1].y;
    }
    points[1].x /= 1.0;
    points[1].y /= 1.0;

    quad->hash[0].x = points[2].x;
    quad->hash[0].y = points[2].y;
    quad->hash[1].x = points[3].x;
    quad->hash[1].y = points[3].y;

    return quad;
}

OLQuad *ol_quad_copy(OLQuad *quad)
{
    OLQuad *newquad = g_new0(OLQuad, 1);

    memcpy(newquad, quad, sizeof(*quad));

    return newquad;
}

void ol_quad_free(OLQuad *quad)
{
    g_free(quad);
}

gboolean ol_quad_equal(OLQuad *quad,
                       OLQuad *other)
{
    return (ol_location_equal(&quad->stars[0], &other->stars[0]) &&
            ol_location_equal(&quad->stars[1], &other->stars[1]) &&
            ol_location_equal(&quad->stars[2], &other->stars[2]) &&
            ol_location_equal(&quad->stars[3], &other->stars[3]));
}

gboolean ol_quad_matches(OLQuad *quad,
                         OLQuad *unknown,
                         gdouble fuzz)
{
    return (fabs(quad->hash[0].x - unknown->hash[0].x) < fuzz) &&
        (fabs(quad->hash[0].y - unknown->hash[0].y) < fuzz) &&
        (fabs(quad->hash[1].x - unknown->hash[1].x) < fuzz) &&
        (fabs(quad->hash[1].y - unknown->hash[1].y) < fuzz);
}

gdouble ol_quad_distance(OLQuad *quad,
                         OLQuad *unknown)
{
    return sqrt(pow(quad->hash[0].x -  unknown->hash[0].x, 2) +
                pow(quad->hash[0].y -  unknown->hash[0].y, 2) +
                pow(quad->hash[1].x -  unknown->hash[1].x, 2) +
                pow(quad->hash[1].y -  unknown->hash[1].y, 2));
}

void ol_quad_render(OLQuad *quad,
                    OLImage *image)
{
    gushort pixel[3] = {
        random() * 65536 / RAND_MAX,
        random() * 65536 / RAND_MAX,
        random() * 65536 / RAND_MAX,
    };
    ol_image_draw_line(image,
                       quad->stars[0].x,
                       quad->stars[0].y,
                       quad->stars[2].x,
                       quad->stars[2].y,
                       (guchar *)pixel);
    ol_image_draw_line(image,
                       quad->stars[0].x,
                       quad->stars[0].y,
                       quad->stars[3].x,
                       quad->stars[3].y,
                       (guchar *)pixel);
    ol_image_draw_line(image,
                       quad->stars[1].x,
                       quad->stars[1].y,
                       quad->stars[2].x,
                       quad->stars[2].y,
                       (guchar *)pixel);
    ol_image_draw_line(image,
                       quad->stars[1].x,
                       quad->stars[1].y,
                       quad->stars[3].x,
                       quad->stars[3].y,
                       (guchar *)pixel);
}
