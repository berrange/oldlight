/*
 * oldlight-bayer-downsample.h: bayer processing downsample algorithm
 *
 * This file is derived from bayer.c in libdc1394,
 * written by Damien Douxchamps and Frederic Devernay
 *
 * The oldlight changes are:
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * coriander's Bayer decoding
 */

#include "oldlight/oldlight-bayer-downsample.h"

#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


struct _OLBayerDownsample
{
    OLBayer parent;
};

G_DEFINE_TYPE(OLBayerDownsample, ol_bayer_downsample, OL_TYPE_BAYER);


static void
ol_bayer_downsample_decode_uint8(const guint8 *restrict bayer,
                                 guint8 *restrict rgb,
                                 gsize sx,
                                 gsize sy,
                                 OLBayerFilter filter);
static void
ol_bayer_downsample_decode_uint16(const guint16 *restrict bayer,
                                  guint16 *restrict rgb,
                                  gsize sx,
                                  gsize sy,
                                  OLBayerFilter filter,
                                  int bits);

static void ol_bayer_downsample_class_init(OLBayerDownsampleClass *klass)
{
    OLBayerClass *bayer_class = OL_BAYER_CLASS(klass);

    bayer_class->decode8 = ol_bayer_downsample_decode_uint8;
    bayer_class->decode16 = ol_bayer_downsample_decode_uint16;
}


static void ol_bayer_downsample_init(OLBayerDownsample *bayer G_GNUC_UNUSED)
{
}


static void
ol_bayer_downsample_decode_uint8(const guint8 *restrict bayer,
                                 guint8 *restrict rgb,
                                 gsize sx,
                                 gsize sy,
                                 OLBayerFilter filter)
{
    guint8 *outR, *outG, *outB;
    register int i, j;
    int tmp;
    int st=sx*sy;
    int p;
    int sx2=sx<<1;

    switch (filter) {
    case OL_BAYER_FILTER_GRBG:
    case OL_BAYER_FILTER_BGGR:
        outR = &rgb[0];
        outG = &rgb[1];
        outB = &rgb[2];
        break;
    case OL_BAYER_FILTER_GBRG:
    case OL_BAYER_FILTER_RGGB:
        outR = &rgb[2];
        outG = &rgb[1];
        outB = &rgb[0];
        break;
    default:
        g_assert_not_reached();
    }

    switch (filter) {
    case OL_BAYER_FILTER_GRBG:        //---------------------------------------------------------
    case OL_BAYER_FILTER_GBRG:
        for (i = 0; i < st; i += sx2) {
            for (j = 0; j < sx; j += 2) {
                p=((i >> 2) + (j >> 1)) * 3;
                tmp = bayer[i + j];
                tmp+= bayer[i + sx + j + 1];
                tmp>>=1;
                outG[p] = (guint8)tmp;
                outR[p] = bayer[i + j + 1];
                outB[p] = bayer[i + sx + j];
            }
        }
        break;
    case OL_BAYER_FILTER_BGGR:        //---------------------------------------------------------
    case OL_BAYER_FILTER_RGGB:
        for (i = 0; i < st; i += sx2) {
            for (j = 0; j < sx; j += 2) {
                p=((i >> 2) + (j >> 1)) * 3;
                tmp = bayer[i + sx + j];
                tmp+= bayer[i + j + 1];
                tmp>>= 1;
                outG[p] = (guint8)tmp;
                outR[p] = bayer[i + sx + j + 1];
                outB[p] = bayer[i + j];
            }
        }
        break;
    default:
        g_assert_not_reached();
    }

}


static void
ol_bayer_downsample_decode_uint16(const guint16 *restrict bayer,
                                  guint16 *restrict rgb,
                                  gsize sx,
                                  gsize sy,
                                  OLBayerFilter filter,
                                  int bits G_GNUC_UNUSED)
{
    guint16 *outR, *outG, *outB;
    register int i, j;
    int tmp;
    int st=sx*sy;
    int sx2=sx<<1;
    int p;

    switch (filter) {
    case OL_BAYER_FILTER_GRBG:
    case OL_BAYER_FILTER_BGGR:
        outR = &rgb[0];
        outG = &rgb[1];
        outB = &rgb[2];
        break;
    case OL_BAYER_FILTER_GBRG:
    case OL_BAYER_FILTER_RGGB:
        outR = &rgb[2];
        outG = &rgb[1];
        outB = &rgb[0];
        break;
    default:
        g_assert_not_reached();
    }

    switch (filter) {
    case OL_BAYER_FILTER_GRBG:        //---------------------------------------------------------
    case OL_BAYER_FILTER_GBRG:
        for (i = 0; i < st; i += sx2) {
            for (j = 0; j < sx; j += 2) {
                p=((i >> 2) + (j >> 1)) * 3;
                tmp = bayer[i + j];
                tmp+= bayer[i + sx + j + 1];
                tmp>>=1;
                outG[p] = (guint16)tmp;
                outR[p] = bayer[i + j + 1];
                outB[p] = bayer[i + sx + j];
            }
        }
        break;
    case OL_BAYER_FILTER_BGGR:        //---------------------------------------------------------
    case OL_BAYER_FILTER_RGGB:
        for (i = 0; i < st; i += sx2) {
            for (j = 0; j < sx; j += 2) {
                p=((i >> 2) + (j >> 1)) * 3;
                tmp = bayer[i + sx + j];
                tmp+= bayer[i + j + 1];
                tmp>>= 1;
                outG[p] = (guint16)tmp;
                outR[p] = bayer[i + sx + j + 1];
                outB[p] = bayer[i + j];
            }
        }
        break;
    default:
        g_assert_not_reached();
    }
}
