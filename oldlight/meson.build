
gnome = import('gnome')

oldlight_sources = [
  'oldlight-bayer.h',
  'oldlight-bayer.c',
  'oldlight-bayer-ahd.h',
  'oldlight-bayer-ahd.c',
  'oldlight-bayer-bilinear.h',
  'oldlight-bayer-bilinear.c',
  'oldlight-bayer-downsample.h',
  'oldlight-bayer-downsample.c',
  'oldlight-bayer-edge-sense.h',
  'oldlight-bayer-edge-sense.c',
  'oldlight-bayer-hqlinear.h',
  'oldlight-bayer-hqlinear.c',
  'oldlight-bayer-nearest-neighbour.h',
  'oldlight-bayer-nearest-neighbour.c',
  'oldlight-bayer-simple.h',
  'oldlight-bayer-simple.c',
  'oldlight-bayer-vng.h',
  'oldlight-bayer-vng.c',
  'oldlight-catalog.c',
  'oldlight-catalog.h',
  'oldlight-extractor.c',
  'oldlight-extractor.h',
  'oldlight-identity.h',
  'oldlight-identity.c',
  'oldlight-image.c',
  'oldlight-image.h',
  'oldlight-image-list.c',
  'oldlight-image-list.h',
  'oldlight-image-io.c',
  'oldlight-image-io.h',
  'oldlight-image-reader.c',
  'oldlight-image-reader.h',
  'oldlight-image-reader-raw.c',
  'oldlight-image-reader-raw.h',
  'oldlight-image-reader-pnm.c',
  'oldlight-image-reader-pnm.h',
  'oldlight-image-writer.c',
  'oldlight-image-writer.h',
  'oldlight-image-writer-pnm.c',
  'oldlight-image-writer-pnm.h',
  'oldlight-location.c',
  'oldlight-location.h',
  'oldlight-mapping.c',
  'oldlight-mapping.h',
  'oldlight-matrix.c',
  'oldlight-matrix.h',
  'oldlight-progress.c',
  'oldlight-progress.h',
  'oldlight-quad.h',
  'oldlight-quad.c',
  'oldlight-raw-params.h',
  'oldlight-raw-params.c',
  'oldlight-raw-processor.h',
  'oldlight-raw-processor.c',
  'oldlight-stacker.c',
  'oldlight-stacker.h',
  'oldlight-star.c',
  'oldlight-star.h',
  'oldlight-transform.c',
  'oldlight-transform.h',
]

oldlight_enums = []

oldlight_enum_headers = [
  'oldlight-matrix.h',
  'oldlight-image.h',
  'oldlight-stacker.h',
  'oldlight-bayer.h',
  'oldlight-raw-processor.h',
]

oldlight_enums += gnome.mkenums(
  'oldlight-enums.h',
  sources: oldlight_enum_headers,
  comments: '/* @comment@ */',
  identifier_prefix: 'OL',
  symbol_prefix: 'ol',
  fhead: '#ifndef OL_ENUMS_H__\n#define OL_ENUMS_H__\n\n#include <glib-object.h>\nG_BEGIN_DECLS\n\n',
  ftail: 'G_END_DECLS\n\n#endif /* OL_ENUMS_H__ */\n',
  fprod: '\n/* --- @filename@ --- */',
  eprod: '#define OL_TYPE_@ENUMSHORT@ @enum_name@_get_type()\nGType @enum_name@_get_type (void);\n',
  install_header: false,
)

oldlight_enums += gnome.mkenums(
  'oldlight-enums.c',
  sources: oldlight_enum_headers,
  comments: '/* @comment@ */',
  identifier_prefix: 'OL',
  symbol_prefix: 'ol',
  fhead: '#include "oldlight/oldlight-enums.h"\n\n',
  fprod: '\n/* enumerations from "@filename@" */\n#include "@filename@"\n',
  vhead: 'static const G@Type@Value _@enum_name@_values[] = {',
  vprod: '  { @VALUENAME@, "@VALUENAME@", "@valuenick@" },',
  vtail: '  { 0, NULL, NULL }\n};\n\nGType\n@enum_name@_get_type (void)\n{\n  static GType type = 0;\n\n  if (!type)\n    type = g_@type@_register_static ("@EnumName@", _@enum_name@_values);\n\n  return type;\n}\n\n',
  install_header: false,
)

enum_dep = declare_dependency(sources: oldlight_enums)

oldlight_sources += oldlight_enums

oldlight_deps = [
  enum_dep,
  glib_dep,
  libraw_dep,
  libm_dep,
]

liboldlight = shared_library(
  'oldlight',
  sources: oldlight_sources,
  dependencies: oldlight_deps,
  include_directories: oldlight_inc,
  version: '0.0.0',
  install: true,
  install_dir: oldlight_libdir,
)

liboldlight_dep = declare_dependency(
  link_with: liboldlight,
  dependencies: oldlight_deps,
)

oldlight = executable(
  'oldlight',
  ['oldlight.c'],
  dependencies: [liboldlight_dep],
  include_directories: oldlight_inc,
  install: true,
  install_dir: oldlight_bindir
)

oldlight_stack = executable(
  'oldlight-stack',
  ['oldlight-stack.c'],
  dependencies: [liboldlight_dep],
  include_directories: oldlight_inc,
  install: true,
  install_dir: oldlight_bindir
)

oldlight_raw_extract = executable(
  'oldlight-raw-extract',
  ['oldlight-raw-extract.c'],
  dependencies: [liboldlight_dep],
  include_directories: oldlight_inc,
  install: true,
  install_dir: oldlight_bindir
)

test_oldlight_transform = executable(
  'test-oldlight-transform',
  ['oldlight-transform-test.c'],
  dependencies: oldlight_deps + [liboldlight_dep],
  include_directories: oldlight_inc,
  install: false,
)

test_oldlight_quad = executable(
  'test-oldlight-quad',
  ['oldlight-quad-test.c'],
  dependencies: oldlight_deps + [liboldlight_dep],
  include_directories: oldlight_inc,
  install: false,
)

test('Transform', test_oldlight_transform)
test('Quad', test_oldlight_quad)
