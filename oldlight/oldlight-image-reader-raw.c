/*
 * oldlight-image-reader-raw.c: Raw image reader
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-reader-raw.h"

#include <libraw/libraw.h>
#include <unistd.h>

struct _OLImageReaderRaw
{
    OLImageReader parent;
    libraw_data_t *handle;
};


G_DEFINE_TYPE(OLImageReaderRaw, ol_image_reader_raw, OL_TYPE_IMAGE_READER);


static gboolean
ol_image_reader_raw_open(OLImageIO *io,
                         GError **err);
static gboolean
ol_image_reader_raw_close(OLImageIO *io,
                          GError **err);
static OLImage *
ol_image_reader_raw_load_payload(OLImageReader *reader,
                                 guint x,
                                 guint y,
                                 guint width,
                                 guint height,
                                 GError **err);


#define OL_IMAGE_READER_RAW_ERROR ol_image_reader_raw_error_quark()

static GQuark
ol_image_reader_raw_error_quark(void)
{
    return g_quark_from_static_string("ol-image-reader-raw");
}


static void
ol_image_reader_raw_finalize(GObject *object)
{
    ol_image_io_close(OL_IMAGE_IO(object), NULL);

    G_OBJECT_CLASS(ol_image_reader_raw_parent_class)->finalize(object);
}


static void
ol_image_reader_raw_class_init(OLImageReaderRawClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    OLImageIOClass *io_class = OL_IMAGE_IO_CLASS(klass);
    OLImageReaderClass *reader_class = OL_IMAGE_READER_CLASS(klass);

    object_class->finalize = ol_image_reader_raw_finalize;

    io_class->open = ol_image_reader_raw_open;
    io_class->close = ol_image_reader_raw_close;
    reader_class->load_payload = ol_image_reader_raw_load_payload;
}


static void
ol_image_reader_raw_init(OLImageReaderRaw *reader G_GNUC_UNUSED)
{
}


OLImageReaderRaw *
ol_image_reader_raw_new(const gchar *filename)
{
    return OL_IMAGE_READER_RAW(g_object_new(OL_TYPE_IMAGE_READER_RAW,
                                            "filename", filename,
                                            NULL));
}


static gboolean
ol_image_reader_set_error(const gchar *filename,
                          const gchar *fmt,
                          int ret,
                          GError **err)
{
    const gchar *msg;
    if (ret == LIBRAW_SUCCESS)
        return FALSE;

    if (ret > 0) {
        msg = strerror(ret);
    } else {
        msg = libraw_strerror(ret);
    }
    g_set_error(err, OL_IMAGE_READER_RAW_ERROR, 0,
                fmt, filename, msg);
    return TRUE;
}


static gboolean
ol_image_reader_raw_open(OLImageIO *io,
                         GError **err)
{
    OLImageReaderRaw *raw = OL_IMAGE_READER_RAW(io);
    const gchar *filename = ol_image_io_get_filename(io);
    int ret;

    raw->handle = libraw_init(0);
    if (!raw->handle) {
        ol_image_reader_set_error(filename, "Unable to load file %s: %s", ENOMEM, err);
        return FALSE;
    }

    ret = libraw_open_file(raw->handle, filename);
    if (ol_image_reader_set_error(filename, "Unable to load file %s: %s", ret, err))
        return FALSE;

    ol_image_io_set_width(io, raw->handle->rawdata.sizes.iwidth);
    ol_image_io_set_height(io, raw->handle->rawdata.sizes.iheight);
    ol_image_io_set_format(io, OL_MATRIX_FORMAT_UINT16);
    ol_image_io_set_channels(io, OL_IMAGE_CHANNELS_GRAY);

    return TRUE;
}


static gboolean
ol_image_reader_raw_close(OLImageIO *io,
                          GError **err G_GNUC_UNUSED)
{
    OLImageReaderRaw *raw = OL_IMAGE_READER_RAW(io);
    if (raw->handle)
        libraw_close(raw->handle);
    raw->handle = NULL;

    return TRUE;
}


static OLImage *
ol_image_reader_raw_load_payload(OLImageReader *reader,
                                 guint x,
                                 guint y,
                                 guint width,
                                 guint height,
                                 GError **err)
{
    OLImageReaderRaw *raw = OL_IMAGE_READER_RAW(reader);
    const gchar *filename = ol_image_io_get_filename(OL_IMAGE_IO(reader));
    guint iwidth, iheight;
    OLImage *img;
    int ret;
    guint8 *tmp;
    gsize line;

    iwidth = ol_image_io_get_width(OL_IMAGE_IO(reader));
    iheight = ol_image_io_get_height(OL_IMAGE_IO(reader));

    if (((x + width) > iwidth) ||
        ((y + height) > iheight)) {
        ol_image_reader_set_error(filename, "Data region out of range loading %s: %s", EINVAL, err);
        return NULL;
    }

    if (!raw->handle->rawdata.raw_image) {
        ret = libraw_unpack(raw->handle);
        if (ol_image_reader_set_error(filename, "Unable to read image data %s: %s", ret, err))
            return NULL;
    }

    img = ol_image_new(OL_MATRIX_FORMAT_UINT16,
                       OL_IMAGE_CHANNELS_GRAY,
                       raw->handle->rawdata.sizes.iwidth,
                       raw->handle->rawdata.sizes.iheight);
    tmp = img->matrix->data.u8;
    for (line = y; line < height; line++) {
        guint8 *data = (guint8 *)raw->handle->rawdata.raw_image +
            (line * raw->handle->rawdata.sizes.raw_pitch) + (x * 2);
        memcpy(tmp, data, width * 2);
        tmp += width * 2;
    }

    return img;
}


OLRawParams *ol_image_reader_raw_get_params(OLImageReaderRaw *raw)
{
    OLRawParams *params = ol_raw_params_new();

    params->filters = raw->handle->rawdata.iparams.filters;
    memcpy(params->colors, raw->handle->rawdata.iparams.cdesc, sizeof(params->colors));

    g_assert(params->img_wb->len ==
             G_N_ELEMENTS(raw->handle->rawdata.color.cam_mul));
    memcpy(params->img_wb->data.f32,
           raw->handle->rawdata.color.cam_mul,
           sizeof(raw->handle->rawdata.color.cam_mul));

    g_assert(params->cam_wb->len ==
             G_N_ELEMENTS(raw->handle->rawdata.color.pre_mul));
    memcpy(params->cam_wb->data.f32,
           raw->handle->rawdata.color.pre_mul,
           sizeof(raw->handle->rawdata.color.pre_mul));

    if (!params->img_wb->data.f32[1])
        params->img_wb->data.f32[1] = 1.0;
    if (!params->img_wb->data.f32[3])
        params->img_wb->data.f32[3] = params->img_wb->data.f32[1];

    if (!params->cam_wb->data.f32[1])
        params->cam_wb->data.f32[1] = 1.0;
    if (!params->cam_wb->data.f32[3])
        params->cam_wb->data.f32[3] = params->cam_wb->data.f32[1];

    params->black = raw->handle->rawdata.color.black;
    params->img_max = raw->handle->rawdata.color.data_maximum;
    params->cam_max = raw->handle->rawdata.color.maximum;

    g_assert(params->cam_2_srgb->width ==
             G_N_ELEMENTS(raw->handle->rawdata.color.rgb_cam));
    g_assert(params->cam_2_srgb->height ==
             G_N_ELEMENTS(raw->handle->rawdata.color.rgb_cam[0]));
    memcpy(params->cam_2_srgb->data.f32,
           raw->handle->rawdata.color.rgb_cam,
           sizeof(raw->handle->rawdata.color.rgb_cam));

    g_assert(params->cam_2_xyz->width ==
             G_N_ELEMENTS(raw->handle->rawdata.color.cam_xyz));
    g_assert(params->cam_2_xyz->height ==
             G_N_ELEMENTS(raw->handle->rawdata.color.cam_xyz[0]));
    memcpy(params->cam_2_xyz->data.f32,
           raw->handle->rawdata.color.cam_xyz,
           sizeof(raw->handle->rawdata.color.cam_xyz));

    return params;
}
