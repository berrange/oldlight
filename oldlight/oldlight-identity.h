/*
 * oldlight-identity.h: list of asterisms
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_IDENTITY_H__
#define OL_IDENTITY_H__

#include <glib-object.h>

#include "oldlight/oldlight-quad.h"
#include "oldlight/oldlight-image.h"
#include "oldlight/oldlight-mapping.h"

G_BEGIN_DECLS

#define OL_TYPE_IDENTITY            (ol_identity_get_type ())

G_DECLARE_FINAL_TYPE(OLIdentity, ol_identity, OL, IDENTITY, GObject);

OLIdentity *ol_identity_new(gsize width,
                            gsize height);

gsize ol_identity_get_nquads(OLIdentity *identity);

OLQuad *ol_identity_get_quad(OLIdentity *identity, gsize i);

OLQuad **ol_identity_get_quads(OLIdentity *identity);

void ol_identity_add_quad(OLIdentity *identity,
                          OLQuad *quad);

void ol_identity_render(OLIdentity *identity,
                        OLImage *image);

OLMapping *ol_identity_match(OLIdentity *identity,
                             OLIdentity *unknown,
                             gdouble fuzz);

gboolean ol_identity_save(OLIdentity *identity,
                          const gchar *filename,
                          GError **error);

gboolean ol_identity_load(OLIdentity *identity,
                          const gchar *filename,
                          GError **error);

G_END_DECLS

#endif /* OL_IDENTITY_H__ */
