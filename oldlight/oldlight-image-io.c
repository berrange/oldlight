/*
 * oldlight-image-io.c: image io
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-image-io.h"


typedef struct _OLImageIOPrivate OLImageIOPrivate;
struct _OLImageIOPrivate
{
    GObject parent;
    gchar *filename;
    guint width;
    guint height;
    OLMatrixFormat format;
    OLImageChannels channels;
};


G_DEFINE_TYPE_WITH_PRIVATE(OLImageIO, ol_image_io, G_TYPE_OBJECT);


enum {
    PROP_0,
    PROP_FILENAME,
    PROP_WIDTH,
    PROP_HEIGHT,
    PROP_FORMAT,
    PROP_CHANNELS,
};


static void ol_image_io_get_property(GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
    OLImageIO *io = OL_IMAGE_IO(object);
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    switch (prop_id) {
    case PROP_FILENAME:
        g_value_set_string(value, priv->filename);
        break;

    case PROP_WIDTH:
        g_value_set_uint(value, priv->width);
        break;

    case PROP_HEIGHT:
        g_value_set_uint(value, priv->height);
        break;

    case PROP_FORMAT:
        g_value_set_enum(value, priv->format);
        break;

    case PROP_CHANNELS:
        g_value_set_enum(value, priv->channels);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_image_io_set_property(GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
    OLImageIO *io = OL_IMAGE_IO(object);
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    switch (prop_id) {
    case PROP_FILENAME:
        priv->filename = g_value_dup_string(value);
        break;

    case PROP_WIDTH:
        priv->width = g_value_get_uint(value);
        break;

    case PROP_HEIGHT:
        priv->height = g_value_get_uint(value);
        break;

    case PROP_FORMAT:
        priv->format = g_value_get_enum(value);
        break;

    case PROP_CHANNELS:
        priv->channels = g_value_get_enum(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void ol_image_io_finalize(GObject *object)
{
    OLImageIO *io = OL_IMAGE_IO(object);
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    g_free(priv->filename);

    G_OBJECT_CLASS(ol_image_io_parent_class)->finalize(object);
}


static void ol_image_io_class_init(OLImageIOClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = ol_image_io_finalize;
    object_class->get_property = ol_image_io_get_property;
    object_class->set_property = ol_image_io_set_property;

    g_object_class_install_property(object_class,
                                    PROP_FILENAME,
                                    g_param_spec_string("filename",
                                                        "Filename",
                                                        "Image filename",
                                                        NULL,
                                                        G_PARAM_READWRITE |
                                                        G_PARAM_CONSTRUCT_ONLY |
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_WIDTH,
                                    g_param_spec_uint("width",
                                                      "Width",
                                                      "Image width",
                                                      0, 65565, 0,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_HEIGHT,
                                    g_param_spec_uint("height",
                                                      "Height",
                                                      "Image height",
                                                      0, 65565, 0,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_FORMAT,
                                    g_param_spec_enum("format",
                                                      "Format",
                                                      "Image format",
                                                      OL_TYPE_MATRIX_FORMAT,
                                                      OL_MATRIX_FORMAT_UINT8,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));

    g_object_class_install_property(object_class,
                                    PROP_CHANNELS,
                                    g_param_spec_enum("channels",
                                                      "Channels",
                                                      "Image channels",
                                                      OL_TYPE_IMAGE_CHANNELS,
                                                      OL_IMAGE_CHANNELS_GRAY,
                                                      G_PARAM_READWRITE |
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB));
}


static void ol_image_io_init(OLImageIO *io G_GNUC_UNUSED)
{
}


const gchar *ol_image_io_get_filename(OLImageIO *io)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    return priv->filename;
}


guint ol_image_io_get_width(OLImageIO *io)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    return priv->width;
}


guint ol_image_io_get_height(OLImageIO *io)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    return priv->height;
}


OLMatrixFormat ol_image_io_get_format(OLImageIO *io)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    return priv->format;
}


OLImageChannels ol_image_io_get_channels(OLImageIO *io)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    return priv->channels;
}


void ol_image_io_set_width(OLImageIO *io,
                           guint width)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    priv->width = width;
}


void ol_image_io_set_height(OLImageIO *io,
                            guint height)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    priv->height = height;
}


void ol_image_io_set_format(OLImageIO *io,
                            OLMatrixFormat format)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    priv->format = format;
}


void ol_image_io_set_channels(OLImageIO *io,
                              OLImageChannels channels)
{
    OLImageIOPrivate *priv = ol_image_io_get_instance_private(io);

    priv->channels = channels;
}


gboolean ol_image_io_open(OLImageIO *io, GError **err)
{
    return OL_IMAGE_IO_GET_CLASS(io)->open(io, err);
}


gboolean ol_image_io_close(OLImageIO *io,
                           GError **err)
{
    return OL_IMAGE_IO_GET_CLASS(io)->close(io, err);
}
