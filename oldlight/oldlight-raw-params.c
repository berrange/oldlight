/*
 * oldlight-raw-params.c: raw file parameters
 *
 * Copyright (C) 2019 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "oldlight/oldlight-raw-params.h"

GType ol_raw_params_get_type(void)
{
    static GType raw_params_type = 0;

    if (G_UNLIKELY(raw_params_type == 0)) {
        raw_params_type = g_boxed_type_register_static
            ("OLRawParams",
             (GBoxedCopyFunc)ol_raw_params_copy,
             (GBoxedFreeFunc)ol_raw_params_free);
    }

    return raw_params_type;
}

OLRawParams *ol_raw_params_new(void)
{
    OLRawParams *params = g_new0(OLRawParams, 1);

    params->img_wb = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT32, 4, 1);
    params->cam_wb = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT32, 4, 1);
    params->cam_2_srgb = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT32, 3, 4);
    params->cam_2_xyz = ol_matrix_new(OL_MATRIX_FORMAT_FLOAT32, 4, 3);

    return params;
}

OLRawParams *ol_raw_params_copy(OLRawParams *params)
{
    OLRawParams *newparams = g_new0(OLRawParams, 1);

    memcpy(newparams, params, sizeof(*params));

    newparams->img_wb = ol_matrix_copy(params->img_wb);
    newparams->cam_wb = ol_matrix_copy(params->cam_wb);
    newparams->cam_2_srgb = ol_matrix_copy(params->cam_2_srgb);
    newparams->cam_2_xyz = ol_matrix_copy(params->cam_2_xyz);

    return newparams;
}

void ol_raw_params_free(OLRawParams *params)
{
    if (!params)
        return;

    ol_matrix_free(params->img_wb);
    ol_matrix_free(params->cam_wb);
    ol_matrix_free(params->cam_2_srgb);
    ol_matrix_free(params->cam_2_xyz);

    g_free(params);
}
