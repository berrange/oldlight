/*
 * oldlight-star.h: star properties
 *
 * Copyright (C) 2018 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OL_STAR_H__
#define OL_STAR_H__

#include "oldlight/oldlight-location.h"
#include "oldlight/oldlight-transform.h"

G_BEGIN_DECLS

#define OL_TYPE_STAR            (ol_star_get_type ())

typedef struct _OLStar OLStar;

struct _OLStar
{
    OLLocation location;
    gdouble roundness1;
    gdouble roundness2;
    gdouble sharpness;
    gdouble flux;
    gdouble magnitude;
};

GType ol_star_get_type(void);

OLStar *ol_star_new(gdouble x,
                    gdouble y,
                    gdouble roundness1,
                    gdouble roundness2,
                    gdouble sharpness,
                    gdouble flux,
                    gdouble magnitude);

OLStar *ol_star_copy(OLStar *star);

OLStar *ol_star_copy_transform(OLStar *star,
                               OLTransform *transform);

void ol_star_free(OLStar *star);

gboolean ol_star_equal(OLStar *star, OLStar *other);

gdouble ol_star_distance(OLStar *star, OLStar *other);

G_END_DECLS

#endif /* OL_STAR_H__ */
